package testdata

import (
	"cloud.google.com/go/firestore"
	"context"
	_ "embed"
	"encoding/json"
	"gitlab.com/mkorman/go-gin-react-playground/backend/client"
	"gitlab.com/mkorman/go-gin-react-playground/backend/security"
)

//go:embed accounts.json
var accountsJson []byte

//go:embed clients.json
var clientsJson []byte

func Upload(fs *firestore.Client) {
	var accounts []*security.Account
	_ = json.Unmarshal(accountsJson, &accounts)

	var clients []*client.Client
	_ = json.Unmarshal(clientsJson, &clients)

	uploadAccounts(fs, accounts)
	uploadClients(fs, clients)
}

func uploadAccounts(fs *firestore.Client, accounts []*security.Account) {
	collection := fs.Collection("accounts")
	batch := fs.Batch()

	for _, a := range accounts {
		doc := collection.Doc(a.ID)
		batch.Set(doc, a)
	}

	_, _ = batch.Commit(context.Background())
}

func uploadClients(fs *firestore.Client, clients []*client.Client) {
	collection := fs.Collection("clients")
	batch := fs.Batch()

	for _, c := range clients {
		doc := collection.Doc(c.ID)
		batch.Set(doc, c)
	}

	_, _ = batch.Commit(context.Background())
}
