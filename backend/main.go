package main

import (
	"flag"
	"github.com/gookit/config/v2"
	"github.com/gookit/config/v2/yaml"
	"github.com/mkorman9/go-commons/firestorelib"
	"github.com/mkorman9/go-commons/gcppubsub"
	"github.com/mkorman9/go-commons/grpclib"
	"github.com/mkorman9/go-commons/httpserver"
	"github.com/mkorman9/go-commons/info"
	"github.com/mkorman9/go-commons/logging"
	"github.com/mkorman9/go-commons/utils"
	"github.com/mkorman9/go-gin-protocol/protocol"
	"github.com/robfig/cron/v3"
	"github.com/rs/zerolog/log"
	"gitlab.com/mkorman/go-gin-react-playground/backend/captcha"
	"gitlab.com/mkorman/go-gin-react-playground/backend/client"
	"gitlab.com/mkorman/go-gin-react-playground/backend/security"
	"gitlab.com/mkorman/go-gin-react-playground/backend/testdata"
	"time"
)

const AppName = "gogin"

var AppVersion = "dev"

var configPath = flag.String("config", "config.yml", "path to config file")
var secretsPath = flag.String("secrets", "secrets.yml", "path to secrets file")

func main() {
	flag.Parse()

	config.AddDriver(yaml.Driver)
	_ = config.LoadFiles(*configPath, *secretsPath)
	_ = utils.LoadConfigFromEnv()

	appInfo := info.Build(AppName, AppVersion)

	logging.Setup(
		logging.Field("app", appInfo.Name),
		logging.Field("version", appInfo.Version),
		logging.Field("deployment_name", appInfo.DeploymentName),
		logging.Field("hostname", appInfo.Hostname),
	)

	log.Info().Msgf("Booting up: %v", appInfo)

	// external services
	fs, closeFS, err := firestorelib.NewClient()
	if err != nil {
		log.Fatal().Err(err).Msg("Cannot establish connection to Firestore, exiting!")
	}
	defer closeFS()

	pubsubClient, err := gcppubsub.NewClient()
	if err != nil {
		log.Fatal().Err(err).Msg("Cannot establish connection to PubSub, exiting!")
	}
	defer pubsubClient.Close()

	// test data for local development
	if upload := config.Bool("testdata.upload"); upload {
		testdata.Upload(fs)
	}

	// DAOs
	clientDAO := &client.FirestoreClientDAO{FS: fs}
	accountsDAO := &security.FirestoreAccountDAO{FS: fs}
	sessionDAO := &security.FirestoreSessionDAO{FS: fs}
	captchaDAO := &captcha.FirestoreCaptchaDAO{FS: fs}
	oauth2StatesDAO := &security.FirestoreOAuth2StatesDAO{FS: fs}

	// services
	mailService, err := security.NewMailService()
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to create mail service")
	}
	oauth2Service := security.NewOAuth2Service(oauth2StatesDAO)
	sessionService := security.NewSessionService(sessionDAO, accountsDAO)
	captchaService := captcha.NewCaptchaService(captchaDAO)
	accountService := security.NewAccountService(accountsDAO, mailService)
	clientsEventBus, err := client.NewClientEventBus(pubsubClient)
	if err != nil {
		log.Fatal().Err(err).Msg("Could not create clients event bus")
	}
	clientEventsChannel, err := client.StartClientEventsSubscription(pubsubClient)
	if err != nil {
		log.Fatal().Err(err).Msg("Could not create clients events handler")
	}
	clientService := client.NewClientService(clientDAO, clientsEventBus)

	// schedulers
	scheduler := cron.New(cron.WithLocation(time.UTC))
	_, _ = scheduler.AddFunc("@every 30m", security.CreateExpiredSessionsScheduler(sessionService))
	_, _ = scheduler.AddFunc("@every 30m", security.CreateExpiredOauth2StatesScheduler(oauth2Service))
	_, _ = scheduler.AddFunc("@every 30m", captcha.CreateExpiredCaptchasScheduler(captchaService))

	scheduler.Start()
	defer scheduler.Stop()

	// APIs
	s := httpserver.NewServer()

	httpserver.DebugAPI(s.Engine, appInfo)
	client.ClientAPI(s.Engine, clientService, sessionService, accountService)
	client.ClientEventsWebsocket(s.Engine, clientEventsChannel)
	security.AuthAPI(s.Engine, accountService, sessionService)
	security.SessionAPI(s.Engine, sessionService)
	security.AccountAPI(s.Engine, accountService, sessionService, captchaService)
	security.OAuth2API(s.Engine, accountService, sessionService, oauth2Service)
	security.AdminAPI(s.Engine, accountService, sessionService)
	captcha.CaptchaAPI(s.Engine, captchaService)

	// gRPC services
	grpcServer := grpclib.NewServer()
	protocol.RegisterClientServiceServer(grpcServer.Get(), client.NewClientServiceGRPC(clientService))

	// start and block
	errorChannel := make(chan error)

	s.Start(errorChannel)
	defer s.Stop()

	grpcServer.Start(errorChannel)
	defer grpcServer.Stop()

	utils.BlockThread(errorChannel)
}
