package security

import (
	"github.com/go-playground/validator/v10"
	"github.com/mkorman9/go-commons/web"
	"regexp"
)

func init() {
	accountNameRegex := regexp.MustCompile(`^[a-zA-Z\d ]+$`)
	web.DefaultValidator.RegisterValidation("accountname", func(fl validator.FieldLevel) bool {
		return accountNameRegex.MatchString(fl.Field().String())
	})

	oAuth2CodeRegex := regexp.MustCompile(`^[a-zA-Z\d]+$`)
	web.DefaultValidator.RegisterValidation("oauth2code", func(fl validator.FieldLevel) bool {
		return oAuth2CodeRegex.MatchString(fl.Field().String())
	})

	roleNameRegex := regexp.MustCompile(`^[A-Z\d_]+$`)
	web.DefaultValidator.RegisterValidation("rolename", func(fl validator.FieldLevel) bool {
		return roleNameRegex.MatchString(fl.Field().String())
	})
}
