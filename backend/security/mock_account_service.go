package security

import (
	"github.com/mkorman9/go-commons/web"
	"time"

	"github.com/stretchr/testify/mock"
)

type MockAccountService struct {
	mock.Mock
}

func (mock *MockAccountService) FindPaged(
	filters AccountsFilters,
	sortingOptions web.SortingOptions,
	paginationOptions web.PaginationOptions,
) (*AccountsPage, error) {
	m := mock.Called(filters, sortingOptions, paginationOptions)

	page := m.Get(0)
	if page == nil {
		return nil, m.Error(1)
	}

	return page.(*AccountsPage), m.Error(1)
}

func (mock *MockAccountService) GetByID(accountID string) (*Account, error) {
	m := mock.Called(accountID)

	account := m.Get(0)
	if account == nil {
		return nil, m.Error(1)
	}

	return account.(*Account), m.Error(1)
}

func (mock *MockAccountService) GetByUsername(username string) (*Account, error) {
	m := mock.Called(username)

	account := m.Get(0)
	if account == nil {
		return nil, m.Error(1)
	}

	return account.(*Account), m.Error(1)
}

func (mock *MockAccountService) GetByGithubAccountID(githubAccountID int64) (*Account, error) {
	m := mock.Called(githubAccountID)

	account := m.Get(0)
	if account == nil {
		return nil, m.Error(1)
	}

	return account.(*Account), m.Error(1)
}

func (mock *MockAccountService) RequestEmailChange(account *Account, email string) error {
	m := mock.Called(account, email)

	return m.Error(0)
}

func (mock *MockAccountService) FinishEmailChange(account *Account, requestID string) error {
	m := mock.Called(account, requestID)

	return m.Error(0)
}

func (mock *MockAccountService) ChangePassword(account *Account, oldPassword, newPassword, ip string) error {
	m := mock.Called(account, oldPassword, newPassword, ip)

	return m.Error(0)
}

func (mock *MockAccountService) ChangeUsername(account *Account, newUsername string) error {
	m := mock.Called(account, newUsername)

	return m.Error(0)
}

func (mock *MockAccountService) ChangeLanguage(account *Account, newLanguage string) error {
	m := mock.Called(account, newLanguage)

	return m.Error(0)
}

func (mock *MockAccountService) ChangeRoles(account *Account, newRoles []string) error {
	m := mock.Called(account, newRoles)

	return m.Error(0)
}

func (mock *MockAccountService) RequestPasswordReset(email string) error {
	m := mock.Called(email)

	return m.Error(0)
}

func (mock *MockAccountService) FinishPasswordReset(account *Account, code, newPassword, ip string) error {
	m := mock.Called(account, code, newPassword, ip)

	return m.Error(0)
}

func (mock *MockAccountService) LoginWithEmailPassword(email, password string) (*Account, error) {
	m := mock.Called(email, password)

	account := m.Get(0)
	if account == nil {
		return nil, m.Error(1)
	}

	return account.(*Account), m.Error(1)
}

func (mock *MockAccountService) LoginWithGithub(account *Account, userInfo *OAuth2GithubUserInfo, ip string) error {
	m := mock.Called(account, userInfo, ip)

	return m.Error(0)
}

func (mock *MockAccountService) RegisterWithEmailPassword(username, password, email, language, ip string) (*Account, error) {
	m := mock.Called(username, password, email, language, ip)

	account := m.Get(0)
	if account == nil {
		return nil, m.Error(1)
	}

	return account.(*Account), m.Error(1)
}

func (mock *MockAccountService) RegisterWithGithub(userInfo *OAuth2GithubUserInfo, language, ip string) (*Account, error) {
	m := mock.Called(userInfo, language, ip)

	account := m.Get(0)
	if account == nil {
		return nil, m.Error(1)
	}

	return account.(*Account), m.Error(1)
}

func (mock *MockAccountService) AddGithubCredentials(account *Account, userInfo *OAuth2GithubUserInfo, ip string) error {
	m := mock.Called(account, userInfo, ip)

	return m.Error(0)
}

func (mock *MockAccountService) AddEmailPasswordCredentials(account *Account, password, ip string) error {
	m := mock.Called(account, password, ip)

	return m.Error(0)
}

func (mock *MockAccountService) Activate(account *Account) error {
	m := mock.Called(account)

	return m.Error(0)
}

func (mock *MockAccountService) Ban(account *Account, duration time.Duration) error {
	m := mock.Called(account, duration)

	return m.Error(0)
}

func (mock *MockAccountService) Unban(account *Account) error {
	m := mock.Called(account)

	return m.Error(0)
}

func (mock *MockAccountService) Delete(account *Account) error {
	m := mock.Called(account)

	return m.Error(0)
}
