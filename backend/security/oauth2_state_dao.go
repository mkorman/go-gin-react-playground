package security

import (
	"errors"
	"time"
)

var ErrOAuth2StateNoResult = errors.New("no result")

type Oauth2StatesDAO interface {
	GetByID(id string) (*Oauth2State, error)

	New(duration time.Duration) (*Oauth2State, error)
	Delete(state *Oauth2State) error

	DeleteExpired() error
}
