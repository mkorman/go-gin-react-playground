package security

import (
	"bytes"
	"encoding/json"
	"github.com/mkorman9/go-commons/httpserver"
	"github.com/stretchr/testify/mock"
	"gitlab.com/mkorman/go-gin-react-playground/backend/commons_test"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/mkorman9/go-commons/logging"
	"github.com/mkorman9/go-commons/web"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestLoginWithEmailPassword(t *testing.T) {
	// given
	email := "testuser@example.com"
	password := "password123"
	account := &Account{
		ID:       "1234",
		Username: "testuser",
	}
	session := &Session{
		ID:        uuid.NewV4().String(),
		IssuedAt:  time.Now().UTC().Unix(),
		Duration:  int64(4 * time.Hour),
		ExpiresAt: time.Now().UTC().Add(4 * time.Hour).Unix(),
		IP:        "127.0.0.1",
		Roles:     []string{"ADMIN"},
		Token:     uuid.NewV4().String(),
	}
	accountServiceMock := new(MockAccountService)
	sessionServiceMock := &MockSessionService{session: nil}

	accountServiceMock.On("LoginWithEmailPassword", email, password).Return(account, nil)
	sessionServiceMock.On("Start", account, false, mock.Anything).Return(session, nil)

	api := prepareAuthAPI(accountServiceMock, sessionServiceMock)

	// when
	response := callLoginWithEmailPassword(t, api, email, password)
	body := commons_test.ReadJSONResponse[SessionStatusResponse](response)

	// then
	assert.Equal(t, http.StatusOK, response.Code, "response not expected")
	assert.Equal(t, session.ID, body.ID, "ID doesn't match")
	assert.Equal(t, account.ID, body.Subject, "subject doesn't match")
	assert.Equal(t, session.IssuedAt, body.StartTime.Unix(), "startTime doesn't match")
	assert.Equal(t, session.ExpiresAt, body.ExpiresAt.Unix(), "expiresAt doesn't match")
	assert.Equal(t, session.IP, body.LoginIP, "IP doesn't match")
	assert.Equal(t, session.Roles, body.Roles, "roles don't match")
	assert.Equal(t, session.Token, body.AccessToken, "accessToken doesn't match")
}

func TestLoginWithEmailPasswordNonExistingAccount(t *testing.T) {
	// given
	email := "testuser@example.com"
	password := "password123"
	accountServiceMock := new(MockAccountService)
	sessionServiceMock := &MockSessionService{session: nil}

	accountServiceMock.On("LoginWithEmailPassword", email, password).Return(nil, ErrAccountNoResult)

	api := prepareAuthAPI(accountServiceMock, sessionServiceMock)

	// when
	response := callLoginWithEmailPassword(t, api, email, password)
	body := commons_test.ReadJSONResponse[web.GenericResponse](response)

	// then
	assert.Equal(t, http.StatusUnauthorized, response.Code, "response not expected")
	commons_test.AssertCause(t, &body, "credentials", "invalid")
}

func TestLoginWithEmailPasswordInvalidCredentials(t *testing.T) {
	// given
	email := "testuser@example.com"
	password := "password123"
	accountServiceMock := new(MockAccountService)
	sessionServiceMock := &MockSessionService{session: nil}

	accountServiceMock.On("LoginWithEmailPassword", email, password).Return(nil, ErrAccountInvalidCredentials)

	api := prepareAuthAPI(accountServiceMock, sessionServiceMock)

	// when
	response := callLoginWithEmailPassword(t, api, email, password)
	body := commons_test.ReadJSONResponse[web.GenericResponse](response)

	// then
	assert.Equal(t, http.StatusUnauthorized, response.Code, "response not expected")
	commons_test.AssertCause(t, &body, "credentials", "invalid")
}

func TestLoginWithEmailPasswordAccountInactive(t *testing.T) {
	// given
	email := "testuser@example.com"
	password := "password123"
	accountServiceMock := new(MockAccountService)
	sessionServiceMock := &MockSessionService{session: nil}

	accountServiceMock.On("LoginWithEmailPassword", email, password).Return(nil, ErrAccountInactive)

	api := prepareAuthAPI(accountServiceMock, sessionServiceMock)

	// when
	response := callLoginWithEmailPassword(t, api, email, password)
	body := commons_test.ReadJSONResponse[web.GenericResponse](response)

	// then
	assert.Equal(t, http.StatusUnauthorized, response.Code, "response not expected")
	commons_test.AssertCause(t, &body, "account", "inactive")
}

func TestLoginWithEmailPasswordNoCredentials(t *testing.T) {
	// given
	email := "testuser@example.com"
	password := "password123"
	accountServiceMock := new(MockAccountService)
	sessionServiceMock := &MockSessionService{session: nil}

	accountServiceMock.On("LoginWithEmailPassword", email, password).Return(nil, ErrAccountNoEmailPasswordCredentials)

	api := prepareAuthAPI(accountServiceMock, sessionServiceMock)

	// when
	response := callLoginWithEmailPassword(t, api, email, password)
	body := commons_test.ReadJSONResponse[web.GenericResponse](response)

	// then
	assert.Equal(t, http.StatusUnauthorized, response.Code, "response not expected")
	commons_test.AssertCause(t, &body, "credentials", "invalid")
}

func init() {
	gin.SetMode(gin.TestMode)
	logging.Setup()
}

func prepareAuthAPI(accountService AccountService, sessionService SessionService) *gin.Engine {
	s := httpserver.NewServer()
	AuthAPI(s.Engine, accountService, sessionService)
	return s.Engine
}

func callLoginWithEmailPassword(t *testing.T, api *gin.Engine, email, password string) *commons_test.TestHttpResponse {
	requestJSON, _ := json.Marshal(&EmailPasswordAuthRequest{
		Email:    email,
		Password: password,
	})

	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/api/v1/session/password", bytes.NewReader(requestJSON))

	api.ServeHTTP(recorder, req)

	return &commons_test.TestHttpResponse{t, recorder.Code, recorder.Body}
}
