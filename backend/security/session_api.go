package security

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/mkorman9/go-commons/web"
)

type SessionStatusResponse struct {
	ID          string    `json:"id"`
	StartTime   time.Time `json:"startTime"`
	ExpiresAt   time.Time `json:"expiresAt"`
	Subject     string    `json:"subject"`
	LoginIP     string    `json:"loginIp,omitempty"`
	Roles       []string  `json:"roles"`
	AccessToken string    `json:"accessToken"`
}

func SessionAPI(engine *gin.Engine, sessionService SessionService) {
	cookieAuthMiddleware := sessionService.CookieMiddleware()
	tokenAuthMiddleware := sessionService.BearerTokenMiddleware()
	group := engine.Group("/api/v1/session")

	group.GET("",
		cookieAuthMiddleware.AnyAuthenticated(),
		func(c *gin.Context) {
			session := MustGetActiveSession(c)

			response := SessionStatusResponse{
				ID:          session.ID,
				StartTime:   time.Unix(session.IssuedAt, 0).UTC(),
				ExpiresAt:   time.Unix(session.ExpiresAt, 0).UTC(),
				Subject:     session.AccountID,
				LoginIP:     session.IP,
				Roles:       session.Roles,
				AccessToken: session.Token,
			}

			c.JSON(http.StatusOK, response)
		},
	)

	group.PUT("",
		tokenAuthMiddleware.AnyAuthenticated(),
		func(c *gin.Context) {
			session := MustGetActiveSession(c)

			if err := sessionService.Refresh(session); err != nil {
				web.InternalError(c, err, "Unexpected error while refreshing session")
				return
			}

			SendSessionCookie(c, session)
			c.JSON(http.StatusOK, &SessionStatusResponse{
				ID:          session.ID,
				StartTime:   time.Unix(session.IssuedAt, 0).UTC(),
				ExpiresAt:   time.Unix(session.ExpiresAt, 0).UTC(),
				Subject:     session.AccountID,
				LoginIP:     session.IP,
				Roles:       session.Roles,
				AccessToken: session.Token,
			})
		},
	)

	group.DELETE("",
		tokenAuthMiddleware.AnyAuthenticated(),
		func(c *gin.Context) {
			session := MustGetActiveSession(c)

			if err := sessionService.Revoke(session); err != nil {
				web.InternalError(c, err, "Unexpected error while revoking session")
				return
			}

			SendSessionCookie(c, nil)
			web.SuccessResponse(c, "OK")
		},
	)
}
