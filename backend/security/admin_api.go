package security

import (
	"errors"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/mkorman9/go-commons/web"
)

type AdminImpersonateRequest struct {
	AccountID string `json:"accountId" validate:"required,uuid4"`
}

type AdminSetRolesRequest struct {
	AccountID string   `json:"accountId" validate:"required,uuid4"`
	Roles     []string `json:"roles" validate:"required,dive,rolename"`
}

type AccountsInfoPage struct {
	web.Page
	Data []*AccountInfoResponse `json:"data"`
}

func AdminAPI(engine *gin.Engine, accountService AccountService, sessionService SessionService) {
	tokenAuthMiddleware := sessionService.BearerTokenMiddleware()
	group := engine.Group("/api/v1/admin")

	group.GET("/accounts",
		tokenAuthMiddleware.AnyOfRoles("PERMISSIONS_ADMIN"),
		func(c *gin.Context) {
			page := c.Query("page")
			pageSize := c.Query("pageSize")
			sortBy := c.Query("sortBy")
			_, sortReverse := c.GetQuery("sortReverse")
			filtersRaw := c.QueryMap("filter")

			filters, causes := parseAccountsFilters(filtersRaw)
			if causes != nil {
				web.ValidationError(c, causes...)
				return
			}

			sortingOptions, causes := parseAccountsSortBy(sortBy, sortReverse)
			if causes != nil {
				web.ValidationError(c, causes...)
				return
			}

			paginationOptions := parseAccountsPaginationOptions(page, pageSize)

			accountsPage, err := accountService.FindPaged(*filters, *sortingOptions, *paginationOptions)
			if err != nil {
				web.ErrorResponse(c, http.StatusInternalServerError, "Unexpected error while retrieving accounts")
				return
			}

			accountsInfoPage := AccountsInfoPage{Page: accountsPage.Page}
			for _, account := range accountsPage.Data {
				var bannedUntil *time.Time
				if account.IsBanned() {
					b := time.Unix(account.BannedUntil, 0).UTC()
					bannedUntil = &b
				}

				accountInfo := AccountInfoResponse{
					ID:           account.ID,
					Username:     account.Username,
					IsActive:     account.IsActive,
					IsBanned:     account.IsBanned(),
					BannedUntil:  bannedUntil,
					Roles:        account.Roles,
					Email:        account.Email(),
					Language:     account.Language,
					RegisteredAt: time.Unix(account.CreatedAt, 0).UTC().Format(time.RFC3339),
					LoginMethods: AccountInfoResponseLoginMethods{
						EmailAndPassword: AccountInfoResponseLoginMethodMetadata{
							IsDefined: account.HasActiveEmailPasswordCredentials(),
						},
						Github: AccountInfoResponseLoginMethodMetadata{
							IsDefined: account.HasActiveGithubCredentials(),
						},
					},
				}

				accountsInfoPage.Data = append(accountsInfoPage.Data, &accountInfo)
			}

			c.JSON(http.StatusOK, accountsInfoPage)
		},
	)

	group.POST("/impersonate",
		tokenAuthMiddleware.AnyOfRoles("PERMISSIONS_ADMIN"),
		func(c *gin.Context) {
			var adminImpersonateRequest AdminImpersonateRequest
			if err := c.ShouldBindJSON(&adminImpersonateRequest); err != nil {
				web.ValidationError(c, web.FieldErrorMessage("body", "json", "Request body needs to be a valid JSON"))
				return
			}

			if ok, causes := web.ValidateStruct(&adminImpersonateRequest); !ok {
				web.ValidationError(c, causes...)
				return
			}

			targetAccount, err := accountService.GetByID(adminImpersonateRequest.AccountID)
			if err != nil {
				if errors.Is(err, ErrAccountNoResult) {
					web.ErrorResponse(c, http.StatusBadRequest, "Account with this AccountID doesn't exist", web.FieldError("account", "not_found"))
					return
				}

				web.ErrorResponse(c, http.StatusInternalServerError, "Unexpected error while retrieving account")
				return
			}

			adminSession := MustGetActiveSession(c)
			if err := sessionService.Revoke(adminSession); err != nil {
				web.ErrorResponse(c, http.StatusInternalServerError, "Unexpected error while revoking previous session")
				return
			}

			impersonatedSession, err := sessionService.Start(targetAccount, true, c.ClientIP())
			if err != nil {
				web.ErrorResponse(c, http.StatusInternalServerError, "Unexpected error while creating impersonated session")
				return
			}

			SendSessionCookie(c, impersonatedSession)
			c.JSON(http.StatusOK, &SessionStatusResponse{
				ID:          impersonatedSession.ID,
				StartTime:   time.Unix(impersonatedSession.IssuedAt, 0),
				ExpiresAt:   time.Unix(impersonatedSession.ExpiresAt, 0),
				Subject:     targetAccount.ID,
				LoginIP:     impersonatedSession.IP,
				Roles:       impersonatedSession.Roles,
				AccessToken: impersonatedSession.Token,
			})
		},
	)

	group.POST("/set/roles",
		tokenAuthMiddleware.AnyOfRoles("PERMISSIONS_ADMIN"),
		func(c *gin.Context) {
			var adminSetRolesRequest AdminSetRolesRequest
			if err := c.ShouldBindJSON(&adminSetRolesRequest); err != nil {
				web.ValidationError(c, web.FieldErrorMessage("body", "json", "Request body needs to be a valid JSON"))
				return
			}

			if ok, causes := web.ValidateStruct(&adminSetRolesRequest); !ok {
				web.ValidationError(c, causes...)
				return
			}

			targetAccount, err := accountService.GetByID(adminSetRolesRequest.AccountID)
			if err != nil {
				if errors.Is(err, ErrAccountNoResult) {
					web.ErrorResponse(c, http.StatusBadRequest, "Account with this AccountID doesn't exist", web.FieldError("account", "not_found"))
					return
				}

				web.ErrorResponse(c, http.StatusInternalServerError, "Unexpected error while retrieving account")
				return
			}

			if err := accountService.ChangeRoles(targetAccount, adminSetRolesRequest.Roles); err != nil {
				web.ErrorResponse(c, http.StatusInternalServerError, "Unexpected error while changing account roles")
				return
			}

			web.SuccessResponse(c, "OK")
		},
	)

	group.POST("/account/ban/:accountID",
		tokenAuthMiddleware.AnyOfRoles("PERMISSIONS_ADMIN"),
		func(c *gin.Context) {
			accountID := c.Param("accountID")
			if ok, causes := web.ValidateVar("accountID", accountID, "required,uuid4"); !ok {
				web.ValidationError(c, causes...)
				return
			}

			targetAccount, err := accountService.GetByID(accountID)
			if err != nil {
				if errors.Is(err, ErrAccountNoResult) {
					web.ErrorResponse(c, http.StatusBadRequest, "Account with this AccountID doesn't exist", web.FieldError("account", "not_found"))
					return
				}

				web.ErrorResponse(c, http.StatusInternalServerError, "Unexpected error while retrieving account")
				return
			}

			if err := accountService.Ban(targetAccount, 24*7*time.Hour); err != nil {
				web.ErrorResponse(c, http.StatusInternalServerError, "Unexpected error while banning account")
				return
			}

			web.SuccessResponse(c, "OK")
		},
	)

	group.POST("/account/unban/:accountID",
		tokenAuthMiddleware.AnyOfRoles("PERMISSIONS_ADMIN"),
		func(c *gin.Context) {
			accountID := c.Param("accountID")
			if ok, causes := web.ValidateVar("accountID", accountID, "required,uuid4"); !ok {
				web.ValidationError(c, causes...)
				return
			}

			targetAccount, err := accountService.GetByID(accountID)
			if err != nil {
				if err == ErrAccountNoResult {
					web.ErrorResponse(c, http.StatusBadRequest, "Account with this AccountID doesn't exist", web.FieldError("account", "not_found"))
					return
				}

				web.ErrorResponse(c, http.StatusInternalServerError, "Unexpected error while retrieving account")
				return
			}

			if err := accountService.Unban(targetAccount); err != nil {
				web.ErrorResponse(c, http.StatusInternalServerError, "Unexpected error while unbanning account")
				return
			}

			web.SuccessResponse(c, "OK")
		},
	)

	group.DELETE("/account/delete/:accountID",
		tokenAuthMiddleware.AnyOfRoles("PERMISSIONS_ADMIN"),
		func(c *gin.Context) {
			accountID := c.Param("accountID")
			if ok, causes := web.ValidateVar("accountID", accountID, "required,uuid4"); !ok {
				web.ValidationError(c, causes...)
				return
			}

			targetAccount, err := accountService.GetByID(accountID)
			if err != nil {
				if err == ErrAccountNoResult {
					web.ErrorResponse(c, http.StatusBadRequest, "Account with this AccountID doesn't exist", web.FieldError("account", "not_found"))
					return
				}

				web.ErrorResponse(c, http.StatusInternalServerError, "Unexpected error while retrieving account")
				return
			}

			if err := accountService.Delete(targetAccount); err != nil {
				web.ErrorResponse(c, http.StatusInternalServerError, "Unexpected error while deleting account")
				return
			}

			web.SuccessResponse(c, "OK")
		},
	)
}
