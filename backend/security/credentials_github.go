package security

import "time"

type CredentialsGithub struct {
	GithubAccountID int64  `json:"githubAccountId" firestore:"githubAccountId"`
	Email           string `json:"email" firestore:"email"`
	Username        string `json:"username" firestore:"username"`
	ProfileURL      string `json:"profileUrl" firestore:"profileUrl"`
	AccessToken     string `json:"accessToken" firestore:"accessToken"`
	LastAccessTime  int64  `json:"lastAccessTime" firestore:"lastAccessTime"`
	LastAccessIP    string `json:"lastAccessIp" firestore:"lastAccessIp"`
}

func (credentials *CredentialsGithub) Access(userInfo *OAuth2GithubUserInfo, initiatorIP string) {
	credentials.Email = userInfo.Email
	credentials.Username = userInfo.Username
	credentials.ProfileURL = userInfo.ProfileURL
	credentials.AccessToken = userInfo.AccessToken
	credentials.LastAccessTime = time.Now().UTC().Unix()
	credentials.LastAccessIP = initiatorIP
}
