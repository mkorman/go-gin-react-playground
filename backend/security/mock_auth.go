package security

import (
	"github.com/gin-gonic/gin"
	"github.com/mkorman9/go-commons/httpauth"
)

type MockedAuthMiddlewaresProvider struct {
	session *Session
	account *Account
}

func NewMockedAuthMiddlewaresProvider(session *Session, account *Account) AuthMiddlewaresProvider {
	return &MockedAuthMiddlewaresProvider{session: session, account: account}
}

func (provider *MockedAuthMiddlewaresProvider) CookieMiddleware() *httpauth.Middleware {
	middleware := httpauth.NewSessionCookieMiddleware(
		sessionCookieName,
		func(c *gin.Context, _ string) (*httpauth.VerificationResult, error) {
			c.Set(activeSessionKey, provider.session)
			c.Set(activeAccountKey, provider.account)

			return &httpauth.VerificationResult{
				Verified: true,
				Roles:    provider.session.Roles,
			}, nil
		},
	)

	return &middleware
}

func (provider *MockedAuthMiddlewaresProvider) BearerTokenMiddleware() *httpauth.Middleware {
	middleware := httpauth.NewBearerTokenMiddleware(
		func(c *gin.Context, _ string) (*httpauth.VerificationResult, error) {
			c.Set(activeSessionKey, provider.session)
			c.Set(activeAccountKey, provider.account)

			return &httpauth.VerificationResult{
				Verified: true,
				Roles:    provider.session.Roles,
			}, nil
		},
	)

	return &middleware
}
