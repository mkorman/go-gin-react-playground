package security

import (
	"cloud.google.com/go/firestore"
	"context"
	uuid "github.com/satori/go.uuid"
	"google.golang.org/api/iterator"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"time"
)

const sessionsCollection = "sessions"

type FirestoreSessionDAO struct {
	FS *firestore.Client
}

func (dao *FirestoreSessionDAO) FindByID(id string) (*Session, error) {
	doc, err := dao.FS.Collection(sessionsCollection).
		Doc(id).
		Get(context.Background())
	if err != nil {
		if status.Code(err) == codes.NotFound {
			return nil, ErrSessionNoResult
		}

		return nil, err
	}

	var session Session
	err = doc.DataTo(&session)
	if err != nil {
		return nil, err
	}

	return &session, nil
}

func (dao *FirestoreSessionDAO) FindByToken(token string) (*Session, error) {
	docs := dao.FS.Collection(sessionsCollection).
		Where("token", "==", token).
		Where("expiresAt", ">", time.Now().UTC().Unix()).
		Documents(context.Background())
	defer docs.Stop()

	doc, err := docs.Next()
	if err != nil {
		if err == iterator.Done {
			return nil, ErrSessionNoResult
		}

		return nil, err
	}

	var session Session
	err = doc.DataTo(&session)
	if err != nil {
		return nil, err
	}

	return &session, nil
}

func (dao *FirestoreSessionDAO) Insert(session *Session) error {
	session.ID = uuid.NewV4().String()

	_, err := dao.FS.Collection(sessionsCollection).
		Doc(session.ID).
		Set(context.Background(), session)
	if err != nil {
		return err
	}

	return nil
}

func (dao *FirestoreSessionDAO) Update(session *Session) error {
	_, err := dao.FS.Collection(sessionsCollection).
		Doc(session.ID).
		Set(context.Background(), session)
	if err != nil {
		return err
	}

	return nil
}

func (dao *FirestoreSessionDAO) Delete(session *Session) error {
	_, err := dao.FS.Collection(sessionsCollection).
		Doc(session.ID).
		Delete(context.Background())
	if err != nil {
		return err
	}

	return nil
}

func (dao *FirestoreSessionDAO) DeleteExpired() error {
	docs := dao.FS.Collection(sessionsCollection).
		Where("expiresAt", "<", time.Now().UTC().Unix()).
		Documents(context.Background())
	defer docs.Stop()

	batch := dao.FS.Batch()
	for {
		doc, err := docs.Next()
		if err != nil {
			if err == iterator.Done {
				break
			}

			return err
		}

		batch.Delete(doc.Ref)
	}

	_, err := batch.Commit(context.Background())
	if err != nil {
		return err
	}

	return nil
}
