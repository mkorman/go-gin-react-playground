package security

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/mkorman9/go-commons/web"
	"net/http"
	"time"
)

type EmailPasswordAuthRequest struct {
	Email    string `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
}

func AuthAPI(engine *gin.Engine, accountService AccountService, sessionService SessionService) {
	group := engine.Group("/api/v1/session/password")

	group.POST("",
		func(c *gin.Context) {
			var authRequest EmailPasswordAuthRequest
			if ok, causes := web.BindJSONBody(c, &authRequest); !ok {
				web.ValidationError(c, causes...)
				return
			}

			_, rememberMe := c.GetQuery("rememberMe")

			account, err := accountService.LoginWithEmailPassword(authRequest.Email, authRequest.Password)
			if err != nil {
				if errors.Is(err, ErrAccountNoResult) {
					web.ErrorResponse(c,
						http.StatusUnauthorized,
						"Invalid credentials",
						web.FieldError("credentials", "invalid"),
					)
					return
				} else if errors.Is(err, ErrAccountInvalidCredentials) {
					web.ErrorResponse(c,
						http.StatusUnauthorized,
						"Invalid credentials",
						web.FieldError("credentials", "invalid"),
					)
					return
				} else if errors.Is(err, ErrAccountInactive) {
					web.ErrorResponse(c,
						http.StatusUnauthorized,
						"Account is inactive",
						web.FieldError("account", "inactive"),
					)
					return
				} else if errors.Is(err, ErrAccountNoEmailPasswordCredentials) {
					web.ErrorResponse(c,
						http.StatusUnauthorized,
						"Invalid username or password",
						web.FieldError("credentials", "invalid"),
					)
					return
				}

				web.InternalError(c, err, "Unexpected error while logging in with email")
				return
			}

			session, err := sessionService.Start(account, rememberMe, c.ClientIP())
			if err != nil {
				web.InternalError(c, err, "Could not start session")
				return
			}

			SendSessionCookie(c, session)
			c.JSON(http.StatusOK, &SessionStatusResponse{
				ID:          session.ID,
				StartTime:   time.Unix(session.IssuedAt, 0).UTC(),
				ExpiresAt:   time.Unix(session.ExpiresAt, 0).UTC(),
				Subject:     account.ID,
				LoginIP:     session.IP,
				Roles:       session.Roles,
				AccessToken: session.Token,
			})
		},
	)
}
