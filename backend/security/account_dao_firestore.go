package security

import (
	"cloud.google.com/go/firestore"
	"context"
	"github.com/mkorman9/go-commons/web"
	"google.golang.org/api/iterator"
)

var accountsCollection = "accounts"

type FirestoreAccountDAO struct {
	FS *firestore.Client
}

func (dao *FirestoreAccountDAO) FindPaged(
	filters AccountsFilters,
	sortingOptions web.SortingOptions,
	paginationOptions web.PaginationOptions,
) (*AccountsPage, error) {
	return nil, nil
}

func (dao *FirestoreAccountDAO) FindByID(id string) (*Account, error) {
	docs := dao.FS.Collection(accountsCollection).
		Where("id", "==", id).
		Where("deleted", "==", false).
		Documents(context.Background())
	defer docs.Stop()

	var doc *firestore.DocumentSnapshot
	for {
		d, err := docs.Next()
		if err != nil {
			if err == iterator.Done {
				break
			}

			return nil, err
		}

		doc = d
	}

	if doc == nil {
		return nil, ErrAccountNoResult
	}

	var account Account
	err := doc.DataTo(&account)
	if err != nil {
		return nil, err
	}

	return &account, nil
}

func (dao *FirestoreAccountDAO) FindByUsername(username string) (*Account, error) {
	docs := dao.FS.Collection(accountsCollection).
		Where("username", "==", username).
		Where("deleted", "==", false).
		Documents(context.Background())
	defer docs.Stop()

	var doc *firestore.DocumentSnapshot
	for {
		d, err := docs.Next()
		if err != nil {
			if err == iterator.Done {
				break
			}

			return nil, err
		}

		doc = d
	}

	if doc == nil {
		return nil, ErrAccountNoResult
	}

	var account Account
	err := doc.DataTo(&account)
	if err != nil {
		return nil, err
	}

	return &account, nil
}

func (dao *FirestoreAccountDAO) FindByPasswordCredentialsEmail(email string) (*Account, error) {
	docs := dao.FS.Collection(accountsCollection).
		WherePath(firestore.FieldPath{"credentials", "emailPassword", "email"}, "==", email).
		Where("deleted", "==", false).
		Documents(context.Background())
	defer docs.Stop()

	var doc *firestore.DocumentSnapshot
	for {
		d, err := docs.Next()
		if err != nil {
			if err == iterator.Done {
				break
			}

			return nil, err
		}

		doc = d
	}

	if doc == nil {
		return nil, ErrAccountNoResult
	}

	var account Account
	err := doc.DataTo(&account)
	if err != nil {
		return nil, err
	}

	return &account, nil
}

func (dao *FirestoreAccountDAO) FindByGithubAccountID(id int64) (*Account, error) {
	return nil, nil
}

func (dao *FirestoreAccountDAO) Insert(account *Account) (string, error) {
	return "", nil
}

func (dao *FirestoreAccountDAO) Update(account *Account) error {
	return nil
}

func (dao *FirestoreAccountDAO) Delete(account *Account) error {
	return nil
}

func (dao *FirestoreAccountDAO) FindEmailChangeRequest(requestID, accountID string) (*AccountEmailChangeRequest, error) {
	return nil, nil
}

func (dao *FirestoreAccountDAO) InsertEmailChangeRequest(request *AccountEmailChangeRequest) (string, error) {
	return "", nil
}

func (dao *FirestoreAccountDAO) DeleteEmailChangeRequest(request *AccountEmailChangeRequest) error {
	return nil
}
