package security

import (
	"github.com/mkorman9/go-commons/httpserver"
	"gitlab.com/mkorman/go-gin-react-playground/backend/commons_test"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/mkorman9/go-commons/logging"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestGetSessionStatus(t *testing.T) {
	// given
	session := &Session{
		ID:        uuid.NewV4().String(),
		IssuedAt:  time.Now().UTC().Unix(),
		Duration:  int64(4 * time.Hour),
		ExpiresAt: time.Now().UTC().Add(4 * time.Hour).Unix(),
		IP:        "127.0.0.1",
		Roles:     []string{"ADMIN"},
		Token:     uuid.NewV4().String(),
	}
	sessionServiceMock := &MockSessionService{
		session: session,
	}

	api := prepareSessionAPI(sessionServiceMock)

	// when
	response := callGetSessionStatus(t, api)
	body := commons_test.ReadJSONResponse[SessionStatusResponse](response)

	// then
	assert.Equal(t, http.StatusOK, response.Code, "response not expected")
	assert.Equal(t, session.ID, body.ID, "ID doesn't match")
	assert.Equal(t, session.IssuedAt, body.StartTime.Unix(), "startTime doesn't match")
	assert.Equal(t, session.ExpiresAt, body.ExpiresAt.Unix(), "expiresAt doesn't match")
	assert.Equal(t, session.IP, body.LoginIP, "IP doesn't match")
	assert.Equal(t, session.Roles, body.Roles, "roles don't match")
	assert.Equal(t, session.Token, body.AccessToken, "accessToken doesn't match")
}

func TestRefreshSession(t *testing.T) {
	// given
	session := &Session{
		ID:        uuid.NewV4().String(),
		IssuedAt:  time.Now().UTC().Unix(),
		Duration:  int64(4 * time.Hour),
		ExpiresAt: time.Now().UTC().Add(4 * time.Hour).Unix(),
		IP:        "127.0.0.1",
		Roles:     []string{"ADMIN"},
		Token:     uuid.NewV4().String(),
	}
	sessionServiceMock := &MockSessionService{
		session: session,
	}

	sessionServiceMock.On("Refresh", session).Return(nil)

	api := prepareSessionAPI(sessionServiceMock)

	// when
	response := callRefreshSession(t, api)
	body := commons_test.ReadJSONResponse[SessionStatusResponse](response)

	// then
	assert.Equal(t, http.StatusOK, response.Code, "response not expected")
	assert.Equal(t, session.ID, body.ID, "ID doesn't match")
	assert.Equal(t, session.IssuedAt, body.StartTime.Unix(), "startTime doesn't match")
	assert.Equal(t, session.ExpiresAt, body.ExpiresAt.Unix(), "expiresAt doesn't match")
	assert.Equal(t, session.IP, body.LoginIP, "IP doesn't match")
	assert.Equal(t, session.Roles, body.Roles, "roles don't match")
	assert.Equal(t, session.Token, body.AccessToken, "accessToken doesn't match")
	sessionServiceMock.AssertCalled(t, "Refresh", session)
}

func TestRevokeSession(t *testing.T) {
	// given
	session := &Session{}
	sessionServiceMock := &MockSessionService{
		session: session,
	}

	sessionServiceMock.On("Revoke", session).Return(nil)

	api := prepareSessionAPI(sessionServiceMock)

	// when
	response := callRevokeSession(t, api)

	// then
	assert.Equal(t, http.StatusOK, response.Code, "response not expected")
	sessionServiceMock.AssertCalled(t, "Revoke", session)
}

func init() {
	gin.SetMode(gin.TestMode)
	logging.Setup()
}

func prepareSessionAPI(sessionService SessionService) *gin.Engine {
	s := httpserver.NewServer()
	SessionAPI(s.Engine, sessionService)
	return s.Engine
}

func callGetSessionStatus(t *testing.T, api *gin.Engine) *commons_test.TestHttpResponse {
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/v1/session", nil)

	api.ServeHTTP(recorder, req)

	return &commons_test.TestHttpResponse{t, recorder.Code, recorder.Body}
}

func callRefreshSession(t *testing.T, api *gin.Engine) *commons_test.TestHttpResponse {
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("PUT", "/api/v1/session", nil)

	api.ServeHTTP(recorder, req)

	return &commons_test.TestHttpResponse{t, recorder.Code, recorder.Body}
}

func callRevokeSession(t *testing.T, api *gin.Engine) *commons_test.TestHttpResponse {
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", "/api/v1/session", nil)

	api.ServeHTTP(recorder, req)

	return &commons_test.TestHttpResponse{t, recorder.Code, recorder.Body}
}
