package security

import "errors"

var ErrSessionNoResult = errors.New("no result")

type SessionDAO interface {
	FindByID(id string) (*Session, error)
	FindByToken(token string) (*Session, error)

	Insert(session *Session) error
	Update(session *Session) error
	Delete(session *Session) error

	DeleteExpired() error
}
