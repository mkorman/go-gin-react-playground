package security

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"

	"github.com/mkorman9/go-commons/requests"
	"github.com/rs/zerolog/log"
)

type OAuth2GithubProvider interface {
	IsEnabled() bool
	ClientID() string
	FinishCodeFlow(code string) (*OAuth2GithubUserInfo, error)
}

type oAuth2GithubProvider struct {
	enabled      bool
	clientID     string
	clientSecret string
	redirectURL  string
	client       *requests.Client
}

type OAuth2GithubUserInfo struct {
	AccountID   int64
	Username    string
	ProfileURL  string
	AccessToken string
	Email       string
}

type OAuth2GithubUserInfoResponse struct {
	ID    int64  `json:"id"`
	Login string `json:"login"`
	URL   string `json:"html_url"`
	Email string `json:"email"`
}

type OAuth2GithubUserEmailsResponse struct {
	Email    string `json:"email"`
	Verified bool   `json:"verified"`
	Primary  bool   `json:"primary"`
}

func (provider *oAuth2GithubProvider) IsEnabled() bool {
	return provider.enabled
}

func (provider *oAuth2GithubProvider) ClientID() string {
	return provider.clientID
}

func (provider *oAuth2GithubProvider) FinishCodeFlow(code string) (*OAuth2GithubUserInfo, error) {
	token, err := provider.callForAccessToken(code)
	if err != nil {
		log.Error().Err(err).Msg("Failed to retrieve access token from Github")
		return nil, err
	}

	userInfo, err := provider.callForUserInfo(token)
	if err != nil {
		log.Error().Err(err).Msg("Failed to retrieve user info from Github")
		return nil, err
	}

	userEmails, err := provider.callForUserEmails(token)
	if err != nil {
		log.Error().Err(err).Msg("Failed to retrieve user emails from Github")
		return nil, err
	}

	primaryEmail := provider.chooseUserEmail(userInfo, userEmails)
	if primaryEmail == "" {
		log.Error().Err(err).Msg("No verified emails defined for Github user")
		return nil, errors.New("no verified emails defined for user")
	}

	ret := &OAuth2GithubUserInfo{
		AccountID:   userInfo.ID,
		Username:    userInfo.Login,
		ProfileURL:  userInfo.URL,
		AccessToken: token,
		Email:       primaryEmail,
	}
	return ret, nil
}

func (provider *oAuth2GithubProvider) callForAccessToken(code string) (string, error) {
	request, err := requests.NewRequest(
		requests.POST,
		requests.URL("https://github.com/login/oauth/access_token"),
		requests.Header("Accept", "application/json"),
		requests.FormBody(&url.Values{
			"client_id":     []string{provider.clientID},
			"client_secret": []string{provider.clientSecret},
			"code":          []string{code},
			"redirect_uri":  []string{provider.redirectURL},
		}),
	)
	if err != nil {
		return "", err
	}

	response, err := provider.client.Send(request)
	if err != nil {
		return "", err
	}
	if response.StatusCode != http.StatusOK {
		return "", errors.New("status code not ok")
	}

	var responseFields map[string]string
	if err := requests.BindResponseJSON(response, &responseFields); err != nil {
		return "", err
	}

	return responseFields["access_token"], nil
}

func (provider *oAuth2GithubProvider) callForUserInfo(token string) (*OAuth2GithubUserInfoResponse, error) {
	request, err := requests.NewRequest(
		requests.GET,
		requests.URL("https://api.github.com/user"),
		requests.Header("Authorization", fmt.Sprintf("token %s", token)),
	)
	if err != nil {
		return nil, err
	}

	response, err := provider.client.Send(request)
	if err != nil {
		return nil, err
	}
	if response.StatusCode != http.StatusOK {
		return nil, errors.New("status code not ok")
	}

	var responseBody OAuth2GithubUserInfoResponse
	if err := requests.BindResponseJSON(response, &responseBody); err != nil {
		return nil, err
	}

	return &responseBody, nil
}

func (provider *oAuth2GithubProvider) callForUserEmails(token string) ([]*OAuth2GithubUserEmailsResponse, error) {
	request, err := requests.NewRequest(
		requests.GET,
		requests.URL("https://api.github.com/user/public_emails"),
		requests.Header("Authorization", fmt.Sprintf("token %s", token)),
	)
	if err != nil {
		return nil, err
	}

	response, err := provider.client.Send(request)
	if err != nil {
		return nil, err
	}
	if response.StatusCode != http.StatusOK {
		return nil, errors.New("status code not ok")
	}

	var responseBody []*OAuth2GithubUserEmailsResponse
	if err := requests.BindResponseJSON(response, &responseBody); err != nil {
		return nil, err
	}

	return responseBody, nil
}

func (provider *oAuth2GithubProvider) chooseUserEmail(userInfo *OAuth2GithubUserInfoResponse, userEmails []*OAuth2GithubUserEmailsResponse) string {
	if userInfo.Email != "" {
		return userInfo.Email
	}

	var email string
	for _, e := range userEmails {
		if e.Primary && e.Verified {
			email = e.Email
			break
		} else if e.Verified {
			email = e.Email
		}
	}

	return email
}
