package security

import (
	"github.com/rs/zerolog/log"
)

func CreateExpiredSessionsScheduler(sessionService SessionService) func() {
	return func() {
		log.Info().Msg("Expired Session scheduler has been triggered")

		err := sessionService.DeleteExpired()
		if err != nil {
			log.Error().Msgf("Error while deleting expired sessions: %v", err)
		}

		log.Info().Msg("Successfully executed Expired Session scheduler")
	}
}
