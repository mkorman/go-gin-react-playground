package security

import (
	"github.com/mkorman9/go-commons/httpauth"
	"github.com/stretchr/testify/mock"
)

type MockSessionService struct {
	mock.Mock
	session *Session
	account *Account
}

func (mock *MockSessionService) CookieMiddleware() *httpauth.Middleware {
	provider := &MockedAuthMiddlewaresProvider{mock.session, mock.account}
	return provider.CookieMiddleware()
}

func (mock *MockSessionService) BearerTokenMiddleware() *httpauth.Middleware {
	provider := &MockedAuthMiddlewaresProvider{mock.session, mock.account}
	return provider.BearerTokenMiddleware()
}

func (mock *MockSessionService) Start(account *Account, prolongDuration bool, ip string) (*Session, error) {
	m := mock.Called(account, prolongDuration, ip)
	return m.Get(0).(*Session), m.Error(1)
}

func (mock *MockSessionService) Refresh(session *Session) error {
	m := mock.Called(session)
	return m.Error(0)
}

func (mock *MockSessionService) Revoke(session *Session) error {
	m := mock.Called(session)
	return m.Error(0)
}

func (mock *MockSessionService) DeleteExpired() error {
	m := mock.Called()
	return m.Error(0)
}
