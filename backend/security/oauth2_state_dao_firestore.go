package security

import (
	"cloud.google.com/go/firestore"
	"context"
	uuid "github.com/satori/go.uuid"
	"google.golang.org/api/iterator"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"time"
)

var oauth2StatesCollection = "oauth2states"

type FirestoreOAuth2StatesDAO struct {
	FS *firestore.Client
}

func (dao *FirestoreOAuth2StatesDAO) GetByID(id string) (*Oauth2State, error) {
	doc, err := dao.FS.Collection(oauth2StatesCollection).
		Doc(id).
		Get(context.Background())
	if err != nil {
		if status.Code(err) == codes.NotFound {
			return nil, ErrOAuth2StateNoResult
		}

		return nil, err
	}

	var state Oauth2State
	err = doc.DataTo(&state)
	if err != nil {
		return nil, err
	}

	return &state, nil
}

func (dao *FirestoreOAuth2StatesDAO) New(duration time.Duration) (*Oauth2State, error) {
	state := &Oauth2State{
		ID:        uuid.NewV4().String(),
		CreatedAt: time.Now().UTC().Unix(),
		ExpiresAt: time.Now().UTC().Add(duration).Unix(),
	}

	_, err := dao.FS.Collection(oauth2StatesCollection).
		Doc(state.ID).
		Set(context.Background(), state)
	if err != nil {
		return nil, err
	}

	return state, nil
}

func (dao *FirestoreOAuth2StatesDAO) Delete(state *Oauth2State) error {
	_, err := dao.FS.Collection(oauth2StatesCollection).
		Doc(state.ID).
		Delete(context.Background())
	if err != nil {
		if status.Code(err) == codes.NotFound {
			return ErrOAuth2StateNoResult
		}

		return err
	}

	return nil
}

func (dao *FirestoreOAuth2StatesDAO) DeleteExpired() error {
	docs := dao.FS.Collection(oauth2StatesCollection).
		Where("expiresAt", "<", time.Now().UTC().Unix()).
		Documents(context.Background())
	defer docs.Stop()

	batch := dao.FS.Batch()
	for {
		doc, err := docs.Next()
		if err != nil {
			if err == iterator.Done {
				break
			}

			return err
		}

		batch.Delete(doc.Ref)
	}

	_, err := batch.Commit(context.Background())
	if err != nil {
		return err
	}

	return nil
}
