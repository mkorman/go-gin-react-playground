package security

import "time"

type CredentialsEmailPassword struct {
	Email             string `json:"email" firestore:"email"`
	PasswordBcrypt    string `json:"passwordBcrypt" firestore:"passwordBcrypt"`
	LastChangeTime    int64  `json:"lastChangeTime" firestore:"lastChangeTime"`
	LastChangeIP      string `json:"lastChangeIp" firestore:"lastChangeIp"`
	PasswordResetCode string `json:"passwordResetCode" firestore:"passwordResetCode"`
}

func (credentials *CredentialsEmailPassword) ChangePassword(newPasswordBcrypt, initiatorIP string) error {
	credentials.PasswordBcrypt = newPasswordBcrypt
	credentials.LastChangeTime = time.Now().UTC().Unix()
	credentials.LastChangeIP = initiatorIP
	credentials.PasswordResetCode = ""

	return nil
}
