package security

import (
	"errors"
	"github.com/mkorman9/go-commons/web"
)

var ErrAccountNoResult = errors.New("no result")
var ErrAccountUsernameDuplicate = errors.New("account with this username already exists")
var ErrAccountEmailDuplicate = errors.New("account with this email already exists")
var ErrEmailChangeRequestNoResult = errors.New("no result")

type AccountDAO interface {
	FindPaged(
		filters AccountsFilters,
		sortingOptions web.SortingOptions,
		paginationOptions web.PaginationOptions,
	) (*AccountsPage, error)
	FindByID(string) (*Account, error)
	FindByUsername(string) (*Account, error)
	FindByPasswordCredentialsEmail(string) (*Account, error)
	FindByGithubAccountID(int64) (*Account, error)

	Insert(*Account) (string, error)
	Update(*Account) error
	Delete(*Account) error

	FindEmailChangeRequest(string, string) (*AccountEmailChangeRequest, error)
	InsertEmailChangeRequest(*AccountEmailChangeRequest) (string, error)
	DeleteEmailChangeRequest(*AccountEmailChangeRequest) error
}
