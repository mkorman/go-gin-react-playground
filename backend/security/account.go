package security

import (
	"time"
)

type Account struct {
	ID          string              `json:"id" firestore:"id"`
	Username    string              `json:"username" firestore:"username"`
	Roles       []string            `json:"roles" firestore:"roles"`
	IsActive    bool                `json:"active" firestore:"active"`
	IsDeleted   bool                `json:"deleted" firestore:"deleted"`
	BannedUntil int64               `json:"bannedUntil" firestore:"bannedUntil"`
	Language    string              `json:"language" firestore:"language"`
	CreatedAt   int64               `json:"createdAt" firestore:"createdAt"`
	Credentials *AccountCredentials `json:"credentials" firestore:"credentials"`
}

type AccountCredentials struct {
	EmailPassword *CredentialsEmailPassword `json:"emailPassword" firestore:"emailPassword"`
	Github        *CredentialsGithub        `json:"github" firestore:"github"`
}

func (a Account) Email() string {
	if a.HasActiveEmailPasswordCredentials() {
		return a.Credentials.EmailPassword.Email
	}
	if a.HasActiveGithubCredentials() {
		return a.Credentials.Github.Email
	}

	return ""
}

func (a Account) IsBanned() bool {
	return time.Now().UTC().Before(time.Unix(a.BannedUntil, 0).UTC())
}

func (a *Account) Ban(duration time.Duration) {
	a.BannedUntil = time.Now().UTC().Add(duration).Unix()
}

func (a *Account) Unban() {
	a.BannedUntil = 0
}

func (a Account) HasActiveEmailPasswordCredentials() bool {
	return a.Credentials != nil && a.Credentials.EmailPassword != nil
}

func (a Account) HasActiveGithubCredentials() bool {
	return a.Credentials != nil && a.Credentials.Github != nil
}
