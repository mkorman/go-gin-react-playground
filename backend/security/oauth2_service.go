package security

import (
	"errors"
	"github.com/gookit/config/v2"
	"time"

	"github.com/mkorman9/go-commons/requests"
	"github.com/rs/zerolog/log"
)

type OAuth2Service interface {
	GenerateState() (string, error)
	ValidateState(state string) (bool, error)
	DeleteExpiredStates() error
	Github() OAuth2GithubProvider
}

type oAuth2Service struct {
	oauth2StatesDAO Oauth2StatesDAO
	stateExpiration time.Duration
	github          *oAuth2GithubProvider
}

func NewOAuth2Service(oauth2StatesDAO Oauth2StatesDAO) *oAuth2Service {
	service := &oAuth2Service{
		oauth2StatesDAO: oauth2StatesDAO,
		stateExpiration: 30 * time.Minute,
	}
	httpClient := requests.NewClient()

	githubEnabled := config.Bool("oauth2.github.enabled")
	githubClientID := config.String("oauth2.github.clientId")
	githubClientSecret := config.String("oauth2.github.clientSecret")
	githubRedirectUrl := config.String("oauth2.github.redirectUrl")

	if githubEnabled {
		service.github = &oAuth2GithubProvider{
			enabled:      true,
			clientID:     githubClientID,
			clientSecret: githubClientSecret,
			redirectURL:  githubRedirectUrl,
			client:       httpClient,
		}
	} else {
		service.github = &oAuth2GithubProvider{
			enabled: false,
		}
	}

	return service
}

func (service *oAuth2Service) GenerateState() (string, error) {
	state, err := service.oauth2StatesDAO.New(service.stateExpiration)
	if err != nil {
		log.Error().Err(err).Msg("Could not generate OAuth2 state")
		return "", err
	}

	return state.ID, nil
}

func (service *oAuth2Service) ValidateState(id string) (bool, error) {
	state, err := service.oauth2StatesDAO.GetByID(id)
	if err != nil {
		if errors.Is(err, ErrOAuth2StateNoResult) {
			return false, nil
		}

		log.Error().Err(err).Msg("Could not validate OAuth2 state")
		return false, err
	}

	if err := service.oauth2StatesDAO.Delete(state); err != nil {
		log.Error().Err(err).Msg("Could not delete OAuth2 state")
		return false, err
	}

	return true, nil
}

func (service *oAuth2Service) DeleteExpiredStates() error {
	return service.oauth2StatesDAO.DeleteExpired()
}

func (service *oAuth2Service) Github() OAuth2GithubProvider {
	return service.github
}
