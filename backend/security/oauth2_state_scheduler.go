package security

import (
	"github.com/rs/zerolog/log"
)

func CreateExpiredOauth2StatesScheduler(service OAuth2Service) func() {
	return func() {
		log.Info().Msg("Expired OAuth2 states scheduler has been triggered")

		err := service.DeleteExpiredStates()
		if err != nil {
			log.Error().Msgf("Error while deleting expired OAuth2 states: %v", err)
		}

		log.Info().Msg("Successfully executed Expired OAuth2 states scheduler")
	}
}
