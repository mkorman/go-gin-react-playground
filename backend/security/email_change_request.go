package security

const accountEmailChangeRequestTableName = "accounts_email_change_request"

type AccountEmailChangeRequest struct {
	ID           string `gorm:"column:id; type:uuid; primaryKey"`
	AccountID    string `gorm:"column:account_id; type:uuid; primaryKey"`
	DesiredEmail string `gorm:"column:desired_email; type:varchar(255)"`
}

func (AccountEmailChangeRequest) TableName() string {
	return accountEmailChangeRequestTableName
}
