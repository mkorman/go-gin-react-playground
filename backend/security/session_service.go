package security

import (
	"crypto/rand"
	"encoding/hex"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/mkorman9/go-commons/httpauth"
	"net/http"
	"time"
)

type AuthMiddlewaresProvider interface {
	CookieMiddleware() *httpauth.Middleware
	BearerTokenMiddleware() *httpauth.Middleware
}

type SessionService interface {
	AuthMiddlewaresProvider
	Start(account *Account, prolongDuration bool, ip string) (*Session, error)
	Refresh(session *Session) error
	Revoke(session *Session) error
	DeleteExpired() error
}

type sessionService struct {
	sessionDAO            SessionDAO
	accountDAO            AccountDAO
	cookieMiddleware      httpauth.Middleware
	bearerTokenMiddleware httpauth.Middleware
}

const sessionCookieName = "SESSION_ID"
const activeSessionKey = "activeSession"
const activeAccountKey = "activeAccount"

func NewSessionService(sessionDAO SessionDAO, accountDAO AccountDAO) *sessionService {
	service := sessionService{
		sessionDAO: sessionDAO,
		accountDAO: accountDAO,
	}

	service.cookieMiddleware = httpauth.NewSessionCookieMiddleware(sessionCookieName, service.verifyCookieFunc)
	service.bearerTokenMiddleware = httpauth.NewBearerTokenMiddleware(service.verifyTokenFunc)

	return &service
}

func (service *sessionService) CookieMiddleware() *httpauth.Middleware {
	return &service.cookieMiddleware
}

func (service *sessionService) BearerTokenMiddleware() *httpauth.Middleware {
	return &service.bearerTokenMiddleware
}

func (service *sessionService) Start(account *Account, prolongDuration bool, ip string) (*Session, error) {
	tokenDuration := 4 * time.Hour
	if prolongDuration {
		tokenDuration = 14 * 24 * time.Hour
	}

	sessionID, err := randomString(32)
	if err != nil {
		return nil, err
	}

	sessionToken, err := randomString(48)
	if err != nil {
		return nil, err
	}

	session := &Session{
		ID:        sessionID,
		AccountID: account.ID,
		Token:     sessionToken,
		Roles:     account.Roles,
		IP:        ip,
		IssuedAt:  time.Now().UTC().Unix(),
		Duration:  int64(tokenDuration.Seconds()),
		ExpiresAt: time.Now().UTC().Add(tokenDuration).Unix(),
	}

	err = service.sessionDAO.Insert(session)
	if err != nil {
		return nil, err
	}

	return session, nil
}

func (service *sessionService) Refresh(session *Session) error {
	if session.Duration > 0 {
		session.ExpiresAt = time.Now().UTC().Add(time.Duration(session.Duration) * time.Second).Unix()

		return service.sessionDAO.Update(session)
	}

	return nil
}

func (service *sessionService) Revoke(session *Session) error {
	return service.sessionDAO.Delete(session)
}

func (service *sessionService) DeleteExpired() error {
	return service.sessionDAO.DeleteExpired()
}

func (service *sessionService) verifyCookieFunc(c *gin.Context, cookie string) (*httpauth.VerificationResult, error) {
	session, err := service.sessionDAO.FindByID(cookie)
	if err != nil {
		if errors.Is(err, ErrSessionNoResult) {
			return &httpauth.VerificationResult{Verified: false}, nil
		}

		return nil, err
	}

	account, err := service.accountDAO.FindByID(session.AccountID)
	if err != nil {
		return nil, err
	}

	c.Set(activeSessionKey, session)
	c.Set(activeAccountKey, account)

	return &httpauth.VerificationResult{
		Verified: true,
		Roles:    session.Roles,
	}, nil
}

func (service *sessionService) verifyTokenFunc(c *gin.Context, token string) (*httpauth.VerificationResult, error) {
	session, err := service.sessionDAO.FindByToken(token)
	if err != nil {
		if errors.Is(err, ErrSessionNoResult) {
			return &httpauth.VerificationResult{Verified: false}, nil
		}

		return nil, err
	}

	account, err := service.accountDAO.FindByID(session.AccountID)
	if err != nil {
		return nil, err
	}

	c.Set(activeSessionKey, session)
	c.Set(activeAccountKey, account)

	return &httpauth.VerificationResult{
		Verified: true,
		Roles:    session.Roles,
	}, nil
}

func SendSessionCookie(c *gin.Context, session *Session) {
	isBehindHTTPS := c.Request.Proto == "https" || c.GetHeader("X-Forwarded-Proto") == "https"

	if session == nil {
		http.SetCookie(c.Writer, &http.Cookie{
			Name:     sessionCookieName,
			Value:    "",
			Expires:  time.Unix(0, 0).UTC(),
			SameSite: http.SameSiteStrictMode,
			Secure:   isBehindHTTPS,
			HttpOnly: true,
		})

		return
	}

	cookie := &http.Cookie{
		Name:     sessionCookieName,
		Value:    session.ID,
		SameSite: http.SameSiteStrictMode,
		Secure:   isBehindHTTPS,
		HttpOnly: true,
	}

	cookie.Expires = time.Unix(session.ExpiresAt, 0).UTC()

	http.SetCookie(c.Writer, cookie)
}

func GetActiveSession(c *gin.Context) *Session {
	if field, ok := c.Get(activeSessionKey); ok {
		return field.(*Session)
	}

	return nil
}

func MustGetActiveSession(c *gin.Context) *Session {
	session := GetActiveSession(c)
	if session == nil {
		panic("missing active session")
	}

	return session
}

func GetActiveAccount(c *gin.Context) *Account {
	if field, ok := c.Get(activeAccountKey); ok {
		return field.(*Account)
	}

	return nil
}

func MustGetActiveAccount(c *gin.Context) *Account {
	account := GetActiveAccount(c)
	if account == nil {
		panic("missing active account")
	}

	return account
}

func randomString(lengthBytes uint) (string, error) {
	var bytes = make([]byte, lengthBytes)
	if _, err := rand.Read(bytes); err != nil {
		return "", err
	}

	return hex.EncodeToString(bytes), nil
}
