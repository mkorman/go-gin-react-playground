package security

type Oauth2State struct {
	ID        string `firestore:"id"`
	CreatedAt int64  `firestore:"createdAt"`
	ExpiresAt int64  `firestore:"expiresAt"`
}
