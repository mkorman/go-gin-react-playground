package security

import (
	"github.com/gookit/config/v2"
	"github.com/mkorman9/go-commons/mail"
)

type MailService struct {
	engine *mail.Engine

	activateAccountTemplate string
	passwordResetTemplate   string
	emailChangeTemplate     string
}

func NewMailService() (*MailService, error) {
	engine, err := mail.NewEngine()
	if err != nil {
		return nil, err
	}

	activateAccountTemplate := config.String("mail.templates.activateAccount")
	passwordResetTemplate := config.String("mail.templates.passwordReset")
	emailChangeTemplate := config.String("mail.templates.emailChange")

	return &MailService{
		engine:                  engine,
		activateAccountTemplate: activateAccountTemplate,
		passwordResetTemplate:   passwordResetTemplate,
		emailChangeTemplate:     emailChangeTemplate,
	}, nil
}

func (service *MailService) SendAccountActivationMessage(account *Account) error {
	return service.engine.SendMail(
		account.Username,
		account.Email(),
		service.activateAccountTemplate,
		mail.Props{
			"name": account.Username,
			"code": account.ID,
		},
	)
}

func (service *MailService) SendPasswordResetMessage(account *Account, code string) error {
	return service.engine.SendMail(
		account.Username,
		account.Email(),
		service.passwordResetTemplate,
		mail.Props{
			"name": account.Username,
			"code": code,
		},
	)
}

func (service *MailService) SendEmailChangeMessage(account *Account, newEmail string, code string) error {
	return service.engine.SendMail(
		account.Username,
		newEmail,
		service.emailChangeTemplate,
		mail.Props{
			"name": account.Username,
			"code": code,
		},
	)
}
