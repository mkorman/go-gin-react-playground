package security

import (
	"errors"
	"gitlab.com/mkorman/go-gin-react-playground/backend/captcha"
	"net/http"
	"strconv"
	"time"

	"github.com/mkorman9/go-commons/web"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
)

type AccountPublicInfoResponse struct {
	ID           string     `json:"id"`
	Username     string     `json:"username"`
	IsActive     bool       `json:"isActive"`
	IsBanned     bool       `json:"isBanned"`
	BannedUntil  *time.Time `json:"bannedUntil,omitempty"`
	Roles        []string   `json:"roles"`
	Language     string     `json:"language"`
	RegisteredAt string     `json:"registeredAt"`
}

type AccountInfoResponse struct {
	ID           string                          `json:"id"`
	Username     string                          `json:"username"`
	IsActive     bool                            `json:"isActive"`
	IsBanned     bool                            `json:"isBanned"`
	BannedUntil  *time.Time                      `json:"bannedUntil,omitempty"`
	Roles        []string                        `json:"roles"`
	Email        string                          `json:"email"`
	Language     string                          `json:"language"`
	RegisteredAt string                          `json:"registeredAt"`
	LoginMethods AccountInfoResponseLoginMethods `json:"loginMethods"`
}

type AccountInfoResponseLoginMethods struct {
	EmailAndPassword AccountInfoResponseLoginMethodMetadata `json:"emailAndPassword"`
	Github           AccountInfoResponseLoginMethodMetadata `json:"github"`
}

type AccountInfoResponseLoginMethodMetadata struct {
	IsDefined bool `json:"defined"`
}

type EditProfileRequest struct {
	Username *EditProfileUsernameRequest `json:"username" validate:"omitempty,dive"`
	Email    *EditProfileEmailRequest    `json:"email" validate:"omitempty,dive"`
	Language *EditProfileLanguageRequest `json:"language" validate:"omitempty,dive"`
	Password *EditProfilePasswordRequest `json:"password" validate:"omitempty,dive"`
}

type EditProfileResponse struct {
	Username EditProfileStatusPart `json:"username"`
	Email    EditProfileStatusPart `json:"email"`
	Language EditProfileStatusPart `json:"language"`
	Password EditProfileStatusPart `json:"password"`
}

type EditProfileStatusPart struct {
	Modified    bool        `json:"isModified"`
	Causes      []web.Cause `json:"causes,omitempty"`
	ServerError bool        `json:"isServerError"`
}

type EditProfileUsernameRequest struct {
	Username string `json:"username" validate:"required,gt=3"`
}

type EditProfileEmailRequest struct {
	Email string `json:"email" validate:"required,email"`
}

type EditProfileLanguageRequest struct {
	ID string `json:"id" validate:"required"`
}

type EditProfilePasswordRequest struct {
	OldPassword string `json:"oldPassword" validate:"required"`
	NewPassword string `json:"newPassword" validate:"required,gt=3"`
}

type AccountRegisterRequest struct {
	Username string                        `json:"username" validate:"required,gt=3,accountname"`
	Email    string                        `json:"email"    validate:"required,email"`
	Password string                        `json:"password" validate:"required,gt=3"`
	Language string                        `json:"language"`
	Captcha  *captcha.CaptchaAnswerRequest `json:"captcha"  validate:"required,dive"`
}

type AccountActivationRequest struct {
	AccountID string `json:"accountID" validate:"required,uuid4"`
}

type EmailChangeRequest struct {
	ID        string `json:"id"        validate:"required,uuid4"`
	AccountID string `json:"accountID" validate:"required,uuid4"`
}

type PasswordResetRequest struct {
	Email   string                        `json:"email"   validate:"required,email"`
	Captcha *captcha.CaptchaAnswerRequest `json:"captcha" validate:"required,dive"`
}

type PasswordCredentialsAddRequest struct {
	Password string `json:"password" validate:"required,gt=3"`
}

func AccountAPI(
	engine *gin.Engine,
	accountService AccountService,
	authMiddlewaresProvider AuthMiddlewaresProvider,
	captchaService captcha.CaptchaService,
) {
	tokenMiddleware := authMiddlewaresProvider.BearerTokenMiddleware()
	group := engine.Group("/api/v1/login/account")

	group.GET("/info/username/:username",
		func(c *gin.Context) {
			username := c.Param("username")
			if ok, causes := web.ValidateVar("username", username, "required,accountname"); !ok {
				web.ValidationError(c, causes...)
				return
			}

			account, err := accountService.GetByUsername(username)
			if err != nil {
				if errors.Is(err, ErrAccountNoResult) {
					web.ErrorResponse(c, http.StatusBadRequest, "Account not found", web.FieldError("account", "not_found"))
				} else {
					log.Error().Err(err).Msg("Unexpected error while retrieving account data")
					web.ErrorResponse(c, http.StatusInternalServerError, "Internal error while retrieving account data")
				}

				return
			}

			if !account.IsActive {
				web.ErrorResponse(c, http.StatusBadRequest, "Account not found", web.FieldError("account", "not_found"))
				return
			}

			var bannedUntil *time.Time
			if account.IsBanned() {
				b := time.Unix(account.BannedUntil, 0).UTC()
				bannedUntil = &b
			}

			response := AccountPublicInfoResponse{
				ID:           account.ID,
				Username:     account.Username,
				IsActive:     account.IsActive,
				IsBanned:     account.IsBanned(),
				BannedUntil:  bannedUntil,
				Roles:        account.Roles,
				Language:     account.Language,
				RegisteredAt: time.Unix(account.CreatedAt, 0).Format(time.RFC3339),
			}
			c.JSON(http.StatusOK, response)
		},
	)

	group.GET("/info/id/:id",
		func(c *gin.Context) {
			id := c.Param("id")
			if ok, causes := web.ValidateVar("id", id, "required,uuid4"); !ok {
				web.ValidationError(c, causes...)
				return
			}

			account, err := accountService.GetByID(id)
			if err != nil {
				if errors.Is(err, ErrAccountNoResult) {
					web.ErrorResponse(c, http.StatusBadRequest, "Account not found", web.FieldError("account", "not_found"))
				} else {
					log.Error().Err(err).Msg("Unexpected error while retrieving account data")
					web.ErrorResponse(c, http.StatusInternalServerError, "Internal error while retrieving account data")
				}

				return
			}

			if !account.IsActive {
				web.ErrorResponse(c, http.StatusBadRequest, "Account not found", web.FieldError("account", "not_found"))
				return
			}

			var bannedUntil *time.Time
			if account.IsBanned() {
				b := time.Unix(account.BannedUntil, 0).UTC()
				bannedUntil = &b
			}

			response := AccountPublicInfoResponse{
				ID:           account.ID,
				Username:     account.Username,
				IsActive:     account.IsActive,
				IsBanned:     account.IsBanned(),
				BannedUntil:  bannedUntil,
				Roles:        account.Roles,
				Language:     account.Language,
				RegisteredAt: time.Unix(account.CreatedAt, 0).UTC().Format(time.RFC3339),
			}
			c.JSON(http.StatusOK, response)
		},
	)

	group.GET("/info", tokenMiddleware.AnyAuthenticated(),
		func(c *gin.Context) {
			account := MustGetActiveAccount(c)

			var bannedUntil *time.Time
			if account.IsBanned() {
				b := time.Unix(account.BannedUntil, 0).UTC()
				bannedUntil = &b
			}

			response := AccountInfoResponse{
				ID:           account.ID,
				Username:     account.Username,
				IsActive:     account.IsActive,
				IsBanned:     account.IsBanned(),
				BannedUntil:  bannedUntil,
				Roles:        account.Roles,
				Email:        account.Email(),
				Language:     account.Language,
				RegisteredAt: time.Unix(account.CreatedAt, 0).UTC().Format(time.RFC3339),
				LoginMethods: AccountInfoResponseLoginMethods{
					EmailAndPassword: AccountInfoResponseLoginMethodMetadata{
						IsDefined: account.HasActiveEmailPasswordCredentials(),
					},
					Github: AccountInfoResponseLoginMethodMetadata{
						IsDefined: account.HasActiveGithubCredentials(),
					},
				},
			}
			c.JSON(http.StatusOK, response)
		},
	)

	group.POST("/edit", tokenMiddleware.AnyAuthenticated(),
		func(c *gin.Context) {
			var editProfileRequest EditProfileRequest
			if err := c.ShouldBindJSON(&editProfileRequest); err != nil {
				web.ValidationError(c, web.FieldErrorMessage("body", "json", "Request body needs to be a valid JSON"))
				return
			}

			if ok, causes := web.ValidateStruct(&editProfileRequest); !ok {
				web.ValidationError(c, causes...)
				return
			}

			account := MustGetActiveAccount(c)

			var usernameStatus EditProfileStatusPart
			var languageStatus EditProfileStatusPart
			var emailStatus EditProfileStatusPart
			var passwordStatus EditProfileStatusPart

			if editProfileRequest.Username != nil {
				if err := accountService.ChangeUsername(account, editProfileRequest.Username.Username); err != nil {
					if errors.Is(err, ErrAccountUsernameDuplicate) {
						usernameStatus = editProfilePartUserError(web.FieldError("username.username", "unique"))
					} else if errors.Is(err, ErrAccountInactive) {
						usernameStatus = editProfilePartUserError(web.FieldError("account", "invalid"))
					} else {
						log.Error().Err(err).Msg("Unexpected error while changing username")
						usernameStatus = editProfilePartServerError()
					}
				} else {
					usernameStatus = editProfilePartSuccessful()
				}
			}

			if editProfileRequest.Language != nil {
				if !isSupportedLanguage(editProfileRequest.Language.ID) {
					languageStatus = editProfilePartUserError(web.FieldError("language.id", "oneof"))
				} else {
					if err := accountService.ChangeLanguage(account, editProfileRequest.Language.ID); err != nil {
						if errors.Is(err, ErrAccountInactive) {
							languageStatus = editProfilePartUserError(web.FieldError("account", "invalid"))
						} else {
							log.Error().Err(err).Msg("Unexpected error while changing language")
							languageStatus = editProfilePartServerError()
						}
					} else {
						languageStatus = editProfilePartSuccessful()
					}
				}
			}

			if editProfileRequest.Email != nil {
				if err := accountService.RequestEmailChange(account, editProfileRequest.Email.Email); err != nil {
					if errors.Is(err, ErrAccountEmailDuplicate) {
						emailStatus = editProfilePartUserError(web.FieldError("email.email", "unique"))
					} else if errors.Is(err, ErrAccountInactive) || errors.Is(err, ErrAccountNoEmailPasswordCredentials) {
						emailStatus = editProfilePartUserError(web.FieldError("account", "invalid"))
					} else {
						log.Error().Err(err).Msg("Unexpected error while requesting email change")
						emailStatus = editProfilePartServerError()
					}
				} else {
					emailStatus = editProfilePartSuccessful()
				}
			}

			if editProfileRequest.Password != nil {
				if err := accountService.ChangePassword(account, editProfileRequest.Password.OldPassword, editProfileRequest.Password.NewPassword, c.ClientIP()); err != nil {
					if errors.Is(err, ErrAccountInvalidCredentials) {
						passwordStatus = editProfilePartUserError(web.FieldError("password.oldPassword", "invalid"))
					} else if errors.Is(err, ErrAccountInactive) || errors.Is(err, ErrAccountNoEmailPasswordCredentials) {
						passwordStatus = editProfilePartUserError(web.FieldError("account", "invalid"))
					} else {
						log.Error().Err(err).Msg("Unexpected error while requesting password change")
						passwordStatus = editProfilePartServerError()
					}
				} else {
					passwordStatus = editProfilePartSuccessful()
				}
			}

			c.JSON(http.StatusOK, &EditProfileResponse{
				Username: usernameStatus,
				Language: languageStatus,
				Email:    emailStatus,
				Password: passwordStatus,
			})
		},
	)

	group.POST("/register/password",
		func(c *gin.Context) {
			var accountRegisterRequest AccountRegisterRequest
			if err := c.ShouldBindJSON(&accountRegisterRequest); err != nil {
				web.ValidationError(c, web.FieldErrorMessage("body", "json", "Request body needs to be a valid JSON"))
				return
			}

			if ok, causes := web.ValidateStruct(&accountRegisterRequest); !ok {
				web.ValidationError(c, causes...)
				return
			}

			captchaOK, err := captchaService.VerifyAnswer(accountRegisterRequest.Captcha.ID, accountRegisterRequest.Captcha.Answer)
			if err != nil {
				if errors.Is(err, captcha.ErrCaptchaNoResult) {
					web.ValidationError(c, web.FieldError("captcha.id", "invalid"))
					return
				}

				log.Error().Err(err).Msg("Unexpected error while verifying captcha")
				web.ErrorResponse(c, http.StatusInternalServerError, "Internal error while creating account")
				return
			}
			if !captchaOK {
				web.ValidationError(c, web.FieldError("captcha.answer", "invalid"))
				return
			}

			if _, err := accountService.RegisterWithEmailPassword(
				accountRegisterRequest.Username,
				accountRegisterRequest.Password,
				accountRegisterRequest.Email,
				mapLanguage(accountRegisterRequest.Language),
				c.ClientIP(),
			); err != nil {
				if err == ErrAccountUsernameDuplicate {
					web.ValidationError(c, web.FieldError("username", "unique"))
					return
				} else if err == ErrAccountEmailDuplicate {
					web.ValidationError(c, web.FieldError("email", "unique"))
					return
				}

				log.Error().Err(err).Msg("Unexpected error while inserting new account")
				web.ErrorResponse(c, http.StatusInternalServerError, "Internal error while creating account")
				return
			}

			web.SuccessResponse(c, "OK")
		},
	)

	group.POST("/activate",
		func(c *gin.Context) {
			var accountActivationRequest AccountActivationRequest
			if err := c.ShouldBindJSON(&accountActivationRequest); err != nil {
				web.ValidationError(c, web.FieldErrorMessage("body", "json", "Request body needs to be a valid JSON"))
				return
			}

			if ok, causes := web.ValidateStruct(&accountActivationRequest); !ok {
				web.ValidationError(c, causes...)
				return
			}

			account, err := accountService.GetByID(accountActivationRequest.AccountID)
			if err != nil {
				if errors.Is(err, ErrAccountNoResult) {
					web.ErrorResponse(c, http.StatusBadRequest, "Invalid account type", web.FieldError("account", "invalid"))
					return
				}

				log.Error().Err(err).Msg("Unexpected error while retrieving account data")
				web.ErrorResponse(c, http.StatusInternalServerError, "Internal error while activating account")
				return
			}

			if err := accountService.Activate(account); err != nil {
				if err == ErrAccountAlreadyActivated {
					web.ErrorResponse(c, http.StatusBadRequest, "Invalid account type", web.FieldError("account", "invalid"))
					return
				} else if err == ErrAccountNoEmailPasswordCredentials {
					web.ErrorResponse(c, http.StatusBadRequest, "Invalid account type", web.FieldError("account", "invalid"))
					return
				}

				log.Error().Err(err).Msg("Unexpected error while activating account")
				web.ErrorResponse(c, http.StatusInternalServerError, "Internal error while activating account")
				return
			}

			web.SuccessResponse(c, "OK")
		},
	)

	group.POST("/email/change",
		func(c *gin.Context) {
			var emailChangeRequest EmailChangeRequest
			if err := c.ShouldBindJSON(&emailChangeRequest); err != nil {
				web.ValidationError(c, web.FieldErrorMessage("body", "json", "Request body needs to be a valid JSON"))
				return
			}

			if ok, causes := web.ValidateStruct(&emailChangeRequest); !ok {
				web.ValidationError(c, causes...)
				return
			}

			account, err := accountService.GetByID(emailChangeRequest.AccountID)
			if err != nil {
				if errors.Is(err, ErrAccountNoResult) {
					web.ErrorResponse(c, http.StatusBadRequest, "Invalid request", web.FieldError("request", "invalid"))
					return
				}

				log.Error().Err(err).Msg("Unexpected error while retrieving account data")
				web.ErrorResponse(c, http.StatusInternalServerError, "Internal error while retrieving account data")
				return
			}

			if err := accountService.FinishEmailChange(account, emailChangeRequest.ID); err != nil {
				if err == ErrAccountInvalidEmailChangeRequest || err == ErrAccountInactive || err == ErrAccountNoEmailPasswordCredentials {
					web.ErrorResponse(c, http.StatusBadRequest, "Invalid request", web.FieldError("request", "invalid"))
					return
				}

				log.Error().Err(err).Msg("Unexpected error while changing email")
				web.ErrorResponse(c, http.StatusInternalServerError, "Internal error while changing email")
				return
			}

			web.SuccessResponse(c, "OK")
		},
	)

	group.POST("/password/reset",
		func(c *gin.Context) {
			var passwordResetRequest PasswordResetRequest
			if err := c.ShouldBindJSON(&passwordResetRequest); err != nil {
				web.ValidationError(c, web.FieldErrorMessage("body", "json", "Request body needs to be a valid JSON"))
				return
			}

			if ok, causes := web.ValidateStruct(&passwordResetRequest); !ok {
				web.ValidationError(c, causes...)
				return
			}

			captchaOK, err := captchaService.VerifyAnswer(passwordResetRequest.Captcha.ID, passwordResetRequest.Captcha.Answer)
			if err != nil {
				if errors.Is(err, captcha.ErrCaptchaNoResult) {
					web.ValidationError(c, web.FieldError("captcha.id", "invalid"))
					return
				}

				log.Error().Err(err).Msg("Unexpected error while verifying captcha")
				web.ErrorResponse(c, http.StatusInternalServerError, "Internal error while resetting password")
				return
			}
			if !captchaOK {
				web.ValidationError(c, web.FieldError("captcha.answer", "invalid"))
				return
			}

			if err := accountService.RequestPasswordReset(passwordResetRequest.Email); err != nil {
				if errors.Is(err, ErrAccountNoResult) || errors.Is(err, ErrAccountInactive) || errors.Is(err, ErrAccountInactive) {
					web.ErrorResponse(
						c,
						http.StatusBadRequest,
						"Cannot reset password for this account",
						web.FieldError("account", "invalid"),
					)

					return
				}

				log.Error().Err(err).Msg("Unexpected error while requesting password reset")
				web.ErrorResponse(c, http.StatusInternalServerError, "Internal error while requesting password reset")
				return
			}

			web.SuccessResponse(c, "OK")
		},
	)

	group.POST("/password/set", tokenMiddleware.AnyAuthenticated(),
		func(c *gin.Context) {
			var passwordCredentialsAddRequest PasswordCredentialsAddRequest
			if err := c.ShouldBindJSON(&passwordCredentialsAddRequest); err != nil {
				web.ValidationError(c, web.FieldErrorMessage("body", "json", "Request body needs to be a valid JSON"))
				return
			}

			if ok, causes := web.ValidateStruct(&passwordCredentialsAddRequest); !ok {
				web.ValidationError(c, causes...)
				return
			}

			account := MustGetActiveAccount(c)

			if err := accountService.AddEmailPasswordCredentials(account, passwordCredentialsAddRequest.Password, c.ClientIP()); err != nil {
				if err == ErrAccountInactive || err == ErrAccountCredentialsAlreadySet {
					web.ErrorResponse(c, http.StatusBadRequest, "Cannot set password for this account", web.FieldError("account", "invalid"))
					return
				}

				log.Error().Err(err).Msg("Unexpected error while adding password credentials to account")
				web.ErrorResponse(c, http.StatusInternalServerError, "Internal error while adding password credentials to account")
				return
			}

			web.SuccessResponse(c, "OK")
		},
	)

	group.POST("/password/set/:accountID/:code",
		func(c *gin.Context) {
			var passwordSetRequest PasswordCredentialsAddRequest
			if err := c.ShouldBindJSON(&passwordSetRequest); err != nil {
				web.ValidationError(c, web.FieldErrorMessage("body", "json", "Request body needs to be a valid JSON"))
				return
			}

			if ok, causes := web.ValidateStruct(&passwordSetRequest); !ok {
				web.ValidationError(c, causes...)
				return
			}

			accountID := c.Param("accountID")
			if ok, causes := web.ValidateVar("accountId", &accountID, "required,uuid4"); !ok {
				web.ValidationError(c, causes...)
				return
			}
			code := c.Param("code")
			if ok, causes := web.ValidateVar("code", &accountID, "required,uuid4"); !ok {
				web.ValidationError(c, causes...)
				return
			}

			account, err := accountService.GetByID(accountID)
			if err != nil {
				if errors.Is(err, ErrAccountNoResult) {
					web.ErrorResponse(
						c,
						http.StatusBadRequest,
						"Cannot reset password of this account",
						web.FieldError("account", "invalid"),
					)

					return
				}

				log.Error().Err(err).Msg("Unexpected error while retrieving account data")
				web.ErrorResponse(c, http.StatusInternalServerError, "Internal error while setting password")
				return
			}

			if err := accountService.FinishPasswordReset(account, code, passwordSetRequest.Password, c.ClientIP()); err != nil {
				if errors.Is(err, ErrAccountInactive) ||
					errors.Is(err, ErrAccountNoEmailPasswordCredentials) ||
					errors.Is(err, ErrAccountInvalidPasswordChangeRequest) {
					web.ErrorResponse(
						c,
						http.StatusBadRequest,
						"Cannot reset password of this account",
						web.FieldError("account", "invalid"),
					)

					return
				}

				log.Error().Err(err).Msg("Unexpected error while setting new password")
				web.ErrorResponse(c, http.StatusInternalServerError, "Internal error while setting new password")
				return
			}

			web.SuccessResponse(c, "OK")
		},
	)
}

func isSupportedLanguage(preferredLanguage string) bool {
	return preferredLanguage == "en-US" || preferredLanguage == "pl-PL"
}

func mapLanguage(preferredLanguage string) string {
	if preferredLanguage == "pl-PL" {
		return preferredLanguage
	} else {
		return "en-US"
	}
}

func editProfilePartSuccessful() EditProfileStatusPart {
	return EditProfileStatusPart{
		Modified: true,
		Causes:   make([]web.Cause, 0),
	}
}

func editProfilePartUserError(causes ...web.Cause) EditProfileStatusPart {
	return EditProfileStatusPart{
		Causes: causes,
	}
}

func editProfilePartServerError() EditProfileStatusPart {
	return EditProfileStatusPart{
		ServerError: true,
		Causes:      make([]web.Cause, 0),
	}
}

func parseAccountsFilters(raw map[string]string) (*AccountsFilters, []web.Cause) {
	var filters AccountsFilters
	var causes []web.Cause

	for field, value := range raw {
		switch field {
		case "username":
			filters.Username = value
		default:
			causes = append(causes, web.FieldError("filter", "oneof"))
			continue
		}
	}

	return &filters, causes
}

func parseAccountsSortBy(sortBy string, reverseSort bool) (*web.SortingOptions, []web.Cause) {
	options := web.SortingOptions{Reverse: reverseSort}

	switch sortBy {
	case "":
		fallthrough
	case "id":
		options.Field = AccountsSortID
	case "username":
		options.Field = AccountsSortUsername
	case "registeredAt":
		options.Field = AccountsRegistrationDate
	default:
		return nil, []web.Cause{web.FieldError("sortBy", "oneof")}
	}

	return &options, nil
}

func parseAccountsPaginationOptions(rawPageNumber, rawPageSize string) *web.PaginationOptions {
	pageNumber, err := strconv.Atoi(rawPageNumber)
	if err != nil {
		pageNumber = DefaultAccountsPaginationOptions.PageNumber
	}

	pageSize, err := strconv.Atoi(rawPageSize)
	if err != nil {
		pageSize = DefaultAccountsPaginationOptions.PageSize
	}

	if pageNumber < 0 {
		pageNumber = DefaultAccountsPaginationOptions.PageNumber
	}

	if pageSize <= 0 {
		pageSize = DefaultAccountsPaginationOptions.PageSize
	}
	if pageSize > MaxAccountsPageSize {
		pageSize = MaxAccountsPageSize
	}

	return &web.PaginationOptions{PageNumber: pageNumber, PageSize: pageSize}
}
