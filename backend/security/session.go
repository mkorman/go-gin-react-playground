package security

type Session struct {
	ID        string   `firestore:"id"`
	AccountID string   `firestore:"accountID"`
	Token     string   `firestore:"token"`
	Roles     []string `firestore:"roles"`
	IP        string   `firestore:"ip"`
	IssuedAt  int64    `firestore:"issuedAt"`
	Duration  int64    `firestore:"duration"`
	ExpiresAt int64    `firestore:"expiresAt"`
}
