package security

import (
	"errors"
	"fmt"
	"github.com/mkorman9/go-commons/web"
	"math"
	"time"

	uuid "github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
)

const (
	AccountsSortID           = iota
	AccountsSortUsername     = iota
	AccountsRegistrationDate = iota
)

var DefaultAccountsFilters = AccountsFilters{}
var DefaultAccountsSortingOptions = web.SortingOptions{Field: AccountsSortID, Reverse: false}
var DefaultAccountsPaginationOptions = web.PaginationOptions{PageNumber: 0, PageSize: 10}
var MaxAccountsPageSize = 100

var ErrAccountInactive = errors.New("inactive account")
var ErrAccountAlreadyActivated = errors.New("account already activated")
var ErrAccountNoEmailPasswordCredentials = errors.New("missing email/password credentials")
var ErrAccountInvalidCredentials = errors.New("invalid credentials")
var ErrAccountInvalidEmailChangeRequest = errors.New("invalid email change request")
var ErrAccountInvalidPasswordChangeRequest = errors.New("invalid password change request")
var ErrAccountCredentialsAlreadySet = errors.New("credentials for this account are already set")

type AccountsFilters struct {
	Username string
}

type AccountsPage struct {
	web.Page
	Data []*Account `json:"data"`
}

type AccountService interface {
	FindPaged(
		filters AccountsFilters,
		sortingOptions web.SortingOptions,
		paginationOptions web.PaginationOptions,
	) (*AccountsPage, error)
	GetByID(accountID string) (*Account, error)
	GetByUsername(username string) (*Account, error)
	GetByGithubAccountID(githubAccountID int64) (*Account, error)

	LoginWithEmailPassword(email, password string) (*Account, error)
	LoginWithGithub(account *Account, userInfo *OAuth2GithubUserInfo, ip string) error
	RegisterWithEmailPassword(username, password, email, language, ip string) (*Account, error)
	RegisterWithGithub(userInfo *OAuth2GithubUserInfo, language, ip string) (*Account, error)
	AddGithubCredentials(account *Account, userInfo *OAuth2GithubUserInfo, ip string) error
	AddEmailPasswordCredentials(account *Account, password, ip string) error

	ChangePassword(account *Account, oldPassword, newPassword, ip string) error
	ChangeUsername(account *Account, newUsername string) error
	ChangeLanguage(account *Account, newLanguage string) error
	ChangeRoles(account *Account, newRoles []string) error

	RequestEmailChange(account *Account, email string) error
	FinishEmailChange(account *Account, requestID string) error
	RequestPasswordReset(email string) error
	FinishPasswordReset(account *Account, code, newPassword, ip string) error
	Activate(account *Account) error
	Ban(account *Account, duration time.Duration) error
	Unban(account *Account) error
	Delete(account *Account) error
}

type accountService struct {
	dao         AccountDAO
	mailService *MailService
}

func NewAccountService(dao AccountDAO, mailService *MailService) AccountService {
	return &accountService{
		dao:         dao,
		mailService: mailService,
	}
}

func (service *accountService) GetByID(accountID string) (*Account, error) {
	account, err := service.dao.FindByID(accountID)
	if err != nil {
		return nil, err
	}

	return account, nil
}

func (service *accountService) FindPaged(
	filters AccountsFilters,
	sortingOptions web.SortingOptions,
	paginationOptions web.PaginationOptions,
) (*AccountsPage, error) {
	return service.dao.FindPaged(filters, sortingOptions, paginationOptions)
}

func (service *accountService) GetByUsername(username string) (*Account, error) {
	account, err := service.dao.FindByUsername(username)
	if err != nil {
		return nil, err
	}

	return account, nil
}

func (service *accountService) GetByGithubAccountID(githubAccountID int64) (*Account, error) {
	account, err := service.dao.FindByGithubAccountID(githubAccountID)
	if err != nil {
		return nil, err
	}

	return account, nil
}

func (service *accountService) RequestEmailChange(account *Account, email string) error {
	if !account.IsActive {
		return ErrAccountInactive
	}

	if !account.HasActiveEmailPasswordCredentials() {
		return ErrAccountNoEmailPasswordCredentials
	}

	_, err := service.dao.FindByPasswordCredentialsEmail(email)
	if err != nil {
		if err != ErrAccountNoResult {
			return err
		}
	} else {
		return ErrAccountEmailDuplicate
	}

	emailChangeRequest := &AccountEmailChangeRequest{
		AccountID:    account.ID,
		DesiredEmail: email,
	}
	requestID, err := service.dao.InsertEmailChangeRequest(emailChangeRequest)
	if err != nil {
		return err
	}

	if err := service.mailService.SendEmailChangeMessage(account, email, requestID); err != nil {
		return err
	}

	return nil
}

func (service *accountService) FinishEmailChange(account *Account, requestID string) error {
	if !account.IsActive {
		return ErrAccountInactive
	}

	if !account.HasActiveEmailPasswordCredentials() {
		return ErrAccountNoEmailPasswordCredentials
	}

	request, err := service.dao.FindEmailChangeRequest(requestID, account.ID)
	if err != nil {
		if err == ErrEmailChangeRequestNoResult {
			return ErrAccountInvalidEmailChangeRequest
		}

		return err
	}

	account.Credentials.EmailPassword.Email = request.DesiredEmail

	if err := service.update(account); err != nil {
		return err
	}

	if err := service.dao.DeleteEmailChangeRequest(request); err != nil {
		return err
	}

	return nil
}

func (service *accountService) ChangePassword(account *Account, oldPassword, newPassword, ip string) error {
	if !account.IsActive {
		return ErrAccountInactive
	}

	if !account.HasActiveEmailPasswordCredentials() {
		return ErrAccountNoEmailPasswordCredentials
	}

	if bcrypt.CompareHashAndPassword([]byte(account.Credentials.EmailPassword.PasswordBcrypt), []byte(oldPassword)) != nil {
		return ErrAccountInvalidCredentials
	}

	newPasswordBcrypt, err := bcrypt.GenerateFromPassword([]byte(newPassword), 0)
	if err != nil {
		return err
	}

	if err := account.Credentials.EmailPassword.ChangePassword(string(newPasswordBcrypt), ip); err != nil {
		return err
	}

	return service.update(account)
}

func (service *accountService) ChangeUsername(account *Account, newUsername string) error {
	if !account.IsActive {
		return ErrAccountInactive
	}

	account.Username = newUsername

	return service.update(account)
}

func (service *accountService) ChangeLanguage(account *Account, newLanguage string) error {
	if !account.IsActive {
		return ErrAccountInactive
	}

	account.Language = newLanguage

	return service.update(account)
}

func (service *accountService) ChangeRoles(account *Account, newRoles []string) error {
	account.Roles = newRoles
	return service.update(account)
}

func (service *accountService) RequestPasswordReset(email string) error {
	account, err := service.dao.FindByPasswordCredentialsEmail(email)
	if err != nil {
		return err
	}

	if !account.IsActive {
		return ErrAccountInactive
	}

	if !account.HasActiveEmailPasswordCredentials() {
		return ErrAccountNoEmailPasswordCredentials
	}

	account.Credentials.EmailPassword.PasswordResetCode = uuid.NewV4().String()

	if err := service.update(account); err != nil {
		return err
	}

	if err := service.mailService.SendPasswordResetMessage(account, account.Credentials.EmailPassword.PasswordResetCode); err != nil {
		return err
	}

	return nil
}

func (service *accountService) FinishPasswordReset(account *Account, code, newPassword, ip string) error {
	if !account.IsActive {
		return ErrAccountInactive
	}

	if !account.HasActiveEmailPasswordCredentials() {
		return ErrAccountNoEmailPasswordCredentials
	}

	if account.Credentials.EmailPassword.PasswordResetCode == "" || account.Credentials.EmailPassword.PasswordResetCode != code {
		return ErrAccountInvalidPasswordChangeRequest
	}

	newPasswordBcrypt, err := bcrypt.GenerateFromPassword([]byte(newPassword), 0)
	if err != nil {
		return err
	}

	if err := account.Credentials.EmailPassword.ChangePassword(string(newPasswordBcrypt), ip); err != nil {
		return err
	}

	if err := service.dao.Update(account); err != nil {
		return err
	}

	return nil
}

func (service *accountService) LoginWithEmailPassword(email, password string) (*Account, error) {
	account, err := service.dao.FindByPasswordCredentialsEmail(email)
	if err != nil {
		return nil, err
	}

	if !account.HasActiveEmailPasswordCredentials() {
		return nil, ErrAccountNoEmailPasswordCredentials
	}

	if bcrypt.CompareHashAndPassword([]byte(account.Credentials.EmailPassword.PasswordBcrypt), []byte(password)) != nil {
		return nil, ErrAccountInvalidCredentials
	}

	if !account.IsActive {
		return nil, ErrAccountInactive
	}

	return account, nil
}

func (service *accountService) LoginWithGithub(account *Account, userInfo *OAuth2GithubUserInfo, ip string) error {
	if !account.IsActive {
		return ErrAccountInactive
	}

	account.Credentials.Github.Access(userInfo, ip)

	if err := service.update(account); err != nil {
		return err
	}

	return nil
}

func (service *accountService) RegisterWithEmailPassword(username, password, email, language, ip string) (*Account, error) {
	newAccount := Account{
		Username:    username,
		Roles:       []string{},
		IsActive:    false,
		IsDeleted:   false,
		Language:    language,
		BannedUntil: math.MaxInt64,
		Credentials: &AccountCredentials{
			EmailPassword: &CredentialsEmailPassword{
				Email: email,
			},
		},
	}

	newPasswordBcrypt, err := bcrypt.GenerateFromPassword([]byte(password), 0)
	if err != nil {
		return nil, err
	}

	if err := newAccount.Credentials.EmailPassword.ChangePassword(string(newPasswordBcrypt), ip); err != nil {
		return nil, err
	}

	if _, err := service.dao.Insert(&newAccount); err != nil {
		return nil, err
	}

	if err := service.mailService.SendAccountActivationMessage(&newAccount); err != nil {
		return nil, err
	}

	return &newAccount, nil
}

func (service *accountService) RegisterWithGithub(userInfo *OAuth2GithubUserInfo, language, ip string) (*Account, error) {
	newAccount := Account{
		Username:    userInfo.Username,
		Roles:       []string{},
		IsActive:    true,
		IsDeleted:   false,
		Language:    language,
		BannedUntil: math.MaxInt64,
		Credentials: &AccountCredentials{
			Github: &CredentialsGithub{
				GithubAccountID: userInfo.AccountID,
			},
		},
	}
	newAccount.Credentials.Github.Access(userInfo, ip)

	tryNumber := 1
	for {
		_, err := service.dao.Insert(&newAccount)
		if err != nil {
			if err == ErrAccountUsernameDuplicate {
				// account with this username already exist so add a numer at the end of the username and try again
				newAccount.Username = fmt.Sprintf("%s%d", userInfo.Username, tryNumber)
				continue
			} else {
				return nil, err
			}
		}

		break
	}

	return &newAccount, nil
}

func (service *accountService) AddGithubCredentials(account *Account, userInfo *OAuth2GithubUserInfo, ip string) error {
	if !account.IsActive {
		return ErrAccountInactive
	}

	account.Credentials.Github = &CredentialsGithub{
		GithubAccountID: userInfo.AccountID,
	}
	account.Credentials.Github.Access(userInfo, ip)

	return service.update(account)
}

func (service *accountService) AddEmailPasswordCredentials(account *Account, password, ip string) error {
	if !account.IsActive {
		return ErrAccountInactive
	}

	if account.HasActiveEmailPasswordCredentials() {
		return ErrAccountCredentialsAlreadySet
	}

	account.Credentials.EmailPassword = &CredentialsEmailPassword{
		Email: account.Email(),
	}

	newPasswordBcrypt, err := bcrypt.GenerateFromPassword([]byte(password), 0)
	if err != nil {
		return err
	}

	if err := account.Credentials.EmailPassword.ChangePassword(string(newPasswordBcrypt), ip); err != nil {
		return err
	}

	return service.update(account)
}

func (service *accountService) Activate(account *Account) error {
	if account.IsActive {
		return ErrAccountAlreadyActivated
	}

	if !account.HasActiveEmailPasswordCredentials() {
		return ErrAccountNoEmailPasswordCredentials
	}

	account.IsActive = true

	return service.update(account)
}

func (service *accountService) Ban(account *Account, duration time.Duration) error {
	account.Ban(duration)
	return service.update(account)
}

func (service *accountService) Unban(account *Account) error {
	account.Unban()
	return service.update(account)
}

func (service *accountService) Delete(account *Account) error {
	return service.dao.Delete(account)
}

func (service *accountService) update(account *Account) error {
	err := service.dao.Update(account)
	if err != nil {
		return err
	}

	return nil
}
