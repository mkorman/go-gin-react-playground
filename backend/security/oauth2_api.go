package security

import (
	"errors"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/mkorman9/go-commons/web"
)

type OAuth2MetadataResponse struct {
	Github *OAuth2ProvidersInfoResponse `json:"github"`
	State  string                       `json:"state"`
}

type OAuth2ProvidersInfoResponse struct {
	Enabled  bool   `json:"enabled"`
	ClientID string `json:"clientId,omitempty"`
}

type OAuth2CodeFlowRequest struct {
	Code     string `json:"code" validate:"required,oauth2code,gt=0"`
	State    string `json:"state" validate:"required,uuid4,gt=0"`
	Language string `json:"language"`
}

type GithubAPIOperations struct {
	accountService AccountService
	sessionService SessionService
}

func OAuth2API(
	engine *gin.Engine,
	accountService AccountService,
	sessionService SessionService,
	oAuth2Service OAuth2Service,
) {
	tokenAuthMiddleware := sessionService.BearerTokenMiddleware()
	group := engine.Group("/api/v1/login/oauth2")

	group.GET("",
		func(c *gin.Context) {
			var oAuth2State string
			if oAuth2Service.Github().IsEnabled() /* || (any other providers) */ {
				state, err := oAuth2Service.GenerateState()
				if err != nil {
					web.InternalError(c, err, "Unexpected error while generating OAuth2 state")
					return
				}

				oAuth2State = state
			}

			response := &OAuth2MetadataResponse{
				Github: &OAuth2ProvidersInfoResponse{
					Enabled:  oAuth2Service.Github().IsEnabled(),
					ClientID: oAuth2Service.Github().ClientID(),
				},
				State: oAuth2State,
			}

			c.JSON(http.StatusOK, response)
		},
	)

	group.POST("/github/code",
		tokenAuthMiddleware.Anyone(),
		func(c *gin.Context) {
			var oAuth2CodeFlowRequest OAuth2CodeFlowRequest
			if err := c.ShouldBindJSON(&oAuth2CodeFlowRequest); err != nil {
				web.ValidationError(c, web.FieldError("body", "json"))
				return
			}

			if ok, causes := web.ValidateStruct(&oAuth2CodeFlowRequest); !ok {
				web.ValidationError(c, causes...)
				return
			}

			if !oAuth2Service.Github().IsEnabled() {
				web.ErrorResponse(c,
					http.StatusBadRequest,
					"Github provider is disabled on this server",
					web.FieldError("provider", "disabled"),
				)
				return
			}

			stateVerified, err := oAuth2Service.ValidateState(oAuth2CodeFlowRequest.State)
			if err != nil {
				web.InternalError(c, err, "Unexpected error while validating OAuth2 state")
				return
			}
			if !stateVerified {
				web.ErrorResponse(c,
					http.StatusBadRequest,
					"Invalid OAuth2 code",
					web.FieldError("state", "invalid"),
				)
				return
			}

			githubUserInfo, err := oAuth2Service.Github().FinishCodeFlow(oAuth2CodeFlowRequest.Code)
			if err != nil {
				web.ErrorResponse(c,
					http.StatusUnauthorized,
					"Could not finish flow",
					web.FieldError("flow", "failed"),
				)
				return
			}

			account, err := accountService.GetByGithubAccountID(githubUserInfo.AccountID)
			if err != nil {
				if errors.Is(err, ErrAccountNoResult) {
					account = nil
				} else {
					web.InternalError(c, err, "Unexpected error while retrieving account data by Github account ID")
					return
				}
			}

			ops := &GithubAPIOperations{
				accountService: accountService,
				sessionService: sessionService,
			}

			if account != nil {
				ops.authExistingGithubAccount(c, githubUserInfo, account)
			} else {
				account := GetActiveAccount(c)
				if account != nil {
					ops.authAddGithubCredentialsToExistingAccount(c, githubUserInfo, account)
				} else {
					ops.registerNewGithubAccount(c, githubUserInfo, &oAuth2CodeFlowRequest)
				}
			}
		},
	)
}

func (ops *GithubAPIOperations) authExistingGithubAccount(c *gin.Context, userInfo *OAuth2GithubUserInfo, account *Account) {
	if err := ops.accountService.LoginWithGithub(account, userInfo, c.ClientIP()); err != nil {
		if err == ErrAccountInactive {
			web.ErrorResponse(c, http.StatusUnauthorized, "Account is inactive", web.FieldError("account", "inactive"))
			return
		}

		web.InternalError(c, err, "Unexpected error while updating account data with Github access metadata")
		return
	}

	if err := ops.endExistingSession(c); err != nil {
		web.InternalError(c, err, "Unexpected error while revoking previous session")
		return
	}

	ops.startSession(c, account)
}

func (ops *GithubAPIOperations) registerNewGithubAccount(
	c *gin.Context,
	userInfo *OAuth2GithubUserInfo,
	oAuth2CodeFlowRequest *OAuth2CodeFlowRequest,
) {
	newAccount, err := ops.accountService.RegisterWithGithub(
		userInfo,
		mapLanguage(oAuth2CodeFlowRequest.Language),
		c.ClientIP(),
	)
	if err != nil {
		web.InternalError(c, err, "Unexpected error while inserting account data")
		return
	}

	ops.startSession(c, newAccount)
}

func (ops *GithubAPIOperations) authAddGithubCredentialsToExistingAccount(
	c *gin.Context,
	userInfo *OAuth2GithubUserInfo,
	account *Account,
) {
	if err := ops.accountService.AddGithubCredentials(account, userInfo, c.ClientIP()); err != nil {
		if err == ErrAccountInactive {
			web.ErrorResponse(
				c,
				http.StatusUnauthorized,
				"Account is inactive",
				web.FieldError("account", "inactive"),
			)
			return
		}

		web.InternalError(c, err, "Unexpected error while updating account data with Github access metadata")
		return
	}

	if err := ops.endExistingSession(c); err != nil {
		web.InternalError(c, err, "Unexpected error while revoking previous session")
		return
	}

	ops.startSession(c, account)
}

func (ops *GithubAPIOperations) startSession(c *gin.Context, account *Account) {
	session, err := ops.sessionService.Start(account, false, c.ClientIP())
	if err != nil {
		web.InternalError(c, err, "Could not start session")
		return
	}

	SendSessionCookie(c, session)
	c.JSON(http.StatusOK, &SessionStatusResponse{
		ID:          session.ID,
		StartTime:   time.Unix(session.IssuedAt, 0).UTC(),
		ExpiresAt:   time.Unix(session.ExpiresAt, 0).UTC(),
		Subject:     account.ID,
		LoginIP:     session.IP,
		Roles:       session.Roles,
		AccessToken: session.Token,
	})
}

func (ops *GithubAPIOperations) endExistingSession(c *gin.Context) error {
	session := GetActiveSession(c)
	if session != nil {
		return ops.sessionService.Revoke(session)
	}

	return nil
}
