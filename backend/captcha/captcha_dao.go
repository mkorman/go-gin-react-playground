package captcha

import (
	"errors"
)

var ErrCaptchaNoResult = errors.New("no result")

type CaptchaDAO interface {
	GetByID(id string) (*Captcha, error)

	Insert(captcha *Captcha) error
	Delete(captcha *Captcha) error

	DeleteExpired() error
}
