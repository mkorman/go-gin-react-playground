package captcha

import (
	"bytes"
	"errors"
	"fmt"
	"image/png"
	"math/rand"
	"net/http"
	"net/url"
	"strings"
	"time"

	captchagen "github.com/dchest/captcha"
	"github.com/mkorman9/go-commons/requests"
	"github.com/patrickmn/go-cache"
)

type CaptchaLanguage = string

const (
	CaptchaLangEn = CaptchaLanguage("en")
	CaptchaLangPl = CaptchaLanguage("pl")
)

var captchaCharacters = []rune("0123456789")

type CaptchaService interface {
	GenerateCode() (string, error)
	GetImageForCode(captchaID string, width, height int) ([]byte, error)
	GetAudioForCode(captchaID, language string) ([]byte, error)
	VerifyAnswer(captchaID, answer string) (bool, error)
	DeleteExpired() error
}

type captchaService struct {
	captchaDAO     CaptchaDAO
	codeLength     int
	generator      *rand.Rand
	codeExpiration time.Duration
	httpClient     *requests.Client
	audioCache     *cache.Cache
}

func NewCaptchaService(captchaDAO CaptchaDAO) CaptchaService {
	service := captchaService{
		captchaDAO:     captchaDAO,
		codeLength:     6,
		generator:      rand.New(rand.NewSource(time.Now().UnixNano())),
		codeExpiration: 30 * time.Minute,
		httpClient:     requests.NewClient(),
		audioCache:     cache.New(time.Minute, 2*time.Minute),
	}

	return &service
}

func (service *captchaService) GenerateCode() (string, error) {
	code := service.generateCode()

	captcha := &Captcha{
		Code:      code,
		CreatedAt: time.Now().UTC().Unix(),
		ExpiresAt: time.Now().UTC().Add(service.codeExpiration).Unix(),
	}

	if err := service.captchaDAO.Insert(captcha); err != nil {
		return "", err
	}

	return captcha.ID, nil
}

func (service *captchaService) GetImageForCode(captchaID string, width, height int) ([]byte, error) {
	captcha, err := service.captchaDAO.GetByID(captchaID)
	if err != nil {
		if errors.Is(err, ErrCaptchaNoResult) {
			return nil, err
		}

		return nil, err
	}

	img := captchagen.NewImage(captchaID, service.captchaToBytes(captcha.Code), width, height)

	var imgBuf bytes.Buffer
	if err := png.Encode(&imgBuf, img.Paletted); err != nil {
		return nil, err
	}

	return imgBuf.Bytes(), nil
}

func (service *captchaService) GetAudioForCode(captchaID string, language CaptchaLanguage) ([]byte, error) {
	captcha, err := service.captchaDAO.GetByID(captchaID)
	if err != nil {
		if errors.Is(err, ErrCaptchaNoResult) {
			return nil, err
		}

		return nil, err
	}

	cacheKey := fmt.Sprintf("%s_%s", language, captchaID)
	textToSpeech := service.joinTextToSpeech(captcha.Code, language)

	if cachedAudio, found := service.audioCache.Get(cacheKey); found {
		return cachedAudio.([]byte), nil
	}

	speechRequest, _ := requests.NewRequest(
		requests.GET,
		requests.URL(fmt.Sprintf("http://translate.google.com/translate_tts?ie=UTF-8&total=1&idx=0&textlen=32&client=tw-ob&q=%s&tl=%s", url.QueryEscape(textToSpeech), language)),
	)

	speechResponse, err := service.httpClient.Send(speechRequest)
	if err != nil {
		return nil, err
	}
	if speechResponse.StatusCode != http.StatusOK {
		return nil, errors.New("invalid status code")
	}

	audioBuf, err := requests.ReadResponseBody(speechResponse)
	if err != nil {
		return nil, err
	}

	service.audioCache.Set(cacheKey, audioBuf, cache.DefaultExpiration)

	return audioBuf, nil
}

func (service *captchaService) VerifyAnswer(captchaID, answer string) (bool, error) {
	captcha, err := service.captchaDAO.GetByID(captchaID)
	if err != nil {
		if errors.Is(err, ErrCaptchaNoResult) {
			return false, err
		}

		return false, err
	}

	service.audioCache.Delete(fmt.Sprintf("en_%s", captchaID))
	service.audioCache.Delete(fmt.Sprintf("pl_%s", captchaID))

	if err := service.captchaDAO.Delete(captcha); err != nil {
		return false, err
	}

	return captcha.Code == answer, nil
}

func (service *captchaService) DeleteExpired() error {
	return service.captchaDAO.DeleteExpired()
}

func (service *captchaService) generateCode() string {
	b := make([]rune, service.codeLength)

	for i := range b {
		b[i] = captchaCharacters[service.generator.Intn(len(captchaCharacters))]
	}

	return string(b)
}

func (service *captchaService) captchaToBytes(captcha string) []byte {
	b := make([]byte, len(captcha))

	for i, character := range captcha {
		b[i] = byte(character - 48)
	}

	return b
}

func (service *captchaService) joinTextToSpeech(captchaCode string, language CaptchaLanguage) string {
	var textToSpeech string

	switch language {
	case CaptchaLangPl:
		fallthrough
	case CaptchaLangEn:
		textToSpeech = strings.Join(strings.Split(captchaCode, ""), ", ")
	}

	return textToSpeech
}
