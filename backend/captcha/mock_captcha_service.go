package captcha

import "github.com/stretchr/testify/mock"

type MockCaptchaService struct {
	mock.Mock
}

func (mock *MockCaptchaService) GenerateCode() (string, error) {
	m := mock.Called()
	return m.Get(0).(string), m.Error(1)
}

func (mock *MockCaptchaService) GetImageForCode(captchaID string, width, height int) ([]byte, error) {
	m := mock.Called(captchaID, width, height)
	return m.Get(0).([]byte), m.Error(1)
}

func (mock *MockCaptchaService) GetAudioForCode(captchaID, language string) ([]byte, error) {
	m := mock.Called(captchaID, language)
	return m.Get(0).([]byte), m.Error(1)
}

func (mock *MockCaptchaService) VerifyAnswer(captchaID, answer string) (bool, error) {
	m := mock.Called(captchaID, answer)
	return m.Get(0).(bool), m.Error(1)
}

func (mock *MockCaptchaService) DeleteExpired() error {
	m := mock.Called()
	return m.Error(0)
}
