package captcha

import (
	"fmt"
	"github.com/mkorman9/go-commons/httpserver"
	"gitlab.com/mkorman/go-gin-react-playground/backend/commons_test"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/mkorman9/go-commons/logging"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestGenerateCaptcha(t *testing.T) {
	// given
	randomCaptchaID := uuid.NewV4().String()

	captchaServiceMock := new(MockCaptchaService)
	captchaServiceMock.On("GenerateCode").Return(randomCaptchaID, nil)

	api := prepareCaptchaAPI(captchaServiceMock)

	// when
	response := callGenerateCaptcha(t, api)
	body := commons_test.ReadJSONResponse[CaptchaGenerateResponse](response)

	// then
	assert.Equal(t, http.StatusOK, response.Code, "response not expected")
	assert.Equal(t, randomCaptchaID, body.ID)
}

func TestCaptchaGetImage(t *testing.T) {
	// given
	randomCaptchaID := uuid.NewV4().String()
	randomImage := []byte{21, 37, 0, 11, 27}
	expectedImageWidth := 250
	expectedImageHeight := 75

	captchaServiceMock := new(MockCaptchaService)
	captchaServiceMock.On("GetImageForCode", randomCaptchaID, expectedImageWidth, expectedImageHeight).Return(randomImage, nil)

	api := prepareCaptchaAPI(captchaServiceMock)

	// when
	response := callGetCaptchaImage(t, api, randomCaptchaID)

	// then
	assert.Equal(t, http.StatusOK, response.Code, "response not expected")
	assert.Equal(t, randomImage, response.Body.Bytes())
}

func TestCaptchaGetImageOverMaxSize(t *testing.T) {
	// given
	randomCaptchaID := uuid.NewV4().String()
	randomImage := []byte{21, 37, 0, 11, 27}
	expectedImageWidth := 750
	expectedImageHeight := 300

	captchaServiceMock := new(MockCaptchaService)
	captchaServiceMock.On("GetImageForCode", randomCaptchaID, expectedImageWidth, expectedImageHeight).Return(randomImage, nil)

	api := prepareCaptchaAPI(captchaServiceMock)

	// when
	response := callGetCaptchaImageWithSize(t, api, randomCaptchaID, 1000, 1000)

	// then
	assert.Equal(t, http.StatusOK, response.Code, "response not expected")
	assert.Equal(t, randomImage, response.Body.Bytes())
}

func TestCaptchaGetImageUnderMinSize(t *testing.T) {
	// given
	randomCaptchaID := uuid.NewV4().String()
	randomImage := []byte{21, 37, 0, 11, 27}
	expectedImageWidth := 100
	expectedImageHeight := 45

	captchaServiceMock := new(MockCaptchaService)
	captchaServiceMock.On("GetImageForCode", randomCaptchaID, expectedImageWidth, expectedImageHeight).Return(randomImage, nil)

	api := prepareCaptchaAPI(captchaServiceMock)

	// when
	response := callGetCaptchaImageWithSize(t, api, randomCaptchaID, 10, 10)

	// then
	assert.Equal(t, http.StatusOK, response.Code, "response not expected")
	assert.Equal(t, randomImage, response.Body.Bytes())
}

func TestCaptchaGetNonExistingImage(t *testing.T) {
	// given
	captchaServiceMock := new(MockCaptchaService)
	captchaServiceMock.On("GetImageForCode", mock.Anything, mock.Anything, mock.Anything).Return([]byte{}, ErrCaptchaNoResult)

	api := prepareCaptchaAPI(captchaServiceMock)

	// when
	response := callGetCaptchaImage(t, api, uuid.NewV4().String())

	// then
	assert.Equal(t, http.StatusNotFound, response.Code, "response not expected")
}

func TestCaptchaGetAudio(t *testing.T) {
	// given
	randomCaptchaID := uuid.NewV4().String()
	randomAudio := []byte{21, 37, 0, 11, 27}
	language := "en-US"
	expectedAudioLanguage := "en"

	captchaServiceMock := new(MockCaptchaService)
	captchaServiceMock.On("GetAudioForCode", randomCaptchaID, expectedAudioLanguage).Return(randomAudio, nil)

	api := prepareCaptchaAPI(captchaServiceMock)

	// when
	response := callGetCaptchaAudio(t, api, randomCaptchaID, language)

	// then
	assert.Equal(t, http.StatusOK, response.Code, "response not expected")
	assert.Equal(t, randomAudio, response.Body.Bytes())
}

func TestCaptchaGetNonExistingAudio(t *testing.T) {
	// given
	captchaServiceMock := new(MockCaptchaService)
	captchaServiceMock.On("GetAudioForCode", mock.Anything, mock.Anything).Return([]byte{}, ErrCaptchaNoResult)

	api := prepareCaptchaAPI(captchaServiceMock)

	// when
	response := callGetCaptchaAudio(t, api, uuid.NewV4().String(), "en-US")

	// then
	assert.Equal(t, http.StatusNotFound, response.Code, "response not expected")
}

func init() {
	gin.SetMode(gin.TestMode)
	logging.Setup()
}

func prepareCaptchaAPI(captchaService CaptchaService) *gin.Engine {
	s := httpserver.NewServer()

	CaptchaAPI(s.Engine, captchaService)

	return s.Engine
}

func callGenerateCaptcha(t *testing.T, api *gin.Engine) *commons_test.TestHttpResponse {
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/v1/captcha/generate", nil)

	api.ServeHTTP(recorder, req)

	return &commons_test.TestHttpResponse{T: t, Code: recorder.Code, Body: recorder.Body}
}

func callGetCaptchaImage(t *testing.T, api *gin.Engine, captchaID string) *commons_test.TestHttpResponse {
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/api/v1/captcha/image/%s", captchaID), nil)

	api.ServeHTTP(recorder, req)

	return &commons_test.TestHttpResponse{T: t, Code: recorder.Code, Body: recorder.Body}
}

func callGetCaptchaImageWithSize(t *testing.T, api *gin.Engine, captchaID string, width, height int) *commons_test.TestHttpResponse {
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/api/v1/captcha/image/%s?width=%d&height=%d", captchaID, width, height), nil)

	api.ServeHTTP(recorder, req)

	return &commons_test.TestHttpResponse{T: t, Code: recorder.Code, Body: recorder.Body}
}

func callGetCaptchaAudio(t *testing.T, api *gin.Engine, captchaID string, language string) *commons_test.TestHttpResponse {
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/api/v1/captcha/audio/%s?lang=%s", captchaID, language), nil)

	api.ServeHTTP(recorder, req)

	return &commons_test.TestHttpResponse{T: t, Code: recorder.Code, Body: recorder.Body}
}
