package captcha

import (
	"cloud.google.com/go/firestore"
	"context"
	uuid "github.com/satori/go.uuid"
	"google.golang.org/api/iterator"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"time"
)

var captchasCollection = "captchas"

type FirestoreCaptchaDAO struct {
	FS *firestore.Client
}

func (dao *FirestoreCaptchaDAO) GetByID(id string) (*Captcha, error) {
	doc, err := dao.FS.Collection(captchasCollection).
		Doc(id).
		Get(context.Background())
	if err != nil {
		if status.Code(err) == codes.NotFound {
			return nil, ErrCaptchaNoResult
		}

		return nil, err
	}

	var captcha Captcha
	err = doc.DataTo(&captcha)
	if err != nil {
		return nil, err
	}

	return &captcha, nil
}

func (dao *FirestoreCaptchaDAO) Insert(captcha *Captcha) error {
	captcha.ID = uuid.NewV4().String()

	_, err := dao.FS.Collection(captchasCollection).
		Doc(captcha.ID).
		Set(context.Background(), captcha)
	if err != nil {
		return err
	}

	return nil
}

func (dao *FirestoreCaptchaDAO) Delete(captcha *Captcha) error {
	_, err := dao.FS.Collection(captchasCollection).
		Doc(captcha.ID).
		Delete(context.Background())
	if err != nil {
		if status.Code(err) == codes.NotFound {
			return ErrCaptchaNoResult
		}

		return err
	}

	return nil
}

func (dao *FirestoreCaptchaDAO) DeleteExpired() error {
	docs := dao.FS.Collection(captchasCollection).
		Where("expiresAt", "<", time.Now().UTC().Unix()).
		Documents(context.Background())
	defer docs.Stop()

	batch := dao.FS.Batch()
	for {
		doc, err := docs.Next()
		if err != nil {
			if err == iterator.Done {
				break
			}

			return err
		}

		batch.Delete(doc.Ref)
	}

	_, err := batch.Commit(context.Background())
	if err != nil {
		return err
	}

	return nil
}
