package captcha

import (
	"github.com/rs/zerolog/log"
)

func CreateExpiredCaptchasScheduler(captchaService CaptchaService) func() {
	return func() {
		log.Info().Msg("Expired Captchas scheduler has been triggered")

		err := captchaService.DeleteExpired()
		if err != nil {
			log.Error().Msgf("Error while deleting expired captchas: %v", err)
		}

		log.Info().Msg("Successfully executed Expired Captchas scheduler")
	}
}
