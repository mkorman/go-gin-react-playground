package captcha

import (
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/mkorman9/go-commons/web"
)

type CaptchaGenerateResponse struct {
	ID string `json:"id"`
}

type CaptchaAnswerRequest struct {
	ID     string `json:"id" validate:"required,uuid4"`
	Answer string `json:"answer" validate:"required"`
}

var GetCaptchaImageQueryParams = web.QueryParamsParsingRules{
	"width": func(value string) interface{} {
		width, err := strconv.Atoi(value)
		if err != nil {
			width = 250
		}

		if width < 100 {
			width = 100
		}
		if width > 750 {
			width = 750
		}

		return width
	},
	"height": func(value string) interface{} {
		height, err := strconv.Atoi(value)
		if err != nil {
			height = 75
		}

		if height < 45 {
			height = 45
		}
		if height > 300 {
			height = 300
		}

		return height
	},
}

var GetCaptchaAudioQueryParams = web.QueryParamsParsingRules{
	"lang": func(value string) interface{} {
		var language CaptchaLanguage

		switch value {
		case "pl-PL":
			language = CaptchaLangPl
		default:
			language = CaptchaLangEn
		}

		return language
	},
}

func CaptchaAPI(engine *gin.Engine, captchaService CaptchaService) {
	group := engine.Group("/api/v1/captcha")

	group.GET("/generate",
		func(c *gin.Context) {
			id, err := captchaService.GenerateCode()
			if err != nil {
				web.InternalError(c, err, "Unexpected error while generating captcha")
				return
			}

			c.JSON(http.StatusOK, &CaptchaGenerateResponse{id})
		},
	)

	group.GET("/image/:id",
		func(c *gin.Context) {
			id := c.Param("id")
			if ok, causes := web.ValidateVar("id", &id, "required,uuid4"); !ok {
				web.ValidationError(c, causes...)
				return
			}

			queryParams := web.ParseQueryParams(c, GetCaptchaImageQueryParams)

			img, err := captchaService.GetImageForCode(id, queryParams["width"].Int(), queryParams["height"].Int())
			if err != nil {
				if errors.Is(err, ErrCaptchaNoResult) {
					web.ErrorResponse(c, http.StatusNotFound, "Captcha not found")
					return
				}

				web.InternalError(c, err, "Internal error while retrieving image for captcha")
				return
			}

			c.Data(http.StatusOK, "image/png", img)
		},
	)

	group.GET("/audio/:id",
		func(c *gin.Context) {
			id := c.Param("id")
			if ok, causes := web.ValidateVar("id", &id, "required,uuid4"); !ok {
				web.ValidationError(c, causes...)
				return
			}

			queryParams := web.ParseQueryParams(c, GetCaptchaAudioQueryParams)

			audio, err := captchaService.GetAudioForCode(id, queryParams["lang"].String())
			if err != nil {
				if errors.Is(err, ErrCaptchaNoResult) {
					web.ErrorResponse(c, http.StatusNotFound, "Captcha not found")
					return
				}

				web.InternalError(c, err, "Internal error while retrieving audio for captcha")
				return
			}

			c.Data(http.StatusOK, "audio/mpeg", audio)
		},
	)
}
