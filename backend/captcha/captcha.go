package captcha

type Captcha struct {
	ID        string `firestore:"id"`
	Code      string `firestore:"code"`
	CreatedAt int64  `firestore:"createdAt"`
	ExpiresAt int64  `firestore:"expiresAt"`
}
