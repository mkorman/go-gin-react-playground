package commons_test

import (
	"bytes"
	"encoding/json"
	"testing"

	"github.com/mkorman9/go-commons/web"
	"github.com/stretchr/testify/assert"
)

type TestHttpResponse struct {
	T    *testing.T
	Code int
	Body *bytes.Buffer
}

func ReadJSONResponse[V any](response *TestHttpResponse) V {
	var v V
	err := json.Unmarshal(response.Body.Bytes(), &v)
	assert.NoError(response.T, err, "cannot unmarshal response body from json")
	return v
}

func AssertCause(t *testing.T, response *web.GenericResponse, field, code string) {
	found := false

	for _, cause := range response.Causes {
		if cause.Field == field && cause.Code == code {
			found = true
			break
		}
	}

	assert.True(t, found, "error cause not equal")
}
