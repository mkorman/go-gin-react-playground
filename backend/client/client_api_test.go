package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/mkorman9/go-commons/httpserver"
	"gitlab.com/mkorman/go-gin-react-playground/backend/commons_test"
	"gitlab.com/mkorman/go-gin-react-playground/backend/security"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/mkorman9/go-commons/logging"
	"github.com/mkorman9/go-commons/web"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestDefaultPagination(t *testing.T) {
	// given
	existingClientsList := []*Client{
		{ID: uuid.NewV4().String(), FirstName: "AAA", LastName: "111", CreditCards: []string{}},
		{ID: uuid.NewV4().String(), FirstName: "BBB", LastName: "222", CreditCards: []string{"1234 5678 9012 3456"}},
		{ID: uuid.NewV4().String(), FirstName: "BBB", LastName: "333", CreditCards: []string{"2345 6789 0123 4567", "3456 7890 1234 5678"}},
	}
	expectedPage := &ClientResponsePage{
		Data: convertClientsListToResponse(existingClientsList),
		Page: web.Page{PageNumber: DefaultClientPaginationOptions.PageNumber, PageSize: DefaultClientPaginationOptions.PageSize, TotalPages: 1},
	}

	clientsServiceMock := new(MockClientService)
	clientsServiceMock.On("FindPaged", DefaultClientsFilters, DefaultClientsSortingOptions, DefaultClientPaginationOptions).
		Return(&ClientsPage{Data: existingClientsList, Page: expectedPage.Page}, nil)

	api := prepareClientsAPI(clientsServiceMock, false)

	// when
	result := callGetAllClients(t, api)
	retrievedClientsPage := commons_test.ReadJSONResponse[ClientResponsePage](result)

	// then
	assert.Equal(t, http.StatusOK, result.Code, "response not ok")
	assert.Truef(t, assert.ObjectsAreEqual(expectedPage, &retrievedClientsPage), "retrieved clients page is not the expected one")
}

func TestSpecificPagination(t *testing.T) {
	// given
	page := 1
	pageSize := 2
	existingClientsList := []*Client{
		{ID: uuid.NewV4().String(), FirstName: "AAA", LastName: "111", CreditCards: []string{"2345 6789 0123 4567", "3456 7890 1234 5678"}},
	}
	expectedPage := &ClientResponsePage{
		Data: convertClientsListToResponse(existingClientsList),
		Page: web.Page{PageNumber: page, PageSize: pageSize, TotalPages: 1},
	}

	clientsServiceMock := new(MockClientService)
	clientsServiceMock.On("FindPaged", DefaultClientsFilters, DefaultClientsSortingOptions, web.PaginationOptions{PageNumber: page, PageSize: pageSize}).
		Return(&ClientsPage{Data: existingClientsList, Page: expectedPage.Page}, nil)

	api := prepareClientsAPI(clientsServiceMock, false)

	// when
	result := callGetPagedClients(t, api, page, pageSize)
	retrievedClientsPage := commons_test.ReadJSONResponse[ClientResponsePage](result)

	// then
	assert.Equal(t, http.StatusOK, result.Code, "response not ok")
	assert.Truef(t, assert.ObjectsAreEqual(expectedPage, &retrievedClientsPage), "retrieved clients page is not the expected one")
}

func TestPaginationWithInvalidValues(t *testing.T) {
	// given
	existingClientsList := []*Client{
		{ID: uuid.NewV4().String(), FirstName: "AAA", LastName: "111", CreditCards: []string{"2345 6789 0123 4567", "3456 7890 1234 5678"}},
	}
	expectedPage := &ClientResponsePage{
		Data: convertClientsListToResponse(existingClientsList),
		Page: web.Page{PageNumber: DefaultClientPaginationOptions.PageNumber, PageSize: MaxClientsPageSize, TotalPages: 1},
	}

	clientsServiceMock := new(MockClientService)
	clientsServiceMock.On("FindPaged", DefaultClientsFilters, DefaultClientsSortingOptions, web.PaginationOptions{PageNumber: DefaultClientPaginationOptions.PageNumber, PageSize: MaxClientsPageSize}).
		Return(&ClientsPage{Data: existingClientsList, Page: expectedPage.Page}, nil)

	api := prepareClientsAPI(clientsServiceMock, false)

	// when
	result := callGetPagedClients(t, api, -10, 2000)
	retrievedClientsPage := commons_test.ReadJSONResponse[ClientResponsePage](result)

	// then
	assert.Equal(t, http.StatusOK, result.Code, "response not ok")
	assert.Truef(t, assert.ObjectsAreEqual(expectedPage, &retrievedClientsPage), "retrieved clients page is not the expected one")
}

func TestGetOneExisting(t *testing.T) {
	// given
	testClient := &Client{
		ID:          uuid.NewV4().String(),
		FirstName:   "AAA",
		LastName:    "111",
		Email:       "aaa@bbb.com",
		CreditCards: []string{"2345 6789 0123 4567", "3456 7890 1234 5678"},
	}

	clientsServiceMock := new(MockClientService)
	clientsServiceMock.On("FindByID", testClient.ID).Return(testClient, nil)

	api := prepareClientsAPI(clientsServiceMock, false)

	// when
	result := callGetClientByID(t, api, testClient.ID)
	retrievedClientResponse := commons_test.ReadJSONResponse[ClientResponse](result)

	// then
	assert.Equal(t, http.StatusOK, result.Code, "response not ok")
	assert.Truef(t, assert.ObjectsAreEqual(convertClientToResponse(testClient), &retrievedClientResponse), "retrieved client is not the expected one")
}

func TestGetOneNonExisting(t *testing.T) {
	// given
	clientsServiceMock := new(MockClientService)
	clientsServiceMock.On("FindByID", mock.Anything).Return(nil, ErrClientNoResult)

	api := prepareClientsAPI(clientsServiceMock, false)

	// when
	result := callGetClientByID(t, api, "non-existing")

	// then
	assert.Equal(t, http.StatusNotFound, result.Code, "response not expected")
}

func TestAdd(t *testing.T) {
	// given
	newClientRequest := &ClientRequest{FirstName: "AAA", LastName: "111", CreditCards: []CreditCardRequest{{Number: "1234 5678 9012 3456"}}}
	correspondingClient := &Client{
		ID:          uuid.NewV4().String(),
		FirstName:   newClientRequest.FirstName,
		LastName:    newClientRequest.LastName,
		CreditCards: []string{newClientRequest.CreditCards[0].Number},
	}

	clientsServiceMock := new(MockClientService)
	clientsServiceMock.On("Add", mock.Anything, mock.Anything).Return(correspondingClient.ID, nil)
	clientsServiceMock.On("FindByID", correspondingClient.ID).Return(correspondingClient, nil)

	api := prepareClientsAPI(clientsServiceMock, false)

	// when
	addResult := callAddClient(t, api, newClientRequest)
	insertResultBody := commons_test.ReadJSONResponse[map[string]string](addResult)

	getResult := callGetClientByID(t, api, insertResultBody["id"])
	retrievedClientResponse := commons_test.ReadJSONResponse[ClientResponse](getResult)

	// then
	assert.Equal(t, http.StatusOK, addResult.Code, "response for add not ok")
	assert.Equal(t, http.StatusOK, getResult.Code, "response for get not ok")
	assert.Truef(t, assert.ObjectsAreEqual(convertClientToResponse(correspondingClient), &retrievedClientResponse), "retrieved client is not the inserted one")
}

func TestAddEmptyBody(t *testing.T) {
	// given
	clientsServiceMock := new(MockClientService)
	api := prepareClientsAPI(clientsServiceMock, false)

	// when
	result := callAddClient(t, api, &ClientRequest{})
	body := commons_test.ReadJSONResponse[web.GenericResponse](result)

	// then
	assert.Equal(t, http.StatusBadRequest, result.Code, "response not expected")
	commons_test.AssertCause(t, &body, "firstName", "required")
	commons_test.AssertCause(t, &body, "lastName", "required")
}

func TestAddInvalidEmail(t *testing.T) {
	// given
	clientsServiceMock := new(MockClientService)
	api := prepareClientsAPI(clientsServiceMock, false)

	// when
	result := callAddClient(t, api, &ClientRequest{FirstName: "AAA", LastName: "BBB", Email: "111"})
	body := commons_test.ReadJSONResponse[web.GenericResponse](result)

	// then
	assert.Equal(t, http.StatusBadRequest, result.Code, "response not expected")
	commons_test.AssertCause(t, &body, "email", "email")
}

func TestAddInvalidCardNumber(t *testing.T) {
	// given
	clientsServiceMock := new(MockClientService)
	api := prepareClientsAPI(clientsServiceMock, false)

	// when
	result := callAddClient(t, api, &ClientRequest{FirstName: "AAA", LastName: "BBB", CreditCards: []CreditCardRequest{{Number: "aaaa bbbb cccc dddd"}}})
	body := commons_test.ReadJSONResponse[web.GenericResponse](result)

	// then
	assert.Equal(t, http.StatusBadRequest, result.Code, "response not expected")
	commons_test.AssertCause(t, &body, "creditCards[0].number", "ccnumber")
}

func TestUpdate(t *testing.T) {
	// given
	existingClientsList := []*Client{
		{ID: uuid.NewV4().String(), FirstName: "AAA", LastName: "111", CreditCards: []string{"1234 5678 9012 3456"}},
		{ID: uuid.NewV4().String(), FirstName: "BBB", LastName: "222", CreditCards: []string{"2345 6789 0123 4567", "3456 7890 1234 5678"}},
	}
	clientToUpdate := existingClientsList[0]
	updatePayload := &ClientRequest{FirstName: "John", LastName: "Doe", CreditCards: []CreditCardRequest{{Number: "1234 5678 9012 3456"}}}
	expectedClientsList := []*Client{
		{ID: uuid.NewV4().String(), FirstName: "John", LastName: "Doe", CreditCards: []string{"1234 5678 9012 3456"}},
		{ID: uuid.NewV4().String(), FirstName: "BBB", LastName: "222", CreditCards: []string{"2345 6789 0123 4567", "3456 7890 1234 5678"}},
	}
	expectedResponse := &ClientResponsePage{
		Data: convertClientsListToResponse(expectedClientsList),
		Page: web.Page{PageNumber: DefaultClientPaginationOptions.PageNumber, PageSize: DefaultClientPaginationOptions.PageSize, TotalPages: 1},
	}

	clientsServiceMock := new(MockClientService)
	clientsServiceMock.On("Edit", clientToUpdate.ID, mock.Anything, mock.Anything).Return(nil)
	clientsServiceMock.On("FindPaged", DefaultClientsFilters, DefaultClientsSortingOptions, DefaultClientPaginationOptions).
		Return(&ClientsPage{Data: expectedClientsList, Page: expectedResponse.Page}, nil)

	api := prepareClientsAPI(clientsServiceMock, false)

	// when
	updateResult := callUpdateClient(t, api, clientToUpdate.ID, updatePayload)

	getResult := callGetAllClients(t, api)
	retrievedClientsPage := commons_test.ReadJSONResponse[ClientResponsePage](getResult)

	// then
	assert.Equal(t, http.StatusOK, updateResult.Code, "update response not ok")
	assert.Equal(t, http.StatusOK, getResult.Code, "get response not ok")
	assert.Truef(t, assert.ObjectsAreEqual(expectedResponse, &retrievedClientsPage), "retrieved clients list is not the expected one after update")
}

func TestUpdateNonExisting(t *testing.T) {
	// given
	clientsServiceMock := new(MockClientService)
	clientsServiceMock.On("Edit", mock.Anything, mock.Anything, mock.Anything).Return(ErrClientNoResult)

	api := prepareClientsAPI(clientsServiceMock, false)

	// when
	result := callUpdateClient(t, api, "non-existing", &ClientRequest{FirstName: "AAA", LastName: "111"})

	// then
	assert.Equal(t, http.StatusNotFound, result.Code, "response not expected")
}

func TestUpdateEmptyBody(t *testing.T) {
	// given
	clientsServiceMock := new(MockClientService)
	api := prepareClientsAPI(clientsServiceMock, false)

	// when
	result := callUpdateClient(t, api, "client-id", &ClientRequest{})
	body := commons_test.ReadJSONResponse[web.GenericResponse](result)

	// then
	assert.Equal(t, http.StatusBadRequest, result.Code, "response not expected")
	commons_test.AssertCause(t, &body, "firstName", "required")
	commons_test.AssertCause(t, &body, "lastName", "required")
}

func TestDelete(t *testing.T) {
	// given
	existingClientsList := []*Client{
		{ID: uuid.NewV4().String(), FirstName: "AAA", LastName: "111", CreditCards: []string{}},
		{ID: uuid.NewV4().String(), FirstName: "BBB", LastName: "222", CreditCards: []string{"1234 5678 9012 3456"}},
		{ID: uuid.NewV4().String(), FirstName: "BBB", LastName: "333", CreditCards: []string{"2345 6789 0123 4567", "3456 7890 1234 5678"}},
	}
	clientToDelete := existingClientsList[0]
	expectedClientsList := append(existingClientsList[:0], existingClientsList[1:]...)
	expectedResponse := &ClientResponsePage{
		Data: convertClientsListToResponse(expectedClientsList),
		Page: web.Page{PageNumber: DefaultClientPaginationOptions.PageNumber, PageSize: DefaultClientPaginationOptions.PageSize, TotalPages: 1},
	}

	clientsServiceMock := new(MockClientService)
	clientsServiceMock.On("Delete", clientToDelete.ID, mock.Anything).Return(nil)
	clientsServiceMock.On("FindPaged", DefaultClientsFilters, DefaultClientsSortingOptions, DefaultClientPaginationOptions).
		Return(&ClientsPage{Data: expectedClientsList, Page: expectedResponse.Page}, nil)

	api := prepareClientsAPI(clientsServiceMock, false)

	// when
	deleteResult := callDeleteClient(t, api, clientToDelete.ID)

	getResult := callGetAllClients(t, api)
	retrievedClientsPage := commons_test.ReadJSONResponse[ClientResponsePage](getResult)

	// then
	assert.Equal(t, http.StatusOK, deleteResult.Code, "delete response not ok")
	assert.Equal(t, http.StatusOK, getResult.Code, "get response not ok")
	assert.Truef(t, assert.ObjectsAreEqual(expectedResponse, &retrievedClientsPage), "retrieved clients list is not the expected one after delete")
}

func TestDeleteNonExisting(t *testing.T) {
	// given
	clientsServiceMock := new(MockClientService)
	clientsServiceMock.On("Delete", mock.Anything, mock.Anything).Return(ErrClientNoResult)

	api := prepareClientsAPI(clientsServiceMock, false)

	// when
	result := callDeleteClient(t, api, "non-existing")

	// then
	assert.Equal(t, http.StatusNotFound, result.Code, "response not expected")
}

func TestDeleteAccountBanned(t *testing.T) {
	// given
	clientsServiceMock := new(MockClientService)
	clientsServiceMock.On("Delete", "existing-id", mock.Anything).Return(nil)

	api := prepareClientsAPI(clientsServiceMock, true)

	// when
	result := callDeleteClient(t, api, "existing-id")
	body := commons_test.ReadJSONResponse[web.GenericResponse](result)

	// then
	assert.Equal(t, http.StatusBadRequest, result.Code, "response not expected")
	commons_test.AssertCause(t, &body, "account", "banned")
}

func init() {
	gin.SetMode(gin.TestMode)
	logging.Setup()
}

func prepareClientsAPI(clientsService ClientService, accountBanned bool) *gin.Engine {
	mockSession := &security.Session{
		Roles: []string{"CLIENTS_EDITOR"},
	}
	mockAccount := &security.Account{
		ID:       "1234",
		Username: "testclient",
	}

	accountServiceMock := new(security.MockAccountService)
	if accountBanned {
		mockAccount.BannedUntil = time.Now().UTC().Add(time.Hour).Unix()
	}

	s := httpserver.NewServer()
	ClientAPI(s.Engine, clientsService, security.NewMockedAuthMiddlewaresProvider(mockSession, mockAccount), accountServiceMock)
	return s.Engine
}

func callGetAllClients(t *testing.T, api *gin.Engine) *commons_test.TestHttpResponse {
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/v1/client", nil)

	api.ServeHTTP(recorder, req)

	return &commons_test.TestHttpResponse{t, recorder.Code, recorder.Body}
}

func callGetPagedClients(t *testing.T, api *gin.Engine, page, pageSize int) *commons_test.TestHttpResponse {
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/api/v1/client?page=%d&pageSize=%d", page, pageSize), nil)

	api.ServeHTTP(recorder, req)

	return &commons_test.TestHttpResponse{t, recorder.Code, recorder.Body}
}

func callGetSortedClients(t *testing.T, api *gin.Engine, sortBy string) *commons_test.TestHttpResponse {
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/api/v1/client?sortBy=%s", sortBy), nil)

	api.ServeHTTP(recorder, req)

	return &commons_test.TestHttpResponse{t, recorder.Code, recorder.Body}
}

func callGetFilteredClients(t *testing.T, api *gin.Engine, filters map[string]string) *commons_test.TestHttpResponse {
	var filtersList []string
	for key, value := range filters {
		filtersList = append(filtersList, fmt.Sprintf("filter[%s]=%s", key, value))
	}

	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/api/v1/client?%s", strings.Join(filtersList, "&")), nil)

	api.ServeHTTP(recorder, req)

	return &commons_test.TestHttpResponse{t, recorder.Code, recorder.Body}
}

func callGetClientByID(t *testing.T, api *gin.Engine, id string) *commons_test.TestHttpResponse {
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/api/v1/client/%s", id), nil)

	api.ServeHTTP(recorder, req)

	return &commons_test.TestHttpResponse{t, recorder.Code, recorder.Body}
}

func callAddClient(t *testing.T, api *gin.Engine, client *ClientRequest) *commons_test.TestHttpResponse {
	clientJSON, _ := json.Marshal(client)

	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/api/v1/client", bytes.NewReader(clientJSON))

	api.ServeHTTP(recorder, req)

	return &commons_test.TestHttpResponse{t, recorder.Code, recorder.Body}
}

func callUpdateClient(t *testing.T, api *gin.Engine, id string, client *ClientRequest) *commons_test.TestHttpResponse {
	clientJSON, _ := json.Marshal(client)

	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("PUT", fmt.Sprintf("/api/v1/client/%s", id), bytes.NewReader(clientJSON))

	api.ServeHTTP(recorder, req)

	return &commons_test.TestHttpResponse{t, recorder.Code, recorder.Body}
}

func callDeleteClient(t *testing.T, api *gin.Engine, id string) *commons_test.TestHttpResponse {
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", fmt.Sprintf("/api/v1/client/%s", id), nil)

	api.ServeHTTP(recorder, req)

	return &commons_test.TestHttpResponse{t, recorder.Code, recorder.Body}
}
