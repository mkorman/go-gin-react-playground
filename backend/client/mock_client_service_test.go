package client

import (
	"github.com/mkorman9/go-commons/web"
	"github.com/stretchr/testify/mock"
)

type MockClientService struct {
	mock.Mock
}

func (mock *MockClientService) FindPaged(
	filters ClientsFilters,
	sortingOptions web.SortingOptions,
	paginationOptions web.PaginationOptions,
) (*ClientsPage, error) {
	m := mock.Called(filters, sortingOptions, paginationOptions)

	page := m.Get(0)
	if page == nil {
		return nil, m.Error(1)
	}

	return page.(*ClientsPage), m.Error(1)
}

func (mock *MockClientService) FindCursor(
	filters ClientsFilters,
	cursorOptions web.CursorOptions,
) (*ClientsCursor, error) {
	m := mock.Called(filters, cursorOptions)

	cursor := m.Get(0)
	if cursor == nil {
		return nil, m.Error(1)
	}

	return cursor.(*ClientsCursor), m.Error(1)
}

func (mock *MockClientService) FindByID(id string) (*Client, error) {
	m := mock.Called(id)

	user := m.Get(0)
	if user == nil {
		return nil, m.Error(1)
	}

	return user.(*Client), m.Error(1)
}

func (mock *MockClientService) Add(user *Client, changeAuthor string) (string, error) {
	m := mock.Called(user, changeAuthor)
	return m.String(0), m.Error(1)
}

func (mock *MockClientService) Edit(id string, toReplace *Client, changeAuthor string) error {
	m := mock.Called(id, toReplace, changeAuthor)
	return m.Error(0)
}

func (mock *MockClientService) Delete(id string, changeAuthor string) error {
	m := mock.Called(id, changeAuthor)
	return m.Error(0)
}
