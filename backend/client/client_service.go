package client

import (
	"github.com/mkorman9/go-commons/web"
	"time"
)

var DefaultClientsFilters = ClientsFilters{}
var DefaultClientsSortingOptions = web.SortingOptions{Field: ClientsSortID, Reverse: false}
var DefaultClientPaginationOptions = web.PaginationOptions{PageNumber: 0, PageSize: 10}
var MaxClientsPageSize = 100

const (
	ClientsSortID          = iota
	ClientsSortGender      = iota
	ClientsSortFirstName   = iota
	ClientsSortLastName    = iota
	ClientsSortAddress     = iota
	ClientsSortPhoneNumber = iota
	ClientsSortEmail       = iota
	ClientsSortBirthDate   = iota
)

type ClientsFilters struct {
	Gender      string
	FirstName   string
	LastName    string
	Address     string
	PhoneNumber string
	Email       string
	BornAfter   *time.Time
	BornBefore  *time.Time
	CreditCard  string
}

type ClientsPage struct {
	web.Page
	Data []*Client `json:"data"`
}

type ClientsCursor struct {
	web.Cursor
	Data []*Client `json:"data"`
}

type ClientService interface {
	FindPaged(
		filters ClientsFilters,
		sortingOptions web.SortingOptions,
		paginationOptions web.PaginationOptions,
	) (*ClientsPage, error)
	FindCursor(
		filters ClientsFilters,
		cursorOptions web.CursorOptions,
	) (*ClientsCursor, error)
	FindByID(id string) (*Client, error)

	Add(client *Client, changeAuthor string) (string, error)
	Edit(id string, data *Client, changeAuthor string) error
	Delete(id string, changeAuthor string) error
}

type clientService struct {
	clientsDAO      ClientDAO
	clientsEventBus ClientEventBus
}

func NewClientService(clientDAO ClientDAO, clientEventBus ClientEventBus) ClientService {
	return &clientService{
		clientsDAO:      clientDAO,
		clientsEventBus: clientEventBus,
	}
}

func (service *clientService) FindPaged(
	filters ClientsFilters,
	sortingOptions web.SortingOptions,
	paginationOptions web.PaginationOptions,
) (*ClientsPage, error) {
	return service.clientsDAO.FindPaged(filters, sortingOptions, paginationOptions)
}

func (service *clientService) FindCursor(
	filters ClientsFilters,
	cursorOptions web.CursorOptions,
) (*ClientsCursor, error) {
	return service.clientsDAO.FindCursor(filters, cursorOptions)
}

func (service *clientService) FindByID(id string) (*Client, error) {
	client, err := service.clientsDAO.FindByID(id)
	if err != nil {
		return nil, err
	}

	return client, nil
}

func (service *clientService) Add(client *Client, changeAuthor string) (string, error) {
	id, err := service.clientsDAO.Insert(client, changeAuthor)
	if err != nil {
		return "", err
	}

	err = service.clientsEventBus.ClientAdded(id, changeAuthor)
	if err != nil {
		return "", err
	}

	return id, nil
}

func (service *clientService) Edit(id string, data *Client, changeAuthor string) error {
	err := service.clientsDAO.Update(id, data, changeAuthor)
	if err != nil {
		return err
	}

	err = service.clientsEventBus.ClientModified(id, changeAuthor)
	if err != nil {
		return err
	}

	return nil
}

func (service *clientService) Delete(id string, changeAuthor string) error {
	err := service.clientsDAO.Delete(id, changeAuthor)
	if err != nil {
		return err
	}

	err = service.clientsEventBus.ClientDeleted(id, changeAuthor)
	if err != nil {
		return err
	}

	return nil
}
