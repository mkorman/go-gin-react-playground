package client

import (
	"github.com/mkorman9/go-commons/gcppubsub"
	"time"
)

type ClientEvent struct {
	EventType string    `json:"eventType"`
	ClientID  string    `json:"clientId"`
	Timestamp time.Time `json:"timestamp"`
	Author    string    `json:"author"`
}

type ClientEventBus interface {
	ClientAdded(clientId, author string) error
	ClientModified(clientId, author string) error
	ClientDeleted(clientId, author string) error
}

type clientEventsBus struct {
	publisher *gcppubsub.Publisher
}

func NewClientEventBus(client *gcppubsub.Client) (ClientEventBus, error) {
	publisher, err := client.CreatePublisher("client.events")
	if err != nil {
		return nil, err
	}

	return &clientEventsBus{publisher}, nil
}

func StartClientEventsSubscription(client *gcppubsub.Client) (gcppubsub.MessageChannel, error) {
	return client.SubscribeToTopic("client.events")
}

func (bus *clientEventsBus) ClientAdded(clientId, author string) error {
	if bus == nil {
		return nil
	}

	return bus.publisher.Publish(
		&ClientEvent{
			EventType: "client_added",
			ClientID:  clientId,
			Timestamp: time.Now().UTC(),
			Author:    author,
		},
	)
}

func (bus *clientEventsBus) ClientModified(clientId, author string) error {
	if bus == nil {
		return nil
	}

	return bus.publisher.Publish(
		&ClientEvent{
			EventType: "client_modified",
			ClientID:  clientId,
			Timestamp: time.Now().UTC(),
			Author:    author,
		},
	)
}

func (bus *clientEventsBus) ClientDeleted(clientId, author string) error {
	if bus == nil {
		return nil
	}

	return bus.publisher.Publish(
		&ClientEvent{
			EventType: "client_deleted",
			ClientID:  clientId,
			Timestamp: time.Now().UTC(),
			Author:    author,
		},
	)
}
