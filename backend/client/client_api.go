package client

import (
	"errors"
	"gitlab.com/mkorman/go-gin-react-playground/backend/security"
	"net/http"
	"strconv"
	"time"

	"github.com/mkorman9/go-commons/web"

	"github.com/gin-gonic/gin"
)

const clientsDateFormat = "2006-01-02"

type ClientRequest struct {
	Gender      string              `json:"gender" validate:"omitempty,len=1,oneof=M F -"`
	FirstName   string              `json:"firstName" validate:"required,gt=0,lt=255"`
	LastName    string              `json:"lastName" validate:"required,gt=0,lt=255"`
	Address     string              `json:"address" validate:"omitempty,lt=1024"`
	PhoneNumber string              `json:"phoneNumber" validate:"omitempty,lt=64"`
	Email       string              `json:"email" validate:"omitempty,email,lt=64"`
	BirthDate   *time.Time          `json:"birthDate" validate:"omitempty"`
	CreditCards []CreditCardRequest `json:"creditCards" validate:"dive"`
}

type CreditCardRequest struct {
	Number string `json:"number" validate:"required,ccnumber"`
}

type ClientResponse struct {
	ID          string               `json:"id"`
	Gender      string               `json:"gender"`
	FirstName   string               `json:"firstName"`
	LastName    string               `json:"lastName"`
	Address     string               `json:"address"`
	PhoneNumber string               `json:"phoneNumber"`
	Email       string               `json:"email"`
	BirthDate   *time.Time           `json:"birthDate"`
	CreditCards []CreditCardResponse `json:"creditCards"`
}

type CreditCardResponse struct {
	Number string `json:"number"`
}

type ClientChangeResponse struct {
	Type           string          `json:"type"`
	Timestamp      time.Time       `json:"timestamp"`
	AuthorID       string          `json:"authorId"`
	AuthorUsername string          `json:"authorUsername,omitempty"`
	Changeset      ClientChangeset `json:"changeset,omitempty"`
}

type ClientResponsePage struct {
	web.Page
	Data []*ClientResponse `json:"data"`
}

type ClientResponseCursor struct {
	web.Cursor
	Data []*ClientResponse `json:"data"`
}

var GetClientsPagedQueryParams = web.QueryParamsParsingRules{
	"page": func(value string) interface{} {
		page, err := strconv.Atoi(value)
		if err != nil {
			page = DefaultClientPaginationOptions.PageNumber
		}

		if page < 0 {
			page = DefaultClientPaginationOptions.PageNumber
		}

		return page
	},
	"pageSize": func(value string) interface{} {
		pageSize, err := strconv.Atoi(value)
		if err != nil {
			pageSize = DefaultClientPaginationOptions.PageSize
		}

		if pageSize <= 0 {
			pageSize = DefaultClientPaginationOptions.PageSize
		}
		if pageSize > MaxClientsPageSize {
			pageSize = MaxClientsPageSize
		}

		return pageSize
	},
	"sortBy": func(value string) interface{} {
		switch value {
		case "":
			return DefaultClientsSortingOptions.Field
		case "id":
			return ClientsSortID
		case "gender":
			return ClientsSortGender
		case "firstName":
			return ClientsSortFirstName
		case "lastName":
			return ClientsSortLastName
		case "address":
			return ClientsSortAddress
		case "phoneNumber":
			return ClientsSortPhoneNumber
		case "email":
			return ClientsSortEmail
		case "birthDate":
			return ClientsSortBirthDate
		default:
			return DefaultClientsSortingOptions.Field
		}
	},
	"sortReverse": func(value string) interface{} {
		return value != ""
	},
	"filter[gender]": func(value string) interface{} {
		if value != "M" && value != "F" && value != "-" {
			return nil
		}

		return value
	},
	"filter[firstName]": func(value string) interface{} {
		return value
	},
	"filter[lastName]": func(value string) interface{} {
		return value
	},
	"filter[address]": func(value string) interface{} {
		return value
	},
	"filter[phoneNumber]": func(value string) interface{} {
		return value
	},
	"filter[email]": func(value string) interface{} {
		return value
	},
	"filter[bornAfter]": func(value string) interface{} {
		date, err := time.Parse(clientsDateFormat, value)
		if err != nil {
			return nil
		}

		return &date
	},
	"filter[bornBefore]": func(value string) interface{} {
		date, err := time.Parse(clientsDateFormat, value)
		if err != nil {
			return nil
		}

		return &date
	},
	"filter[creditCard]": func(value string) interface{} {
		return value
	},
}

var GetClientsCursorQueryParams = web.QueryParamsParsingRules{
	"cursor": func(value string) interface{} {
		return value
	},
	"limit": func(value string) interface{} {
		limit, err := strconv.Atoi(value)
		if err != nil {
			limit = DefaultClientPaginationOptions.PageSize
		}

		if limit <= 0 {
			limit = DefaultClientPaginationOptions.PageSize
		}
		if limit > MaxClientsPageSize {
			limit = MaxClientsPageSize
		}

		return limit
	},
	"filter[gender]": func(value string) interface{} {
		if value != "M" && value != "F" && value != "-" {
			return nil
		}

		return value
	},
	"filter[firstName]": func(value string) interface{} {
		return value
	},
	"filter[lastName]": func(value string) interface{} {
		return value
	},
	"filter[address]": func(value string) interface{} {
		return value
	},
	"filter[phoneNumber]": func(value string) interface{} {
		return value
	},
	"filter[email]": func(value string) interface{} {
		return value
	},
	"filter[bornAfter]": func(value string) interface{} {
		date, err := time.Parse(clientsDateFormat, value)
		if err != nil {
			return nil
		}

		return &date
	},
	"filter[bornBefore]": func(value string) interface{} {
		date, err := time.Parse(clientsDateFormat, value)
		if err != nil {
			return nil
		}

		return &date
	},
	"filter[creditCard]": func(value string) interface{} {
		return value
	},
}

func ClientAPI(
	engine *gin.Engine,
	clientsService ClientService,
	authMiddlewaresProvider security.AuthMiddlewaresProvider,
	accountService security.AccountService,
) {
	tokenMiddleware := authMiddlewaresProvider.BearerTokenMiddleware()
	group := engine.Group("/api/v1/client")

	group.GET("",
		func(c *gin.Context) {
			queryParams := web.ParseQueryParams(c, GetClientsPagedQueryParams)

			clientsPage, err := clientsService.FindPaged(
				ClientsFilters{
					Gender:      queryParams["filter[gender]"].String(),
					FirstName:   queryParams["filter[firstName]"].String(),
					LastName:    queryParams["filter[lastName]"].String(),
					Address:     queryParams["filter[address]"].String(),
					PhoneNumber: queryParams["filter[phoneNumber]"].String(),
					Email:       queryParams["filter[email]"].String(),
					BornAfter:   queryParams["filter[bornAfter]"].Time(),
					BornBefore:  queryParams["filter[bornBefore]"].Time(),
					CreditCard:  queryParams["filter[creditCard]"].String(),
				},
				web.SortingOptions{
					Field:   queryParams["sortBy"].Int(),
					Reverse: queryParams["sortReverse"].Bool(),
				},
				web.PaginationOptions{
					PageNumber: queryParams["page"].Int(),
					PageSize:   queryParams["pageSize"].Int(),
				},
			)
			if err != nil {
				web.InternalError(c, err, "Unexpected error while retrieving clients list")
				return
			}

			responsePage := &ClientResponsePage{
				Page: clientsPage.Page,
				Data: convertClientsListToResponse(clientsPage.Data),
			}

			c.JSON(http.StatusOK, responsePage)
		},
	)

	group.GET("/cursor/get",
		func(c *gin.Context) {
			queryParams := web.ParseQueryParams(c, GetClientsCursorQueryParams)

			clientsCursor, err := clientsService.FindCursor(
				ClientsFilters{
					Gender:      queryParams["filter[gender]"].String(),
					FirstName:   queryParams["filter[firstName]"].String(),
					LastName:    queryParams["filter[lastName]"].String(),
					Address:     queryParams["filter[address]"].String(),
					PhoneNumber: queryParams["filter[phoneNumber]"].String(),
					Email:       queryParams["filter[email]"].String(),
					BornAfter:   queryParams["filter[bornAfter]"].Time(),
					BornBefore:  queryParams["filter[bornBefore]"].Time(),
					CreditCard:  queryParams["filter[creditCard]"].String(),
				},
				web.CursorOptions{
					Cursor: queryParams["cursor"].String(),
					Limit:  queryParams["limit"].Int(),
				},
			)
			if err != nil {
				web.InternalError(c, err, "Unexpected error while retrieving clients list")
				return
			}

			responsePage := &ClientResponseCursor{
				Cursor: clientsCursor.Cursor,
				Data:   convertClientsListToResponse(clientsCursor.Data),
			}

			c.JSON(http.StatusOK, responsePage)
		},
	)

	group.GET("/:id",
		func(c *gin.Context) {
			id := c.Param("id")
			if len(id) == 0 {
				web.ValidationError(c, web.FieldError("id", "required"))
				return
			}

			client, err := clientsService.FindByID(id)
			if err != nil {
				if errors.Is(err, ErrClientNoResult) {
					web.ErrorResponse(c, http.StatusNotFound, "Client with this ID doesn't exist")
				} else {
					web.InternalError(c, err, "Unexpected error while retrieving client")
				}

				return
			}

			c.JSON(http.StatusOK, convertClientToResponse(client))
		},
	)

	group.GET("/changelog/:id",
		tokenMiddleware.AnyOfRoles("CLIENTS_EDITOR"),
		func(c *gin.Context) {
			id := c.Param("id")
			if len(id) == 0 {
				web.ValidationError(c, web.FieldError("id", "required"))
				return
			}

			client, err := clientsService.FindByID(id)
			if err != nil {
				if errors.Is(err, ErrClientNoResult) {
					web.ErrorResponse(c, http.StatusNotFound, "Client with this ID doesn't exist")
				} else {
					web.InternalError(c, err, "Unexpected error while retrieving client")
				}

				return
			}

			changelog := make([]ClientChangeResponse, len(client.Changelog))
			authorsUsernamesCache := make(map[string]string)

			for i, change := range client.Changelog {
				var changeAuthorUsername string

				if username, ok := authorsUsernamesCache[change.Author]; ok {
					changeAuthorUsername = username
				} else {
					changeAuthorAccount, err := accountService.GetByID(change.Author)
					if err != nil {
						web.InternalError(c, err, "Unexpected error while getting account info")
						return
					}

					authorsUsernamesCache[change.Author] = changeAuthorAccount.Username
					changeAuthorUsername = changeAuthorAccount.Username
				}

				changelog[i] = ClientChangeResponse{
					Type:           change.Type,
					Timestamp:      time.Unix(change.Timestamp, 0).UTC(),
					AuthorID:       change.Author,
					AuthorUsername: changeAuthorUsername,
					Changeset:      change.Changeset,
				}
			}

			c.JSON(http.StatusOK, changelog)
		},
	)

	group.POST("",
		tokenMiddleware.AnyOfRoles("CLIENTS_EDITOR"),
		func(c *gin.Context) {
			var clientRequest ClientRequest
			if ok, causes := web.BindJSONBody(c, &clientRequest); !ok {
				web.ValidationError(c, causes...)
				return
			}

			client := convertRequestToClient(&clientRequest)

			account := security.MustGetActiveAccount(c)
			if account.IsBanned() {
				web.ValidationError(c,
					web.FieldErrorMessage(
						"account",
						"banned",
						"this account is banned and hence cannot perform this operation",
					),
				)
				return
			}

			id, err := clientsService.Add(client, account.ID)
			if err != nil {
				web.InternalError(c, err, "Unexpected error while inserting client")
				return
			}

			c.JSON(http.StatusOK, map[string]string{"id": id})
		},
	)

	group.PUT("/:id",
		tokenMiddleware.AnyOfRoles("CLIENTS_EDITOR"),
		func(c *gin.Context) {
			id := c.Param("id")
			if len(id) == 0 {
				web.ValidationError(c, web.FieldError("id", "required"))
				return
			}

			var clientRequest ClientRequest
			if ok, causes := web.BindJSONBody(c, &clientRequest); !ok {
				web.ValidationError(c, causes...)
				return
			}

			client := convertRequestToClient(&clientRequest)

			account := security.MustGetActiveAccount(c)
			if account.IsBanned() {
				web.ValidationError(c,
					web.FieldErrorMessage(
						"account",
						"banned",
						"this account is banned and hence cannot perform this operation",
					),
				)
				return
			}

			if err := clientsService.Edit(id, client, account.ID); err != nil {
				if errors.Is(err, ErrClientNoResult) {
					web.ErrorResponse(c, http.StatusNotFound, "No client with this ID was found")
				} else {
					web.InternalError(c, err, "Unexpected error while updating client")
				}

				return
			}

			web.SuccessResponse(c, "OK")
		},
	)

	group.DELETE("/:id",
		tokenMiddleware.AnyOfRoles("CLIENTS_EDITOR"),
		func(c *gin.Context) {
			id := c.Param("id")
			if len(id) == 0 {
				web.ValidationError(c, web.FieldError("id", "required"))
				return
			}

			account := security.MustGetActiveAccount(c)
			if account.IsBanned() {
				web.ValidationError(c,
					web.FieldErrorMessage(
						"account",
						"banned",
						"this account is banned and hence cannot perform this operation",
					),
				)
				return
			}

			if err := clientsService.Delete(id, account.ID); err != nil {
				if errors.Is(err, ErrClientNoResult) {
					web.ErrorResponse(c, http.StatusNotFound, "No client with this ID was found")
				} else {
					web.InternalError(c, err, "Unexpected error while deleting client")
				}

				return
			}

			web.SuccessResponse(c, "OK")
		},
	)
}

func convertRequestToClient(clientRequest *ClientRequest) *Client {
	var birthDate *int64
	if clientRequest.BirthDate != nil {
		b := clientRequest.BirthDate.Unix()
		birthDate = &b
	}

	client := &Client{
		Gender:      clientRequest.Gender,
		FirstName:   clientRequest.FirstName,
		LastName:    clientRequest.LastName,
		Address:     clientRequest.Address,
		PhoneNumber: clientRequest.PhoneNumber,
		Email:       clientRequest.Email,
		BirthDate:   birthDate,
	}

	for _, cardRequest := range clientRequest.CreditCards {
		client.CreditCards = append(client.CreditCards, cardRequest.Number)
	}

	return client
}

func convertClientToResponse(client *Client) *ClientResponse {
	var birthDate *time.Time
	if client.BirthDate != nil {
		b := time.Unix(*client.BirthDate, 0).UTC()
		birthDate = &b
	}

	response := &ClientResponse{
		ID:          client.ID,
		Gender:      client.Gender,
		FirstName:   client.FirstName,
		LastName:    client.LastName,
		Address:     client.Address,
		PhoneNumber: client.PhoneNumber,
		Email:       client.Email,
		BirthDate:   birthDate,
		CreditCards: make([]CreditCardResponse, 0),
	}

	for _, card := range client.CreditCards {
		response.CreditCards = append(response.CreditCards, CreditCardResponse{Number: card})
	}

	return response
}

func convertClientsListToResponse(clients []*Client) []*ClientResponse {
	var responses []*ClientResponse

	for _, client := range clients {
		responses = append(responses, convertClientToResponse(client))
	}

	return responses
}
