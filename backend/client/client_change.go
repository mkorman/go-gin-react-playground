package client

type ClientChange struct {
	Type      string          `json:"type" firestore:"type"`
	Timestamp int64           `json:"timestamp" firestore:"timestamp"`
	Author    string          `json:"author" firestore:"author"`
	Changeset ClientChangeset `json:"changeset" firestore:"changeset"`
}

type ClientChangesetItem struct {
	Field    string `json:"field" firestore:"field"`
	OldValue string `json:"old" firestore:"old"`
	NewValue string `json:"new" firestore:"new"`
}

type ClientChangeset = []*ClientChangesetItem
