package client

import (
	"cloud.google.com/go/firestore"
	"context"
	"github.com/mkorman9/go-commons/web"
	uuid "github.com/satori/go.uuid"
	"google.golang.org/api/iterator"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"time"
)

var clientsCollection = "clients"

type FirestoreClientDAO struct {
	FS *firestore.Client
}

func (dao *FirestoreClientDAO) FindPaged(
	filters ClientsFilters,
	sortingOptions web.SortingOptions,
	paginationOptions web.PaginationOptions,
) (*ClientsPage, error) {
	return nil, nil
}

func (dao *FirestoreClientDAO) FindCursor(
	filters ClientsFilters,
	cursorOptions web.CursorOptions,
) (*ClientsCursor, error) {
	query := dao.FS.Collection(clientsCollection).
		Where("deleted", "==", false).
		OrderBy("id", firestore.Asc).
		Limit(cursorOptions.Limit)

	query = dao.addFiltersToQuery(query, filters)

	if cursorOptions.Cursor != "" {
		query = query.Where("id", ">", cursorOptions.Cursor)
	}

	var clients []*Client

	docs := query.Documents(context.Background())
	defer docs.Stop()

	for {
		doc, err := docs.Next()
		if err != nil {
			if err == iterator.Done {
				break
			}

			return nil, err
		}

		var client Client
		err = doc.DataTo(&client)
		if err != nil {
			return nil, err
		}

		clients = append(clients, &client)
	}

	cursor := &ClientsCursor{Data: clients}
	if len(clients) > 0 {
		cursor.NextCursor = clients[len(clients)-1].ID
	}

	return cursor, nil
}

func (dao *FirestoreClientDAO) FindByID(id string) (*Client, error) {
	docs := dao.FS.Collection(clientsCollection).
		Where("id", "==", id).
		Where("deleted", "==", false).
		Documents(context.Background())
	defer docs.Stop()

	var doc *firestore.DocumentSnapshot
	for {
		d, err := docs.Next()
		if err != nil {
			if err == iterator.Done {
				break
			}

			return nil, err
		}

		doc = d
	}

	if doc == nil {
		return nil, ErrClientNoResult
	}

	var client Client
	err := doc.DataTo(&client)
	if err != nil {
		return nil, err
	}

	return &client, nil
}

func (dao *FirestoreClientDAO) Insert(client *Client, changeAuthor string) (string, error) {
	client.ID = uuid.NewV4().String()
	client.Changelog = append(client.Changelog, &ClientChange{
		Type:      "CREATED",
		Timestamp: time.Now().UTC().Unix(),
		Author:    changeAuthor,
		Changeset: client.GenerateChangeset(&Client{}),
	})

	_, err := dao.FS.Collection(clientsCollection).
		Doc(client.ID).
		Set(context.Background(), client)
	if err != nil {
		return "", err
	}

	return client.ID, nil
}

func (dao *FirestoreClientDAO) Update(id string, data *Client, changeAuthor string) error {
	doc, err := dao.FS.Collection(clientsCollection).
		Doc(id).
		Get(context.Background())
	if err != nil {
		if status.Code(err) == codes.NotFound {
			return ErrClientNoResult
		}

		return err
	}

	var client Client
	err = doc.DataTo(&client)
	if err != nil {
		return err
	}

	data.Changelog = append(client.Changelog, &ClientChange{
		Type:      "UPDATED",
		Timestamp: time.Now().UTC().Unix(),
		Author:    changeAuthor,
		Changeset: data.GenerateChangeset(&client),
	})

	_, err = dao.FS.Collection(clientsCollection).
		Doc(id).
		Set(context.Background(), data)
	if err != nil {
		return err
	}

	return nil
}

func (dao *FirestoreClientDAO) Delete(id, changeAuthor string) error {
	doc, err := dao.FS.Collection(clientsCollection).
		Doc(id).
		Get(context.Background())
	if err != nil {
		if status.Code(err) == codes.NotFound {
			return ErrClientNoResult
		}

		return err
	}

	var client Client
	err = doc.DataTo(&client)
	if err != nil {
		return err
	}

	client.IsDeleted = true

	client.Changelog = append(client.Changelog, &ClientChange{
		Type:      "DELETED",
		Timestamp: time.Now().UTC().Unix(),
		Author:    changeAuthor,
		Changeset: nil,
	})

	_, err = dao.FS.Collection(clientsCollection).
		Doc(id).
		Set(context.Background(), client)
	if err != nil {
		return err
	}

	return nil
}

func (dao *FirestoreClientDAO) addFiltersToQuery(query firestore.Query, filters ClientsFilters) firestore.Query {
	if filters.Gender != "" {
		query = query.Where("gender", "==", filters.Gender)
	}

	if filters.FirstName != "" {
		query = query.Where("firstName", "==", filters.FirstName)
	}

	if filters.LastName != "" {
		query = query.Where("lastName", "==", filters.LastName)
	}

	if filters.Address != "" {
		query = query.Where("address", "==", filters.Address)
	}

	if filters.PhoneNumber != "" {
		query = query.Where("phoneNumber", "==", filters.PhoneNumber)
	}

	if filters.Email != "" {
		query = query.Where("email", "==", filters.Email)
	}

	if filters.BornAfter != nil {
		query = query.Where("birthDate", ">=", filters.BornAfter.Unix())
	}

	if filters.BornBefore != nil {
		query = query.Where("birthDate", "<", filters.BornBefore.Unix())
	}

	if filters.CreditCard != "" {
		query = query.Where("creditCards", "array-contains", filters.CreditCard)
	}

	return query
}
