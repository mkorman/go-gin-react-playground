package client

import (
	"github.com/mkorman9/go-commons/web"
	"github.com/mkorman9/go-gin-protocol/protocol"
	"google.golang.org/protobuf/types/known/timestamppb"
	"time"
)

type ClientServiceGRPC struct {
	clientService ClientService
}

func NewClientServiceGRPC(clientService ClientService) *ClientServiceGRPC {
	return &ClientServiceGRPC{
		clientService: clientService,
	}
}

func (service *ClientServiceGRPC) GetClients(
	_ *protocol.ClientRequest,
	stream protocol.ClientService_GetClientsServer,
) error {
	clientsCursor, err := service.clientService.FindCursor(ClientsFilters{}, web.CursorOptions{Limit: 10})
	if err != nil {
		return err
	}

	for _, client := range clientsCursor.Data {
		var birthDate *timestamppb.Timestamp
		if client.BirthDate != nil {
			birthDate = timestamppb.New(time.Unix(*client.BirthDate, 0).UTC())
		}

		var creditCards []*protocol.CreditCard
		for _, creditCard := range client.CreditCards {
			creditCards = append(creditCards, &protocol.CreditCard{Number: creditCard})
		}

		err = stream.Send(&protocol.Client{
			Id:          client.ID,
			Gender:      client.Gender,
			FirstName:   client.FirstName,
			LastName:    client.LastName,
			Address:     client.Address,
			PhoneNumber: client.PhoneNumber,
			Email:       client.Email,
			BirthDate:   birthDate,
			CreditCards: creditCards,
		})
		if err != nil {
			return err
		}
	}

	return nil
}
