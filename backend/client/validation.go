package client

import (
	"github.com/go-playground/validator/v10"
	"github.com/mkorman9/go-commons/web"
	"regexp"
)

func init() {
	ccnumberRegex := regexp.MustCompile(`^\d{4} \d{4} \d{4} \d{4}$`)
	web.DefaultValidator.RegisterValidation("ccnumber", func(fl validator.FieldLevel) bool {
		return ccnumberRegex.MatchString(fl.Field().String())
	})
}
