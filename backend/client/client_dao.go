package client

import (
	"errors"
	"github.com/mkorman9/go-commons/web"
)

var ErrClientNoResult = errors.New("no result")

type ClientDAO interface {
	FindPaged(
		filters ClientsFilters,
		sortingOptions web.SortingOptions,
		paginationOptions web.PaginationOptions,
	) (*ClientsPage, error)
	FindCursor(
		filters ClientsFilters,
		cursorOptions web.CursorOptions,
	) (*ClientsCursor, error)
	FindByID(id string) (*Client, error)

	Insert(client *Client, changeAuthor string) (string, error)
	Update(id string, data *Client, changeAuthor string) error
	Delete(id, changeAuthor string) error
}
