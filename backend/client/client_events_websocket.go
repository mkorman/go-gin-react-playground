package client

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"github.com/mkorman9/go-commons/gcppubsub"
	"github.com/rs/zerolog/log"
	"gopkg.in/olahol/melody.v1"
)

func ClientEventsWebsocket(engine *gin.Engine, clientEventsChannel gcppubsub.MessageChannel) {
	m := melody.New()

	m.HandleConnect(func(s *melody.Session) {
		log.Info().Msg("Clients Events listener connected")
	})

	m.HandleMessage(func(s *melody.Session, message []byte) {
	})

	m.HandleDisconnect(func(s *melody.Session) {
		log.Info().Msg("Clients Events listener disconnected")
	})

	m.HandleError(func(s *melody.Session, err error) {
		if websocketErr, ok := err.(*websocket.CloseError); ok {
			if websocketErr.Code == 1006 { // ignore "(abnormal closure): unexpected EOF" error
				return
			}
		}

		log.Warn().Err(err).Msg("Clients Events listener error")
	})

	listenForClientEvents(m, clientEventsChannel)

	engine.GET("/api/v1/client/events/ws",
		func(c *gin.Context) {
			_ = m.HandleRequest(c.Writer, c.Request)
		},
	)
}

func listenForClientEvents(m *melody.Melody, clientEventsChannel gcppubsub.MessageChannel) {
	go func() {
		for message := range clientEventsChannel {
			var event ClientEvent
			if err := json.Unmarshal(message.Data, &event); err != nil {
				log.Error().Err(err).Msg("Failed to unmarshal client event")
			}

			if err := handleClientEvent(m, &event); err != nil {
				log.Error().Err(err).Msg("Failed to handle client event")
			}

			message.Ack()
		}
	}()
}

func handleClientEvent(m *melody.Melody, event *ClientEvent) error {
	message, err := json.Marshal(event)
	if err != nil {
		return err
	}

	return m.Broadcast(message)
}
