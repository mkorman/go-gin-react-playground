package client

import (
	"fmt"
	"sort"
	"strings"
)

type Client struct {
	ID          string   `json:"id" firestore:"id"`
	Gender      string   `json:"gender" firestore:"gender"`
	FirstName   string   `json:"firstName" firestore:"firstName"`
	LastName    string   `json:"lastName" firestore:"lastName"`
	Address     string   `json:"address" firestore:"address"`
	PhoneNumber string   `json:"phoneNumber" firestore:"phoneNumber"`
	Email       string   `json:"email" firestore:"email"`
	BirthDate   *int64   `json:"birthDate" firestore:"birthDate"`
	CreditCards []string `json:"creditCards" firestore:"creditCards"`
	IsDeleted   bool     `json:"deleted" firestore:"deleted"`

	Changelog []*ClientChange `json:"changelog" firestore:"changelog"`
}

func (c *Client) GenerateChangeset(old *Client) ClientChangeset {
	changeset := make([]*ClientChangesetItem, 0)

	if c.Gender != old.Gender {
		changeset = append(changeset, &ClientChangesetItem{Field: "gender", OldValue: old.Gender, NewValue: c.Gender})
	}
	if c.FirstName != old.FirstName {
		changeset = append(changeset, &ClientChangesetItem{Field: "firstName", OldValue: old.FirstName, NewValue: c.FirstName})
	}
	if c.LastName != old.LastName {
		changeset = append(changeset, &ClientChangesetItem{Field: "lastName", OldValue: old.LastName, NewValue: c.LastName})
	}
	if c.Address != old.Address {
		changeset = append(changeset, &ClientChangesetItem{Field: "address", OldValue: old.Address, NewValue: c.Address})
	}
	if c.PhoneNumber != old.PhoneNumber {
		changeset = append(changeset, &ClientChangesetItem{Field: "phoneNumber", OldValue: old.PhoneNumber, NewValue: c.PhoneNumber})
	}
	if c.Email != old.Email {
		changeset = append(changeset, &ClientChangesetItem{Field: "email", OldValue: old.Email, NewValue: c.Email})
	}

	if c.BirthDate != nil || old.BirthDate != nil {
		var oldBirthDate *int64
		if old.BirthDate != nil {
			oldBirthDate = old.BirthDate
		}

		var newBirthDate *int64
		if c.BirthDate != nil {
			newBirthDate = c.BirthDate
		}

		if oldBirthDate != newBirthDate {
			changeset = append(changeset, &ClientChangesetItem{
				Field:    "birthDate",
				OldValue: fmt.Sprintf("%v", oldBirthDate),
				NewValue: fmt.Sprintf("%v", newBirthDate),
			})
		}
	}

	var oldCreditCards []string
	for _, cc := range old.CreditCards {
		oldCreditCards = append(oldCreditCards, cc)
	}
	sort.Strings(oldCreditCards)

	var newCreditCards []string
	for _, cc := range c.CreditCards {
		newCreditCards = append(newCreditCards, cc)
	}
	sort.Strings(newCreditCards)

	joinedOldCreditCards := strings.Join(oldCreditCards, ",")
	joinedNewCreditCards := strings.Join(newCreditCards, ",")

	if joinedOldCreditCards != joinedNewCreditCards {
		changeset = append(changeset, &ClientChangesetItem{Field: "creditCards", OldValue: joinedOldCreditCards, NewValue: joinedNewCreditCards})
	}

	return changeset
}
