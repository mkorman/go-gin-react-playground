#!/bin/bash

domain="${1}"
ip="${2}"

curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/${DNS_ZONE_ID}/dns_records/${DNS_RECORD_ID}" \
        -H "Authorization: Bearer ${CLOUDFLARE_API_TOKEN}" \
        -H "Content-Type: application/json" \
        --data '{"type":"A","name":"'${domain}'","content":"'${ip}'","proxied":false}'
