#!/bin/bash

ingress_name="ingress"
ingress_ip=""

while [[ "${ingress_ip}" == "" ]]; do
    echo "Waiting for ingress IP..." 1>&2
    ingress_ip="$(kubectl get ingress.v1.networking.k8s.io ${ingress_name} -o jsonpath="{.status.loadBalancer.ingress[*].ip}")"
    sleep 3;
done

echo "${ingress_ip}"
