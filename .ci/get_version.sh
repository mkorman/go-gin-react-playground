#!/bin/bash

if [[ "${CI_COMMIT_TAG}" != '' ]]; then 
    echo "${CI_COMMIT_TAG}"; 
else 
    echo "${CI_COMMIT_REF_NAME}-${CI_PIPELINE_ID}"; 
fi
