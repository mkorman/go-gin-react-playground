#!/bin/bash

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

VERSION="$1"
if [[ "$VERSION" == '' ]]; then
    VERSION='dev'
fi

echo 'Building backend'
cd $SCRIPTPATH/../backend
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "-X main.AppVersion=$VERSION" -o go-gin-react-playground
docker build -t "go-gin-react-playground/backend:$VERSION" .

echo 'Building frontend'
cd $SCRIPTPATH/../frontend
yarn install && yarn build
docker build -t "go-gin-react-playground/frontend:$VERSION" .
