#!/bin/bash

REGISTRY="$1"
if [[ "$REGISTRY" == '' ]]; then
    REGISTRY='localhost:32000'
fi

VERSION="$2"
if [[ "$VERSION" == '' ]]; then
    VERSION='dev'
fi

docker tag "go-gin-react-playground/backend:$VERSION" "$REGISTRY/go-gin-react-playground/backend:$VERSION"
docker tag "go-gin-react-playground/frontend:$VERSION" "$REGISTRY/go-gin-react-playground/frontend:$VERSION"

docker push "$REGISTRY/go-gin-react-playground/backend:$VERSION"
docker push "$REGISTRY/go-gin-react-playground/frontend:$VERSION"

docker rmi "$REGISTRY/go-gin-react-playground/backend:$VERSION"
docker rmi "$REGISTRY/go-gin-react-playground/frontend:$VERSION"
