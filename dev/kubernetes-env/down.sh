#!/bin/bash

deployment_name="${DEPLOYMENT_NAME:-go-gin-local-environment}"
namespace="${KUBE_NAMESPACE:-go-gin}"

echo "Shutting down the environment"
helm uninstall "$deployment_name" --namespace="$namespace" || true
