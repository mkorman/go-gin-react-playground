import requests
import random
import dateutil.parser
import uuid

def random_credit_card_number():
    def part():
        return f"{random.randint(0,9)}{random.randint(0,9)}{random.randint(0,9)}{random.randint(0,9)}"
    return f"{part()} {part()} {part()} {part()}"

ADMIN_USER = 'd9e40989-4892-49bf-a247-bf6f0c82378a'
clients = []
credit_cards = []
changes = []

resp = requests.get(url="https://randomuser.me/api/?results=200")
results = resp.json()['results']

for data in results:
    id = data['login']['uuid']
    gender = data['gender']
    first_name = data['name']['first']
    last_name = data['name']['last']
    address = ', '.join([data['location']['street']['name'] + ' ' + str(data['location']['street']['number']), data['location']['city'], data['location']['state'] + ' ' + str(data['location']['postcode'])])
    phone_number = data['cell']
    email = data['email']
    birth_date = dateutil.parser.isoparse(data['dob']['date']).strftime('%Y-%m-%d %H:%M:%S.%f')
    registration_date = dateutil.parser.isoparse(data['registered']['date']).strftime('%Y-%m-%d %H:%M:%S.%f')
    change_id = uuid.uuid4()

    if gender == 'male':
        gender = 'M'
    elif gender == 'female':
        gender = 'F'
    if random.randint(0,100) < 5:
        gender = '-'

    client_credit_cards = []
    for cc in (random_credit_card_number() for _ in range(random.randint(0,4))):
        client_credit_cards.append(cc)
    
    for cc in client_credit_cards:
        credit_cards.append(f"{id}\t{cc}")

    changeset = ('['
        '{"field":"gender","new":"%s"},'
        '{"field":"firstName","new":"%s"},'
        '{"field":"lastName","new":"%s"},'
        '{"field":"address","new":"%s"},'
        '{"field":"phoneNumber","new":"%s"},'
        '{"field":"email","new":"%s"},'
        '{"field":"birthDate","new":"%s"},'
        '{"field":"creditCards","new":"%s"}'
        ']') % (gender, first_name, last_name, address, phone_number, email, birth_date, ','.join(client_credit_cards))

    clients.append(f"{id}\t{gender}\t{first_name}\t{last_name}\t{address}\t{phone_number}\t{email}\t{birth_date}")
    changes.append(f"{change_id}\t{id}\tCREATED\t{registration_date}\t{ADMIN_USER}\t{changeset}")

print('COPY public.clients (id, gender, first_name, last_name, home_address, phone_number, email, birth_date) FROM stdin;')
print('\n'.join(clients))
print('\\.')
print()
print('COPY public.clients_credit_cards (client_id, number) FROM stdin;')
print('\n'.join(credit_cards))
print('\\.')
print()
print('COPY public.clients_changes (id, client_id, change_type, change_timestamp, author, changeset) FROM stdin;')
print('\n'.join(changes))
print('\\.')
