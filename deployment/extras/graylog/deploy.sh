#!/bin/bash

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

namespace="${TARGET_NAMESPACE:-monitoring}"
deployment_name="${DEPLOYMENT_NAME:-graylog}"

kubectl create namespace "$namespace" || true

echo "Starting Graylog deployment..."

helm install "$deployment_name" ${SCRIPTPATH} \
  --namespace="$namespace" $@ || exit 1

echo "Graylog deployment has finished"
