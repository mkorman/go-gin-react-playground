#!/bin/bash

deployment_name="${DEPLOYMENT_NAME:-graylog}"
namespace="${TARGET_NAMESPACE:-monitoring}"

echo "Shutting down Graylog"
helm uninstall "$deployment_name" --namespace="$namespace"
