#!/bin/bash

deployment_name="${DEPLOYMENT_NAME:-grafana}"
namespace="${TARGET_NAMESPACE:-monitoring}"

echo "Shutting down Grafana"
helm uninstall "$deployment_name" --namespace="$namespace"
