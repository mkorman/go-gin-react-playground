#!/bin/bash

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

namespace="${TARGET_NAMESPACE:-monitoring}"
deployment_name="${DEPLOYMENT_NAME:-grafana}"

kubectl create namespace "$namespace" || true

echo "Starting Grafana deployment..."

helm install "$deployment_name" ${SCRIPTPATH} \
  --namespace="$namespace" $@ || exit 1

echo "Grafana deployment has finished"
