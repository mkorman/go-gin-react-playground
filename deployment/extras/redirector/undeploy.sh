#!/bin/bash

deployment_name="${DEPLOYMENT_NAME:-redirector}"
namespace="${TARGET_NAMESPACE:-redirector}"

echo "Shutting down Redirector"
helm uninstall "$deployment_name" --namespace="$namespace"
