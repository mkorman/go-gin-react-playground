#!/bin/bash

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

namespace="${TARGET_NAMESPACE:-redirector}"
deployment_name="${DEPLOYMENT_NAME:-redirector}"

kubectl create namespace "$namespace" || true

echo "Starting Redirector deployment..."

helm install "$deployment_name" ${SCRIPTPATH} \
  --namespace="$namespace" $@ || exit 1

echo "Redirector deployment has finished"
