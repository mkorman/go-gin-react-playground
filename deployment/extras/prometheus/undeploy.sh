#!/bin/bash

deployment_name="${DEPLOYMENT_NAME:-prometheus}"
namespace="${TARGET_NAMESPACE:-monitoring}"

echo "Shutting down Prometheus"
helm uninstall "$deployment_name" --namespace="$namespace"
