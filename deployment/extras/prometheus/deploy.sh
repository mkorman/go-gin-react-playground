#!/bin/bash

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

namespace="${TARGET_NAMESPACE:-monitoring}"
deployment_name="${DEPLOYMENT_NAME:-prometheus}"

kubectl create namespace "$namespace" || true

echo "Starting Prometheus deployment..."

helm install "$deployment_name" ${SCRIPTPATH} \
  --namespace="$namespace" $@ || exit 1

echo "Prometheus deployment has finished"
