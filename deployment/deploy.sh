#!/bin/bash

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

deployment_name="${DEPLOYMENT_NAME:-go-gin}"
namespace="${KUBE_NAMESPACE:-go-gin}"
timeout="${DEPLOYMENT_TIMEOUT:-3m0s}"

echo "Starting the deployment of the app"

helm upgrade --install "$deployment_name" ${SCRIPTPATH}/go-gin \
        --namespace="$namespace" \
        --wait \
        --timeout "$timeout" $@ || exit 1

echo "Deployment of the app has finished"

kubectl get pods --namespace="$namespace"
