#!/bin/bash

deployment_name="${DEPLOYMENT_NAME:-go-gin}"
namespace="${KUBE_NAMESPACE:-go-gin}"

echo "Shutting down the deployment of the app"
helm uninstall "$deployment_name" --namespace="$namespace"
