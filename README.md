# go-gin-react-playground

This project is a simple petshop-style application. It is meant to serve as a personal playground for experimenting with the tech and as a framework for future,
more serious applications. It consists of a frontend written in React (although I consider myself a mediocre frontend dev) and a backend written in Go. Backend utilises some popular Go libraries, such as [Gin](https://github.com/gin-gonic/gin). The project is meant to be deployed on Kubernetes and provides a [Helm](https://helm.sh) chart to do so. It also has a fully featured Gitlab pipeline to help with that. Some other features include routing production traffic through [Cloudflare](https://www.cloudflare.com) and authorization to REST API through API tokens.

![Screenshot](.imgs/app_screenshot.png)

![Screenshot 2](.imgs/app_screenshot2.png)

![Screenshot iPhone](.imgs/app_screenshot_iphone.png)

# Build

### Backend

```bash
cd backend/
make build
```

### Frontend
```bash
cd frontend/
yarn install
yarn build
```

# Run locally

### Backend
```bash
docker-compose up

cd backend/
make run

# to cleanup:
cd ..
docker-compose down
```

### Frontend
```bash
cd frontend/
yarn install
yarn start
```

`http://localhost:3000/` will automatically open in the default browser.

# Deploy on Kubernetes

- Make sure you have Helm installed (https://helm.sh/docs/intro/install/)

## Prepare cluster

### Install nginx ingress controller

Install nginx ingress controller using Helm. Value of `replicaCount` dependes on your needs, and can be increased.
```bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update

kubectl create namespace ingress-nginx

helm install ingress-nginx ingress-nginx/ingress-nginx \
  --namespace=ingress-nginx \
  --set controller.service.externalTrafficPolicy="Local" \
  --set controller.replicaCount=1
```

Wait for the controller to wake up:
```bash
kubectl wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=120s
```

In `Docker Desktop` the Ingress controller listens on `127.0.0.1`, on ports `80` and `443`. In cloud environments, such as Azure - public IP address is allocated for the needs of the controller. It may introduce some additional costs. **Keep in mind** the Ingress controller works all the time, no matter if any Ingress resource is actually deployed at the time. If no actual Ingress resources are deployed - the Ingress controller redirects all the requests to the `default-backend` that just returns 404 and a fake certificate.

### Create a namespace for the project

```bash
kubectl create namespace go-gin
kubectl config set-context --current --namespace=go-gin
```

### Deploy development version on Docker Desktop

Build Docker images locally with `dev` tag:
```bash
dev/build.sh
```

Start development environment. The script will upload application secrets to the cluster, start external dependencies such as Firestore as well as feed the database with some test data:
```bash
dev/kubernetes-env/up.sh
```

Deploy:
```bash
deployment/deploy.sh
```

In order to clean up, simply run:
```bash
deployment/undeploy.sh
dev/kubernetes-env/down.sh
```

### Deploy development version on any cluster

Build Docker images locally with `dev` tag:
```bash
dev/build.sh
```

Push Docker images to the registry. Both your workstation and the target cluster should be able to access the specified registry:
```bash
dev/push.sh 192.168.1.100:32000
```

Start development environment. The script will upload application secrets to the cluster, start external dependencies such as Firestore as well as feed the database with some test data:
```bash
dev/kubernetes-env/up.sh
```

Deploy (remember to specify the full names of the pushed images):
```bash
deployment/deploy.sh \
  --set backend.imageName="192.168.1.100:32000/go-gin-react-playground/backend" \
  --set frontend.imageName="192.168.1.100:32000/go-gin-react-playground/frontend"
```

In order to clean up, simply run:
```bash
deployment/undeploy.sh
dev/kubernetes-env/down.sh
```

### Deploy production version in the cloud

#### Provide access to Gitlab registry

- Generate a Gitlab personal access token with `read_registry` scope
- Generate `AUTH_STRING` with `echo -n '<USERNAME>:<ACCESS_TOKEN>' | base64`
- Create a `docker.json` file:
```json
{
	"auths": {
		"registry.gitlab.com": {
			"auth": "<AUTH_STRING>"
		}
	}
}
```
- Upload it to the cluster
```bash
kubectl create secret generic gitlab-docker-registry --namespace=kube-system \
    --from-file=.dockerconfigjson=./docker.json --type="kubernetes.io/dockerconfigjson"
```

#### Create a secret with all the credentials
Make sure `credentials.json` file exists and contains service account credentials for GCP.
     
Create `secrets.yml` file and populate it with secret data
```
gcp:
  project: <GCP_PROJECT_ID>
```

Upload it
```bash
kubectl create secret generic backend-secrets --from-file=secrets.yml --from-file=credentials.json
```

#### Generate HTTPS certificate
Either generate a self-signed cert
```bash
export DOMAIN="example.com"
openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout key.pem -out cert.pem -subj "/CN=$DOMAIN/O=$DOMAIN"
```

Or use Let's Encrypt to generate a proper one
```bash
# brew install certbot
export DOMAIN="example.com"
sudo certbot -d "$DOMAIN" --manual --preferred-challenges dns certonly
sudo cp "/etc/letsencrypt/live/$DOMAIN/fullchain.pem" ./cert.pem && sudo chown $USER ./cert.pem
sudo cp "/etc/letsencrypt/live/$DOMAIN/privkey.pem" ./key.pem && sudo chown $USER ./key.pem

# to renew later: sudo certbot renew -q
```

Then just upload it
```bash
kubectl create secret tls domain-specific-tls-cert --key key.pem --cert cert.pem
```

#### (Optional) Add security headers

It's a good idea to also improve security by telling ingress to send some additional security headers. Run:
```bash
kubectl apply -f - <<EOF
apiVersion: v1
kind: ConfigMap
metadata:
  name: security-headers
  namespace: ingress-nginx
data:
  X-Frame-Options: "DENY"
  X-Content-Type-Options: "nosniff"
  X-XSS-Protection: "0"
  Strict-Transport-Security: "max-age=63072000; includeSubDomains; preload"
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: ingress-nginx-controller
  namespace: ingress-nginx
  labels:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/part-of: ingress-nginx
data:
  add-headers: "ingress-nginx/security-headers"
EOF
```

#### Deploy
```bash
deployment/deploy.sh \
  --set app.version="v1.0.4" \
  --set backend.imageName="registry.gitlab.com/mkorman/go-gin-react-playground/backend" \
  --set frontend.imageName="registry.gitlab.com/mkorman/go-gin-react-playground/frontend" \
  --set images.pullSecret="kube-system/gitlab-docker-registry" \
  --set ingress.hostname="example.com" \
  --set ingress.stictHostCheck=true \
  --set ingress.useHttps=true \
  --set ingress.tlsCertName="domain-specific-tls-cert"
```

#### Clean up
```bash
deployment/undeploy.sh
```

## (Optional) Configure logs collection
Graylog can be deployed with:
```bash
deployment/extras/graylog/deploy.sh
```
Graylog UI runs on port `9000`. Default credentials are `admin/admin`. By default, UDP ports `12201` (gelf) and `1514` (syslog) are opened.
In order to configure Graylog to receive messages from backend and frontend:
- Add two new input - `GELF UDP` on port `12201` and `Syslog UDP` on port `1514`
- Under `Streams` add new stream titled `App Logs`, set it's "index set" to `Default index set`, save it and then start it.
- Under `System->Pipelines` add new pipeline `app logs` and attach it to the `All messages` stream.
- Add all the rules from the `deployment/extras/graylog/rules` directory to the `Stage 0` stage of newly added pipeline. Make sure to select `At least one of the rules on this stage matches the message` option.
- Now when you click `Search` and select `App Logs` stream, you should see some properly parsed logs from both frontend and backend.

We also need to make the app aware of the Graylog by specifying additional flags to `deployment/deploy.sh` script when deploying the app:
```bash
deployment/deploy.sh \
...
  --set backend.config.remoteLogging.enabled=true \
  --set frontend.config.remoteLogging.enabled=true
...
```

In order to clean up, run:
```bash
deployment/extras/graylog/undeploy.sh
```

## (Optional) Configure metrics collection

Application is configured to automatically publish metrics in a format recognized by the Prometheus.
All you need to do is to deploy Prometheus to your cluster:
```bash
deployment/extras/prometheus/deploy.sh
```

It can be easily cleaned up with:
```bash
deployment/extras/prometheus/undeploy.sh
```

Grafana can be deployed the similar way:
```bash
deployment/extras/grafana/deploy.sh
```
Grafana UI runs on port `3000`.
Default credentials are `admin/admin`. Address to the prometheus data source would be `http://prometheus.monitoring.svc.cluster.local:9090`.
Sample Grafana dashboard can be imported from `deployment/extras/grafana/dashboard/go_metrics.json`.

To clean up Grafana run:
```bash
deployment/extras/grafana/undeploy.sh
```

## (Random Notes) Deployment on Microsoft Azure

### Provision AKS (Kubernetes cluster)

- Open Azure console, navigate to `Kubernetes Services` and click `Add -> Add Kubernetes cluster`
- Enter `Kubernetes cluster name`, `Region` and choose `Availability Zones`
- Specify number of in the primary pool and their type. For testing environment - `1` node is enough, for production - specify `3` or more nodes for high-availability. `A2_v2` is probably the cheapest node and is more than enough for testing environment. For production - choose general purpose nodes like `D2s_v3`.
- In the next tab you can specify more node pools.
- In `Authentication` tab select `System-assigned managed identity`. ENABLE `Role-based access control (RBAC)` and DISABLE `AKS-managed Azure Active Directory`
- In `Networking` tab - set `Network configuration` to `kubenet`, make sure `Enable HTTP application routing` is DISABLED. Under `Network policy` you may consider choosing `Calico`- it will allow you to create `NetworkPolicy` resources. They WILL NOT WORK if you choose `None`.
- In `Integrations` tab - DISABLE both `Container monitoring` and `Azure Policy`.
- After clicking `Create` a couple of resources will be provisioned - `Kubernetes Services`, `Virtual Network` (`aks-vnet-*`), `Virtual Machine Scale set` and `Public IP Address` (used for egress traffic from the cluster).
- After cluster successfully provisions - you'll be able to get it's connection details by clicking `Connect`. Procedure consists of installing `azure-cli` and retrieving cluster credentials through it:

```bash
brew install azure-cli
az login

az account set --subscription <SUBSCRIPTION_ID>
az aks get-credentials --resource-group <RESOURCE_GROUP> --name <CLUSTER_NAME>
```

### ~~Provision PostgreSQL instance~~

- Open Azure console, navigate to `Azure Database for PostgreSQL servers` and click `New`
- Select `Single server` (it claims to provide 99.99% availability)
- Enter details like `Server Name` and `Location`. Under `Compute + storage` select either `Basic` tier for testing environment or `General Purpose` if the application requires Geo-Redundancy
- Enter `Admin Username`, generate `Admin Password` with something like `dd if=/dev/urandom bs=48 count=1 | base64`
- Create database and wait for it to provision
- Copy DSN from `Connection String` section. If you need an access from the Internet (**NOT RECOMMENDED!**) - add firewall rule `0.0.0.0-255.255.255.255` under `Connection Security`

### ~~Provision Redis instance~~

- Open Azure console, navigate to `Azure Cache for Redis` and click `New`
- Enter `DNS name` and `Location`. Under `Cache Type` select `Basic C0` for testing environment or `Standard C1` (or higher) if the application requires high-availability.
- In the next section select either `Public Endpoint` (**NOT RECOMMENDED!**) or `Private Endpoint`. In case of private endpoint - you'll need to create a private network in the private network created for AKS cluster (`aks-vnet-*`).
- Create instance and wait for it to provision. Address and password will pop up when you click `Keys: Show access keys...`

### Integrate Kubernetes cluster with Gitlab

Integration with Gitlab provides a basic pods monitoring on the project's page and automatic configuration of `kubectl` in deployment CI jobs. Full instruction is available under https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html
